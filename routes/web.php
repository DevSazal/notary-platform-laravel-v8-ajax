<?php

use Illuminate\Support\Facades\Route;
// Add Controller Directory
use App\Http\Controllers\Admin;
use App\Http\Controllers\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Admin\AdminController::class, 'index']);
Route::get('/admin/login', [Admin\AuthController::class, 'index'])->name('login');
Route::post('/admin/login', [Admin\AuthController::class, 'login'])->name('admin.login');

// admin routes [start]

// orders
Route::prefix('admin')->middleware('admin')->group(function () {
  // ... start admin
  Route::get('/', [Admin\AdminController::class, 'index'])->name('admin.index');
  Route::get('/dashboard', [Admin\DashboardController::class, 'index']);

  Route::prefix('order')->group(function () {
    Route::get('/', [Admin\OrderController::class, 'index'])->name('admin.orders.index');
    Route::post('/', [Admin\OrderController::class, 'indexAjax'])->name('admin.order.index.list');
    Route::get('/create', [Admin\OrderController::class, 'create'])->name('admin.orders.create');
    Route::post('/register', [Admin\OrderController::class, 'registerOrder'])->name('admin.order.register');

    Route::get('/saved', [Admin\OrderController::class, 'savedOrder'])->name('admin.order.saved');
    Route::get('/unassigned', [Admin\OrderController::class, 'unassignedOrder'])->name('admin.order.unassigned');
    Route::post('/unassigned', [Admin\OrderController::class, 'unassignedOrderDataTable'])->name('admin.order.unassigned.list');
    Route::get('/hold', [Admin\OrderController::class, 'holdOrder'])->name('admin.order.hold');
    Route::post('/hold', [Admin\OrderController::class, 'holdOrderDataTable'])->name('admin.order.hold.list');
  });

  Route::get('/admins', [Admin\AdminController::class, 'admin'])->name('admin.admin.index');
  Route::get('/admins/create', [Admin\AdminController::class, 'create'])->name('admin.admin.create');

  Route::prefix('qb')->group(function () {
    Route::get('/customers', [Admin\QuickBookController::class, 'customers'])->name('admin.qb.customer');
    Route::get('/vendors', [Admin\QuickBookController::class, 'vendors'])->name('admin.qb.vendor');
    Route::get('/invoices', [Admin\QuickBookController::class, 'invoices'])->name('admin.qb.invoice');
    Route::get('/bills', [Admin\QuickBookController::class, 'bills'])->name('admin.qb.bill');
  });

  // Route::get('/notary', [Admin\NotaryController::class, 'index'])->name('admin.notary.index');

  Route::prefix('check')->group(function () {
    Route::get('/paid', [Admin\CheckController::class, 'paid'])->name('admin.check.paid');
    Route::get('/unpaid', [Admin\CheckController::class, 'unpaid'])->name('admin.check.unpaid');
  });

  Route::prefix('notary')->group(function () {
    Route::get('/', [Admin\NotaryController::class, 'index'])->name('admin.notary.index');
    Route::post('/data-list', [Admin\NotaryController::class, 'notaryDataTable'])->name('admin.notary.index.list');
    Route::get('/create', [Admin\NotaryController::class, 'create'])->name('admin.notary.create');
    Route::post('/register', [Admin\NotaryController::class, 'registerNotary'])->name('admin.notary.register');
    Route::get('/{id}', [Admin\NotaryController::class, 'show'])->name('admin.notary.show');
    Route::get('/{id}/edit', [Admin\NotaryController::class, 'edit'])->name('admin.notary.edit');
    Route::put('/{id}/update', [Admin\NotaryController::class, 'update'])->name('admin.notary.update');

    Route::post('/{id}/active', [Admin\NotaryController::class, 'active'])->name('admin.notary.active');
    Route::post('/{id}/inactive', [Admin\NotaryController::class, 'inactive'])->name('admin.notary.inactive');
    Route::delete('/{id}/delete', [Admin\NotaryController::class, 'delete'])->name('admin.notary.delete');
  });

  Route::get('/shipment', [Admin\ShipmentController::class, 'index'])->name('admin.shipment.index');
  Route::get('/guide', [Admin\GuideController::class, 'index'])->name('admin.guide.index');

  Route::prefix('news')->group(function () {
    Route::get('/', [Admin\NewsController::class, 'index'])->name('admin.news.index');
  });

  Route::prefix('client')->group(function () {
    Route::get('/', [Admin\ClientController::class, 'index'])->name('admin.client.index');
    Route::get('/create', [Admin\ClientController::class, 'create'])->name('admin.client.create');
    Route::get('/{id}', [Admin\ClientController::class, 'show'])->name('admin.client.show');
  });

  Route::get('/logout', [Admin\AuthController::class, 'logout'])->name('admin.logout');
  // ... end admin
});

// client routes [start]
Route::prefix('client')->group(function () {
  Route::get('/', [Client\DefaultController::class, 'index'])->name('client.index');

  Route::prefix('order')->group(function () {
    Route::get('/', [Client\OrderController::class, 'index'])->name('client.order.index');
    Route::get('/request-a-signing', [Client\OrderController::class, 'place'])->name('client.order.place');
  });

  Route::prefix('profile')->group(function () {
    Route::get('/', [Client\ProfileController::class, 'index'])->name('client.pro.index');
    Route::get('/edit', [Client\ProfileController::class, 'edit'])->name('client.pro.edit');
  });

  Route::get('/make-suggestion', [Client\DefaultController::class, 'suggestion'])->name('client.suggestion.index');
  Route::get('/message-co-worker', [Client\DefaultController::class, 'coWorker'])->name('client.co-worker.index');
  Route::get('/request-mousepad', [Client\RequestController::class, 'mousepad'])->name('client.request.mousepad');

});

// Route::get('Admin/Orders/Add');
// Route::get('Admin/Orders/Unassigned');
// Route::get('Admin/Orders/List');
// Route::get('Admin/Orders/Onhold');
// Route::get('Admin/Orders/Saved');
//
//
// // miscs
// Route::get('Admin/Files');
// Route::get('Admin/Clients');
// Route::get('Admin/Notaries');
// Route::get('Admin/Agents');
// Route::get('Admin/Leads');
//
// // quickbooks
// Route::get('Admin/Quickbooks/Vendors');
// Route::get('Admin/Quickbooks/Customers');
// Route::get('Admin/Quickbooks/Invoice');
// Route::get('Admin/Quickbooks/Bills');
// Route::get('Admin/Quickbooks/Bills');
//
// // admins
// Route::get('Admin/Admins/FullAccess');
// Route::get('Admin/Admins/LimitedAccess');
// Route::get('Admin/Admins/Add');
