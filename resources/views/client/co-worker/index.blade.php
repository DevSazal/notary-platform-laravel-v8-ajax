<!--  -->

@extends('layouts.client.app')

@section('title', 'Tell A Co-Worker')
@section('description', '')
@section('keyword', '')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">


    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/editors/quill/quill.snow.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/editors/quill/quill.bubble.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-quill-editor.css') }}">

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Tell A Co-Worker</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('client') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('client') }}">Client</a>
                                    </li>
                                    <li class="breadcrumb-item active">New Message
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
              <!-- Basic Textarea start -->
                <section class="basic-textarea">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"></h4>
                                </div>
                                <div class="card-body">
                                    <!-- <p class="card-text text-success mb-2"></p> -->




                                    <form class="form form-vertical">
                                          <div class="row">
                                              <div class="col-12">
                                                  <div class="form-group">
                                                      <label>*Email :</label>
                                                      <div class="input-group input-group-merge">
                                                          <div class="input-group-prepend">
                                                              <span class="input-group-text"><i class="far fa-envelope"></i></span>
                                                          </div>
                                                          <input type="email" class="form-control" name="ncP" placeholder="Emails seprated by comma" />
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12">
                                                  <div class="form-group">
                                                      <label>Subject :</label>
                                                      <div class="input-group input-group-merge">
                                                          <div class="input-group-prepend">
                                                              <span class="input-group-text"><i class="fas fa-tag"></i></span>
                                                          </div>
                                                          <input type="email" class="form-control" name="ncP" placeholder="Subject" />
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12  mt-2">
                                                  <div class="form-group">
                                                      <label>Message :</label>




                                                          <div class="row">
                                                              <div class="col-sm-12">
                                                                  <div id="snow-wrapper">
                                                                      <div id="snow-container">
                                                                          <div class="quill-toolbar">
                                                                              <span class="ql-formats">
                                                                                  <select class="ql-header">
                                                                                      <option value="1">Heading</option>
                                                                                      <option value="2">Subheading</option>
                                                                                      <option selected>Normal</option>
                                                                                  </select>
                                                                                  <select class="ql-font">
                                                                                      <option selected>Sailec Light</option>
                                                                                      <option value="sofia">Sofia Pro</option>
                                                                                      <option value="slabo">Slabo 27px</option>
                                                                                      <option value="roboto">Roboto Slab</option>
                                                                                      <option value="inconsolata">Inconsolata</option>
                                                                                      <option value="ubuntu">Ubuntu Mono</option>
                                                                                  </select>
                                                                              </span>
                                                                              <span class="ql-formats">
                                                                                  <button class="ql-bold"></button>
                                                                                  <button class="ql-italic"></button>
                                                                                  <button class="ql-underline"></button>
                                                                              </span>
                                                                              <span class="ql-formats">
                                                                                  <button class="ql-list" value="ordered"></button>
                                                                                  <button class="ql-list" value="bullet"></button>
                                                                              </span>
                                                                              <span class="ql-formats">
                                                                                  <button class="ql-link"></button>
                                                                                  <button class="ql-image"></button>
                                                                                  <button class="ql-video"></button>
                                                                              </span>
                                                                              <span class="ql-formats">
                                                                                  <button class="ql-formula"></button>
                                                                                  <button class="ql-code-block"></button>
                                                                              </span>
                                                                              <span class="ql-formats">
                                                                                  <button class="ql-clean"></button>
                                                                              </span>
                                                                          </div>
                                                                          <div class="editor">

                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>



                                                  </div>
                                              </div>
                                              <div class="col-12 text-center mt-2">
                                                  <button type="reset" class="btn btn-primary waves-effect waves-float waves-light mr-1"><i class="far fa-paper-plane mr-25"></i> Send</button>
                                                  <button type="reset" class="btn btn-outline-danger"><i class="fas fa-redo-alt mr-25"></i> Reset</button>
                                              </div>
                                          </div>
                                    </form>









                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Textarea end -->




            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>



    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>

    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/highlight.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/katex.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-quill-editor.js') }}"></script>
    <style>
        .ql-editor{
            min-height:400px;
        }
    </style>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });

      var date = new Date();

      var currentTime = date.getHours() + ':' + date.getMinutes() + ' AM';


      document.getElementsByClassName('fp2').value = currentTime;
      // alert(currentTime);

      $('.fp2').flatpickr({
        enableTime: true,
         noCalendar: true,
          dateFormat: "h:i K",
      });




    </script>
@endsection
