<!--  -->

@extends('layouts.client.app')

@section('title', 'Request A Signing')
@section('description', '')
@section('keyword', '')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Request</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('client') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('client/order') }}">Order</a>
                                    </li>
                                    <li class="breadcrumb-item active">Signing
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts flatpickr">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">PLACE AN ORDER SIGNING INFORMATION</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="isScheduled" class="custom-control-input" id="customCheck1" id="isScheduled" onclick="$('#RequestedAppointmentDateDiv').toggle(); $('#isScheduledTDiv').toggle(); $('#isScheduledDiv').toggle();" />
                                                        <label class="custom-control-label" for="customCheck1">
                                                           If you would like us to schedule the appointment time with Customer, please click here
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="RequestedAppointmentDateDiv">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Requested Appointment Date</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="fp-default" name="RequestedAppointmentDate" value="{{ date('m-d-Y') }}" data-date-format="m-d-Y" class="form-control flatpickr-basic" placeholder="MM-DD-YYYY" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isScheduledDiv" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Choose Scheduling Option</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                          <option value="">Select Option</option>
                                                          <option value="Today">Today</option>
                                                          <option value="Today Or Tomorrow">Today Or Tomorrow</option>
                                                          <option value="Tomorrow">Tomorrow</option>
                                                          <option value="Open">Open</option>
                                                          <option value="Other">Other</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isScheduledTDiv">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Requested Appointment Time</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="input-group">
                                                          <div class="input-group-prepend" style="">
                                                              <span class="input-group-text text-primary" id="basic-addon1"><i class="far fa-clock"></i></span>
                                                          </div>
                                                          <input type="text" id="fp-default" name="RequestedAppointmentTime" data-date-format="h:i K" class="form-control flatpickr-basic fp2" placeholder="" />

                                                      </div>
                                                      <!-- <input type="text" id="fp-default" name="RequestedAppointmentTime" data-date-format="h:i K" class="form-control flatpickr-basic fp2" placeholder="" /> -->
                                                      <!-- <div class="row">
                                                        <div class="col-4">
                                                          <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                              <option value="">Select Hour</option>
                                                              <option value="01">01</option>
                                                              <option value="01">02</option>
                                                              <option value="01">03</option>
                                                              <option value="01">01</option>
                                                              <option value="01">01</option>
                                                          </select>
                                                        </div>
                                                        <div class="col-4">
                                                          <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                              <option value="">Select Minute</option>
                                                              <option value="1">1</option>
                                                              <option value="1">2</option>
                                                              <option value="1">3</option>
                                                              <option value="1">4</option>
                                                          </select>
                                                        </div>
                                                        <div class="col-4">
                                                          <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                              <option value="">Select</option>
                                                              <option value="AM">AM</option>
                                                              <option value="PM">PM</option>
                                                          </select>
                                                        </div>
                                                      </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Type of Request:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="RequestType" name="Request_type" onchange="">
                                                          <option value="">Select Option</option>
                                                          <option value="1">New Signing</option>
                                                          <option value="2">Redraw</option>
                                                          <option value="3">Single Document</option>
                                                          <option value="4">Multiple Document</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Is this the 1st trip to meet with this Customer?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="isMeet" id="inlineRadio1" value="Yes" checked="" onclick="$('#tripDiv').hide()">
                                                            <label class="form-check-label" for="inlineRadio1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="isMeet" id="inlineRadio2" value="No" onclick="$('#tripDiv').toggle()">
                                                            <label class="form-check-label" for="inlineRadio2">NO</label>
                                                        </div>
                                                      </div>



                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="tripDiv" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Trip#:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="invoiceDBA" name="invoiceDBA" onchange="">
                                                          <option value="">1</option>
                                                          <option value="1">2</option>
                                                          <option value="2">3</option>
                                                          <option value="3">4</option>
                                                          <option value="4">5</option>
                                                          <option value="6">More than 5</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Invoice client:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="invoiceDBA" name="invoiceDBA" onchange="">
                                                          <option value="">Select Client</option>
                                                          <option value="1">Andrew Dains</option>
                                                          <option value="2">John</option>
                                                          <option value="3">Alex</option>
                                                          <option value="4">Durai</option>
                                                          <option value="5">Lucifer</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Method of Delivery for Documents</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc1" value="Yes" onclick="$('#estimatedTimeDiv').show()">
                                                            <label class="form-check-label" for="methodDeliveryDoc1">Email</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc2" value="No" onclick="$('#methodDeliveryDocTypeDiv').show();$('#estimatedTimeDiv').hide()">
                                                            <label class="form-check-label" for="methodDeliveryDoc2">Overnight</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc2" value="No" onclick="$('#methodDeliveryDocTypeDiv').show();$('#estimatedTimeDiv').hide()">
                                                            <label class="form-check-label" for="methodDeliveryDoc2">Courier</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc2" value="No" onclick="$('#methodDeliveryDocTypeDiv').show();$('#estimatedTimeDiv').hide()">
                                                            <label class="form-check-label" for="methodDeliveryDoc2">Customer Has Docs</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="estimatedTimeDiv" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Estimated Time</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="estimatedTime" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="methodDeliveryDocTypeDiv" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Delivery Document Type:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDocType" id="inlineRadio1" value="Yes" checked>
                                                            <label class="form-check-label" for="inlineRadio1">Docs To Notary</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDocType" id="inlineRadio2" value="No">
                                                            <label class="form-check-label" for="inlineRadio2">Docs To Customer</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Loan Package Type:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="" name="invoiceDBA" onchange="">
                                                          <option value="">Select Package</option>
                                                          <option value="1">Andrew Dains</option>
                                                          <option value="2">John</option>
                                                          <option value="3">Alex</option>
                                                          <option value="4">Durai</option>
                                                          <option value="5">Lucifer</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Lender Name:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="" name="invoiceDBA" onchange="">
                                                          <option value="">Select Lender</option>
                                                          <option value="1">Andrew Dains</option>
                                                          <option value="2">John</option>
                                                          <option value="3">Alex</option>
                                                          <option value="4">Durai</option>
                                                          <option value="5">Lucifer</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Escrow #</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="Escrow" placeholder="Escrow Number" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Customer First Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="first-name" class="form-control" name="CutomerFirstName" placeholder="First Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Customer Last Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="first-name" class="form-control" name="CutomerLastName" placeholder="Last Name" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="signer2" class="custom-control-input" id="signer2" onclick="$('#customerName2Div').toggle()" />
                                                        <label class="custom-control-label" for="signer2">
                                                           Add Another Signer
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="customerName2Div" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">2nd Customer Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="first-name" class="form-control" name="customerName2" placeholder="2nd Customer Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Marital Status</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="maritalStatus" id="maritalStatus1" value="Yes" >
                                                            <label class="form-check-label" for="maritalStatus1">Married</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="maritalStatus" id="maritalStatus2" value="No" >
                                                            <label class="form-check-label" for="maritalStatus2">Unmarried</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="maritalStatus" id="maritalStatus2" value="No" >
                                                            <label class="form-check-label" for="maritalStatus2">TBD</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Signing Location :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="signingLocation" id="signingLocation1" value="Yes" onclick="$('#businessNameDiv').hide()" checked>
                                                            <label class="form-check-label" for="signingLocation1">Residence</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="signingLocation" id="signingLocation2" value="No" onclick="$('#businessNameDiv').show()">
                                                            <label class="form-check-label" for="maritalStatus2">Business</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="businessNameDiv" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Business Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="customerName2" placeholder="Business Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Signing Address</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="address" placeholder="Address" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">City</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="address" placeholder="CITY NAME" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">ZIP</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="address" placeholder="ZIP CODE" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">State:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="" name="state" onchange="">
                                                          <option value="">Select State</option>
                                                          <option value="1">CA</option>
                                                          <option value="2">AZ</option>
                                                          <option value="3">NY</option>
                                                          <option value="4">Durai</option>
                                                          <option value="5">Lucifer</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="contact-info">Phone</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="number" id="contact-info" class="form-control" name="contact" placeholder="Phone" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Customer Email (Optional)</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="email" id="email-id" class="form-control" name="customer_email" placeholder="Email" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Comments/Special Instructions	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <textarea type="text" name="name" class="form-control" rows="3" cols="80" placeholder="Write..."></textarea>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Funds Due Escrow?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_fund_due_escrow" id="is_fund_due_escrow1" value="Yes" onclick="$('#isFundDueEscrowDiv').show()">
                                                            <label class="form-check-label" for="is_fund_due_escrow1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_fund_due_escrow" id="is_fund_due_escrow2" value="No" onclick="$('#isFundDueEscrowDiv').hide()">
                                                            <label class="form-check-label" for="is_fund_due_escrow2">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12" id="isFundDueEscrowDiv" style="display: none">
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label for="first-name">Fund Due Amount	</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="is_collect_due_HUD" id="is_collect_due_HUD1" value="Yes" onclick="$('#isFundDueEscrowDiv').show()">
                                                                <label class="form-check-label" for="is_collect_due_HUD1">
                                                                  <div class="input-group pl-1">
                                                                      <div class="input-group-prepend" style="height: 2rem">
                                                                          <span class="input-group-text" id="basic-addon1">$</span>
                                                                      </div>
                                                                      <input type="number" class="form-control" placeholder="0.00" aria-label="" aria-describedby="basic-addon1" id="fundValue" style="height: 2rem; width: 10rem">
                                                                  </div>
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="is_collect_due_HUD" id="is_collect_due_HUD2" value="HUD" onclick="$( "#fundValue" ).prop( "disabled", true );">
                                                                <label class="form-check-label" for="is_collect_due_HUD2"> Collect Amount Due On HUD</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label for="first-name">Method (check all that apply):	</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                        <div class="form-group">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkMethod[]" class="custom-control-input" id="checkMethod1" />
                                                                <label class="custom-control-label" for="checkMethod1">Personal Check</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkMethod[]" class="custom-control-input" id="checkMethod2" />
                                                                <label class="custom-control-label" for="checkMethod2">Cashiers Check</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkMethod[]" class="custom-control-input" id="checkMethod3" />
                                                                <label class="custom-control-label" for="checkMethod3">Customer To Wire</label>
                                                            </div>
                                                        </div>
                                                      </div>
                                                  </div>
                                              </div>
                                            </div>



                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Customer Signing in Trust?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="isTrust" id="isTrust1" value="Yes" onclick="$('#isTrustDiv').show(); $('#isTrust2Div').show()">
                                                            <label class="form-check-label" for="isTrust1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="isTrust" id="isTrust2" value="No" onclick="$('#isTrustDiv').hide(); ; $('#isTrust2Div').hide()">
                                                            <label class="form-check-label" for="isTrust2">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isTrustDiv" style="display:none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Do you want the customer to sign with trustee verbiage?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_trustee_verbiage" id="is_trustee_verbiage1" value="Yes" onclick="">
                                                            <label class="form-check-label" for="is_trustee_verbiage1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_trustee_verbiage" id="is_trustee_verbiage2" value="No" onclick="">
                                                            <label class="form-check-label" for="is_trustee_verbiage2">Customer signs their name only</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isTrust2Div" style="display:none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Notes About Trustee	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <textarea type="text" name="trustee_notes" class="form-control" rows="2" cols="" placeholder="Write Note..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Customer Signing with POA?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_borrower_signing_POA"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_borrower_signing_POA"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Esigning?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_amtrust_signing"  value="Yes" onclick="$('#isAmTrust').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_amtrust_signing"  value="No" onclick="$('#isAmTrust').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3" id="isAmTrust" style="display:none">
                                              <div class="form-group row pl-1">
                                                <div class="demo-inline-spacing">
                                                  <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="is_amtrust"  value="Yes" onclick="">
                                                      <label class="form-check-label">NYCB</label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="is_amtrust"  value="No" onclick="">
                                                      <label class="form-check-label">Flagstar</label>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Provident Funding Loan?	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_provident_funding_loan"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_provident_funding_loan"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Please Obtain Copy of Customer ID?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="cid"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="cid"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do You Want To Provide Loan Officer Contact Info?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_loan_officer"  value="Yes" onclick="$('#loanOfficerDiv').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_loan_officer"  value="No" onclick="$('#loanOfficerDiv').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="loanOfficerDiv" style="display:none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Phone Number	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="text" id="" class="form-control" name="" placeholder="Loan Officer Number" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you want to CC anyone on this one order? (Your Assistant, Co-worker, LO or Broker etc.)</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_clientEmailCC"  value="Yes" onclick="$('#clientEmailCCDiv').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_clientEmailCC"  value="No" onclick="$('#clientEmailCCDiv').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="clientEmailCCDiv" style="display: none">
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label for="first-name">Additional party email to CC</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                        <input type="text" id="" class="form-control" name="" placeholder="email" />
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Initial Order Request Email</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x1" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x1" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Notary Assigned Order Conformation and ProLink Invoice Email</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x2" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x2" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Signing Completed And Prolink Invoice Email</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x3" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x3" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you want to change the default return method?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_update_instructions"  value="Yes" onclick="$('#is_update_instructions').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_update_instructions"  value="No" onclick="$('#is_update_instructions').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="is_update_instructions" style="display: none">
                                              <div class="col-sm-9 offset-sm-3" style="padding-bottom: 2rem">
                                                  <p class="card-text" style="font-weight: 800">
                                                      Default Return Method:
                                                      Return Docs To:
                                                      ProLink
                                                      Attn: Adam Babish
                                                      TBD
                                                      San Jose California 95136

                                                      Method of shipping: FedEx
                                                      Choose Option: Priority Overnight (10:30 A.M.)
                                                      Account# : See Label
                                                    </p>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Would you like to have the notary hold docs for courier pick up?</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x5" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x5" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Mandatory For The notary to make the same day pick up?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="sameDayPickup"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="sameDayPickup"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Scanbacks Required?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_required"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_required"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you Want to Upload Docs Now?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_requiredc"  value="Yes" onclick="$('#allDocUploaded').show(); $('#Uploader').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_requiredc"  value="No" onclick="$('#allDocUploaded').hide(); $('#Uploader').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">How will Invoice Be Paid?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="form-check">
                                                          <div class="form-group">
                                                              <input class="form-check-input radio-sazal" type="radio" name="invoicePaidMethod"  value="Yes" onclick="$('#invoicePaidMethod').show()">
                                                              <label class="form-check-label radio-sazal-label">Inside Of Escrow (I will be responsible to pay the fee)</label>
                                                          </div>
                                                          <div class="form-group">
                                                              <input class="form-check-input radio-sazal" type="radio" name="invoicePaidMethod"  value="Yes" onclick="$('#invoicePaidMethod').show()">
                                                              <label class="form-check-label radio-sazal-label">Outside Of Escrow (Someone else will be responsible to pay the fee)</label>
                                                          </div>
                                                          <div class="form-group">
                                                              <input class="form-check-input radio-sazal" type="radio" name="invoicePaidMethod"  value="Yes" onclick="$('#invoicePaidMethod').show()">
                                                              <label class="form-check-label radio-sazal-label">Other</label>
                                                          </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3" id="allDocUploaded" style="display:none">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="allDocUploaded" id="upd" class="custom-control-input" />
                                                        <label class="custom-control-label" for="upd">
                                                           All Docs Uploaded
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-9 offset-sm-3" id="Uploader" style="display:none">
                                                <div class="form-group">
                                                    <form action="#" class="dropzone dropzone-area" id="dpz-multiple-files">
                                                        <div class="dz-message">Drop files here or click to upload.</div>
                                                    </form>
                                                </div>
                                            </div> -->
                                            <!-- multi file upload starts -->
                                            <!-- <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <p class="card-text" style="">
                                                              By default, dropzone is a multiple file uploader. User can either click on the dropzone area and select
                                                              multiple files or just drop all selected files in the dropzone area. This example is the most basic setup
                                                              for dropzone.
                                                            </p>
                                                            <form action="#" class="dropzone dropzone-area" id="dpz-multiple-files">
                                                                <div class="dz-message">Drop files here or click to upload.</div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- multi file upload ends -->

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you want to add Internall notes on this order?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_internal_notes"  value="Yes" onclick="$('#textarea').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_internal_notes"  value="No" onclick="$('#textarea').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3" id="textarea" style="display:none; padding-bottom: 3rem">
                                                <textarea name="nameh" class="form-control" rows="3" cols="" placeholder="Add some note..."></textarea>
                                            </div>



                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                                <button type="reset" class="btn btn-info mr-1">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->



            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });

      var date = new Date();

      var currentTime = date.getHours() + ':' + date.getMinutes() + ' AM';


      document.getElementsByClassName('fp2').value = currentTime;
      // alert(currentTime);

      $('.fp2').flatpickr({
        enableTime: true,
         noCalendar: true,
          dateFormat: "h:i K",
      });




    </script>
@endsection
