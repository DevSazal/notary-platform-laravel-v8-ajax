<!--  -->

@extends('layouts.client.app')

@section('title', 'My Orders - dataTableJS')
@section('description', '')
@section('keyword', '')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">My Orders</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="#">Order</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">Orders
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">

                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <p><a href="https://datatables.net/" target="_blank"></a></p>
                    </div>
                </div>


                <!-- Row grouping -->
                <section id="row-grouping-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-bottom">
                                    <h4 class="card-title">Orders</h4>
                                </div>
                                <div class="card-datatable">
                                    <table id="dataTable_orders" class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Escrow#</th>
                                                <th>Customer Name</th>
                                                <th>Appt Date & Time</th>
                                                <th>Tracking No</th>
                                                <th>Status </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td></td>
                                            <td><a href="#">123456</a></td>
                                            <td>Sazal Ahamed</td>
                                            <td><span class="text-success mr-25">10-05-17, 08:00 AM  Thursday</span></td>
                                            <td><i class="fas fa-shipping-fast"></i> E2566756</td>
                                            <td><div class="badge badge-light-primary mr-1">Assigned</div></td>
                                            <td>
                                              <a href="#" class="btn btn-sm btn-primary waves-effect waves-float waves-light">
                                                <i class="far fa-eye mr-25"></i> View
                                              </a>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td></td>
                                            <td><a href="#">123456</a></td>
                                            <td>AHAMED</td>
                                            <td><span class="text-success mr-25">10-05-17, 08:00 AM  Thursday</span></td>
                                            <td><i class="fas fa-shipping-fast"></i> E2566778</td>
                                            <td><div class="badge badge-light-success mr-1">On-Hold</div></td>
                                            <td>
                                              <a href="#" class="btn btn-sm btn-primary waves-effect waves-float waves-light">
                                                <i class="far fa-eye mr-25"></i> View
                                              </a>
                                            </td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Row grouping -->



            </div>
        </div>
@endsection

@section('js')
<!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ asset('theme/app-assets/js/scripts/tables/table-datatables-basic.js') }}"></script>-->
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        $('#dataTable_orders').DataTable();
      /*  var table = $('#dataTable_orders').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.orders.index') }}",
            // columns: [
            //     {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            //     {data: 'name', name: 'name'},
            //     {data: 'email', name: 'email'},
            //     {data: 'action', name: 'action', orderable: false, searchable: false},
            // ]
        });*/
      });
    </script>
@endsection
