<!--  -->

@extends('layouts.client.app')

@section('title', 'Request a Mousepad')
@section('description', '')
@section('keyword', '')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">


    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/editors/quill/quill.snow.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/editors/quill/quill.bubble.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-quill-editor.css') }}">

    <style media="screen">
      .custom-label{
        height: 4rem;
        background: #f5f2f4;
        padding: 0.4rem 1.5rem;
      }
      .custom-size{
        /* height: 20rem; */
        width: 20rem;
        margin-bottom: 2rem
      }
    </style>

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Request a Mousepad</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('client') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('client') }}">Client</a>
                                    </li>
                                    <li class="breadcrumb-item active">New Mousepad
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
              <!-- start -->
                <section class="basic-textarea">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"></h4>
                                </div>
                                <div class="card-body">
                                    <!-- <p class="card-text text-success mb-2"></p> -->

                                    <div class="col-12 mt-1 text-center" style="margin-bottom: 7rem">
                                      <img class="text-center rounded custom-size" src="{{ asset('theme/app-assets/images/mp.jpg') }}" alt="">
                                      <h3 class="text-primary text-center">
                                        Has your Mousepad seen better days?
                                      </h3>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 mb-2">

                                              <h5 class="text-primary text-center mb-2 custom-label">Request A Free ProLink Mousepad</h5>
                                              <form class="form form-vertical">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label class="font-weight-bolder">Quantity :</label>
                                                                <select class="form-control" id="" name="q" onchange="">

                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <button type="reset" class="btn btn-primary waves-effect waves-float waves-light mr-1"><i class="fas fa-puzzle-piece mr-25"></i> Request</button>

                                                        </div>
                                                    </div>
                                              </form>
                                        </div>
                                        <div class="col-md-4 mb-2">

                                              <h5 class="text-primary text-center mb-2 custom-label">Request a ProLink Brochure
                                                <br>(Email Over Snail Mail?)</h5>
                                              <form class="form form-vertical">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label class="font-weight-bolder">Method :</label>
                                                                <select class="form-control" id="" name="m" onchange="">

                                                                    <option value="1">Email</option>
                                                                    <option value="2">Snail Mail</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <button type="reset" class="btn btn-primary waves-effect waves-float waves-light mr-1"><i class="fas fa-puzzle-piece mr-25"></i> Request</button>

                                                        </div>
                                                    </div>
                                              </form>
                                        </div>
                                        <div class="col-md-4 mb-2">

                                              <h5 class="text-primary text-center mb-2 custom-label">
                                                Request a Cappuccino and a Puppy
                                              </h5>
                                              <form class="form form-vertical">
                                                    <div class="row">
                                                        <!-- <div class="col-12">
                                                            <div class="form-group">
                                                                <label class="font-weight-bolder">Method :</label>
                                                                <select class="form-control" id="" name="m" onchange="">

                                                                    <option value="1">Email</option>
                                                                    <option value="2">Snail Mail</option>
                                                                </select>
                                                            </div>
                                                        </div> -->

                                                        <div class="col-12 mt-2">
                                                            <button type="reset" class="btn btn-primary waves-effect waves-float waves-light mr-1"><i class="fas fa-puzzle-piece mr-25"></i> Request</button>

                                                        </div>
                                                    </div>
                                              </form>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--  end -->




            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>



    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>

    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/highlight.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/katex.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-quill-editor.js') }}"></script>
    <style>
        .ql-editor{
            min-height:400px;
        }
    </style>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });

      var date = new Date();

      var currentTime = date.getHours() + ':' + date.getMinutes() + ' AM';


      document.getElementsByClassName('fp2').value = currentTime;
      // alert(currentTime);

      $('.fp2').flatpickr({
        enableTime: true,
         noCalendar: true,
          dateFormat: "h:i K",
      });




    </script>
@endsection
