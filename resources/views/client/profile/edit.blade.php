<!--  -->

@extends('layouts.client.app')

@section('title', 'Edit Profile (Sazal Ahamed) ')
@section('description', '')
@section('keyword', '')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/file-uploaders/dropzone.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/pages/app-invoice-list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/pages/app-user.css') }}">

<style media="screen">
.custom-control-input[disabled] ~ .custom-control-label, .custom-control-input:disabled ~ .custom-control-label {
  color: #3c3c3c;
}
.media-body.link{
  font-size: 0.9rem;
  color: #ea54d1;
}
.media-body.link:hover{
  color: #7367f0;
  font-weight: bold;
  transition: .5s;
}
</style>
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0"> Profile</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('client/profile') }}">Profile</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">

              <section class="app-user-view">
                    <!-- User Card & Plan Starts -->
                    <div class="row">
                        <!-- User Card starts-->
                        <div class="col-xl-9 col-lg-8 col-md-7">
                          <div class="row">
                            <div class="col-md-12">


                            <div class="card user-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                            <div class="user-avatar-section">
                                                <div class="d-flex justify-content-start">
                                                    <img class="img-fluid rounded" src="{{ asset('theme/app-assets/images/avatars/9.png') }}" height="104" width="104" alt="User avatar" />
                                                    <div class="d-flex flex-column ml-1">
                                                        <div class="user-info mb-1">
                                                            <h4 class="mb-1 text-primary">
                                                              Ticor Title
                                                            </h4>
                                                            <span class="card-text" style="font-weight: 500">
                                                              Liz Barahona1
                                                              4304 El Dorado Street
                                                              <br>
                                                              Oakley, California 94561
                                                              <br>
                                                              Contra Costa
                                                            </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                            <div class="user-info-wrapper">

                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="star" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">SSN</span>
                                                    </div>
                                                    <p class="card-text mb-0">559-82-3494</p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="flag" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">State</span>
                                                    </div>
                                                    <p class="card-text mb-0">California</p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i class="fas fa-at mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Email</span>
                                                    </div>
                                                    <p class="card-text mb-0">lizbara04@yahoo.com</p>
                                                </div>
                                                <div class="d-flex flex-wrap">
                                                    <div class="user-info-title">
                                                        <i data-feather="phone" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Contact</span>
                                                    </div>
                                                    <p class="card-text mb-0">(123) 456-7890</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                              <div class="card">
                                  <!-- <div class="card-header">
                                      <h4 class="card-title">
                                        Administration Comments
                                      </h4>
                                  </div> -->
                                  <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <!-- <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Administration Comments	:</label>
                                                    </div> -->
                                                    <div class="col-sm-12">
                                                      <div class="row">
                                                        <div class="col-10">
                                                          <textarea type="text" name="adminComments" class="form-control" rows="3" placeholder="Write administration comments..." ></textarea>
                                                        </div>
                                                        <div class="col-2">
                                                          <button type="reset" class="btn btn-primary mr-1  text-center btn-block">Update</button>
                                                          <button type="reset" class="btn btn-outline-secondary  text-center btn-block">Save</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Update</button>
                                                <button type="reset" class="btn btn-outline-secondary">Save</button>
                                            </div> -->
                                        </div>
                                    </form>
                                  </div>
                              </div>
                            </div>


                            </div>

                        </div>
                        <!-- /User Card Ends-->

                        <!-- Plan Card starts-->
                        <div class="col-xl-3 col-lg-4 col-md-5">
                            <div class="card plan-card border-primary">
                                <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                                    <!-- <h5 class="mb-0">Current Plan</h5>
                                    <span class="badge badge-light-secondary" data-toggle="tooltip" data-placement="top" title="Expiry Date">July 22, <span class="nextYear"></span>
                                    </span> -->
                                </div>
                                <div class="card-body">


                                    <button onclick="window.location.href='{{ route('client.pro.index') }}'"  class="btn btn-primary text-center btn-block"><i class="far fa-id-badge mr-25"></i> Current Profile</button>
                                    <!-- <button class="btn btn-primary text-center btn-block">Send to QB</button> -->
                                    <!-- <button onclick="window.location.href='{{ route('client.pro.edit') }}'" class="btn btn-outline-success text-center btn-block"><i class="far fa-edit mr-25"></i> Edit Profile</button> -->
                                    <button onclick="window.location.href='{{ route('client.order.index') }}'" class="btn btn-outline-success text-center btn-block"><i class="far fa-file-alt mr-25"></i> My Orders</button>

                                </div>
                            </div>
                        </div>
                        <!-- /Plan CardEnds -->
                    </div>
                    <!-- User Card & Plan Ends -->

                    <!-- User Timeline & Permissions Starts -->
                    <div class="row">
                        <!-- Client Profile starts -->
                        <div class="col-md-7">
                          <!-- Basic Horizontal form layout section start -->
                          <section id="basic-horizontal-layouts flatpickr">
                              <div class="row">
                                  <div class="col-md-12 col-12">
                                      <div class="card">
                                          <div class="card-header">
                                              <h4 class="card-title">Edit Your Profile Information</h4>
                                          </div>
                                          <div class="card-body">
                                              <form class="form form-horizontal">
                                                  <div class="row">
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Title Company</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="" class="form-control" name="cName" value="Ticor Title" placeholder="" />
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>First Name</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="first-name" class="form-control" name="FirstName" value="Sazal" placeholder="First Name" />
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Last Name</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="last-name" class="form-control" name="LastName" placeholder="Last Name" />
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Role :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <select class="form-control" id="" name="state" onchange="">
                                                                    <option value=""> - Select Role - </option>
                                                                    <option value="1">Escrow Officer</option>
                                                                    <option value="2">Escrow Assistant</option>
                                                                    <option value="3">Assistant</option>
                                                                    <option value="4">Loan Officer</option>
                                                                    <option value="5">Other</option>
                                                                </select>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Address</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="" class="form-control" name="address" placeholder="Address" />
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-9 offset-sm-3">
                                                          <div class="form-group">
                                                              <input type="text" id="" class="form-control" name="address2" placeholder="" />
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>City</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="" class="form-control" name="address" placeholder="CITY NAME" />
                                                              </div>
                                                          </div>
                                                      </div>

                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>State:</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <select class="form-control" id="" name="state" onchange="">
                                                                    <option value=""> - Select State - </option>
                                                                    <option value="1">CA</option>
                                                                    <option value="2">AZ</option>
                                                                    <option value="3">NY</option>
                                                                    <option value="4">Durai</option>
                                                                    <option value="5">Lucifer</option>
                                                                </select>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>ZIP</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="" class="form-control" name="address" placeholder="ZIP CODE" />
                                                              </div>
                                                          </div>
                                                      </div>


                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Phone</label>
                                                              </div>
                                                              <div class="col-sm-9">

                                                                <div class="input-group">
                                                                    <div class="input-group-prepend" style="">
                                                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-alt"></i></span>
                                                                    </div>
                                                                    <input type="number" id="contact-info-icon" class="form-control" name="contact-icon" placeholder="Phone" />
                                                                </div>

                                                              </div>
                                                          </div>
                                                      </div>


                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Email</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="email" id="email-id" class="form-control" name="email" placeholder="Email" />
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Would you like all invoices also copied to another person?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <input class="form-check-input" type="radio" name="is_invoice_email_cc" id="" value="Yes" onclick="$('#isInvoiceEmailcc').show()">
                                                                      <label class="form-check-label">YES</label>
                                                                  </div>
                                                                  <div class="form-check form-check-inline">
                                                                      <input class="form-check-input" type="radio" name="is_invoice_email_cc" id="" value="No" onclick="$('#isInvoiceEmailcc').hide()">
                                                                      <label class="form-check-label">NO</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="isInvoiceEmailcc" style="display: none">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Enter Email Address</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="" class="form-control" name="email2" placeholder="CC Person Email " />
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Would you like all email contact from ProLink also copied to another person?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <input class="form-check-input" type="radio" name="is_email_contact_cc" id="" value="Yes" onclick="$('#isContactEmailcc').show()">
                                                                      <label class="form-check-label">YES</label>
                                                                  </div>
                                                                  <div class="form-check form-check-inline">
                                                                      <input class="form-check-input" type="radio" name="is_email_contact_cc" id="" value="No" onclick="$('#isContactEmailcc').hide()">
                                                                      <label class="form-check-label">NO</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="isContactEmailcc" style="display: none">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Enter Email Address</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="" class="form-control" name="email2" placeholder="CC Email " />
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Preferred Carrier :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <select class="form-control" id="" name="carrier" onchange="changeCourier(value)">
                                                                    <option value=""> - Select Carrier -</option>
                                                                    <option value="1">UPS</option>
                                                                    <option value="2">FedEx</option>
                                                                    <option value="3">DHL / Airborne</option>
                                                                </select>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="shippingRoot" style="display:none">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Shipping Method (priority, standard, 2 day)</label>
                                                              </div>
                                                              <div class="col-sm-9" id="shippingNull" style="">
                                                                <select class="form-control" id="" name="shippingNull" onchange="">

                                                                </select>
                                                              </div>
                                                              <div class="col-sm-9" id="shippingUPS" style="display: none">
                                                                <select class="form-control" id="" name="shippingUPS" onchange="">

                                                                    <option value="1">Next Day Air (10:30 A.M.)</option>
                                                                    <option value="2">Next Day Air Saver (3:00 P.M.)</option>
                                                                    <option value="3">2nd Day Air A.M. (12:00 Noon)</option>
                                                                </select>
                                                              </div>
                                                              <div class="col-sm-9" id="shippingFedex" style="display: none">
                                                                <select class="form-control" id="" name="shippingFedex" onchange="">

                                                                    <option value="1">Priority Overnight (10:30 A.M.)</option>
                                                                    <option value="2">Standard Overnight (3:00 P.M.)</option>
                                                                    <option value="3">2-Day (4:30 P.M.)</option>
                                                                </select>
                                                              </div>
                                                              <div class="col-sm-9" id="shippingDHL" style="display: none">
                                                                <select class="form-control" id="" name="shippingDHL" onchange="">

                                                                    <option value="1">DHL Overnight (12 noon)</option>
                                                                    <option value="2">DHL 2nd Day (5:00 P.M.)</option>
                                                                </select>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Shipping Account#</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="text" id="" class="form-control" name="ac" placeholder="" />
                                                              </div>
                                                          </div>
                                                      </div>




                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Uploaded PDF Files :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                          <a href="#">
                                                                            <div class="badge badge-light-primary mr-1">
                                                                                <i class="fas fa-link mr-25"></i>
                                                                                <span> Guideline 01.pdf</span>
                                                                            </div>
                                                                          </a>
                                                                          <a href="#">
                                                                            <div class="badge badge-light-primary mr-1">
                                                                                <i class="fas fa-link mr-25"></i>
                                                                                <span> Guideline 02.pdf</span>
                                                                            </div>
                                                                          </a>
                                                                    </div>
                                                              </div>
                                                          </div>
                                                      </div>










                                                      <div class="col-sm-9 offset-sm-3">
                                                          <button type="reset" class="btn btn-primary mr-1">Update</button>
                                                          <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                                      </div>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </section>
                          <!-- Basic Horizontal form layout section end -->
                        </div>
                        <!-- Client profile End -->
                        <!-- information starts -->
                        <div class="col-md-5">

                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Change Password</h4>
                                </div>
                                <div class="card-body">
                                  <form class="form form-vertical">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Current Password :</label>
                                                    <div class="input-group input-group-merge">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i data-feather="lock"></i></span>
                                                        </div>
                                                        <input type="password" class="form-control" name="cP" placeholder="Current Password" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>New Password :</label>
                                                    <div class="input-group input-group-merge">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                                        </div>
                                                        <input type="password" class="form-control" name="nP" placeholder="New Password" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Confirm Password :</label>
                                                    <div class="input-group input-group-merge">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                                        </div>
                                                        <input type="password" class="form-control" name="ncP" placeholder="Confirm Password" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="reset" class="btn btn-warning waves-effect waves-float waves-light mr-1"><i class="fas fa-cogs mr-25"></i> Change</button>
                                                <button type="reset" class="btn btn-outline-danger"><i class="fas fa-redo-alt mr-25"></i> Reset</button>
                                            </div>
                                        </div>
                                  </form>
                                </div>
                            </div>

                            <!-- single file upload starts -->

                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Update Your Photo</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        Would you like to change your profile photo or add new profile photo, so that others can identify you easily on our platform? Then, Let's upload a photo by dropping/dragging a file or just click for selecting. By default, you are able to upload a single image file for your profile.
                                        We are allowing for :
                                        <br>
                                        <code>maximum: 1</code> file should be upload.
                                    </p>
                                    <form action="#" class="dropzone dropzone-area" id="dpz-single-file">
                                        <div class="dz-message" style="font-size: 1rem">Drop a file here or click to upload.</div>
                                    </form>
                                    <div class="col-12 mt-2">
                                        <button type="reset" class="btn btn-primary waves-effect waves-float waves-light mr-1"><i class="fas fa-camera mr-25"></i> Save</button>
                                        <button type="reset" class="btn btn-outline-danger"><i class="fas fa-redo-alt mr-25"></i> Reset</button>
                                    </div>
                                </div>
                            </div>

                    <!-- single file upload ends -->



                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title mb-2">Resent Orders</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="timeline">
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h4>Closed</h4>
                                                    <span class="timeline-event-time" title="19-11-2020">19 Nov, 2020</span>
                                                </div>
                                                <p>Borrower: <span class="text-success">Gordon Ignacio</span></p>
                                                <div class="media align-items-center">
                                                    <a href="#"><div class="media-body link" style="">Order ID# 62092</div></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h5>Pending - Not Responded</h5>
                                                    <span class="timeline-event-time">2 Nov, 2020</span>
                                                </div>
                                                <p>This is a test sample...</p>
                                                <div class="media align-items-center">

                                                        <a href="#"><div class="media-body link">Order ID# 62085</div></a>

                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h4>On Hold</h4>
                                                    <span class="timeline-event-time" title="19-11-2020">1 Nov, 2020</span>
                                                </div>
                                                <p>Borrower: <span class="text-success">Jeffrey Taylor</span></p>
                                                <div class="media align-items-center">
                                                    <a href="#"><div class="media-body link" style="">Order ID# 61037</div></a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- information End -->


                    </div>
                    <!-- User Timeline & Permissions Ends -->


                </section>



            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });

      function changeCourier($i) {
        // alert($i);
        $('#shippingRoot').show();

        if ($i == 1) {
          $('#shippingUPS').show();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 2) {
          $('#shippingUPS').hide();
          $('#shippingFedex').show();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 3) {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').show();
          $('#shippingNull').hide();
        } else {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').show();

          $('#shippingRoot').hide();
        }
       }
    </script>
@endsection
