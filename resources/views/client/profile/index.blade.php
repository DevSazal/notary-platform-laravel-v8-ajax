<!--  -->

@extends('layouts.client.app')

@section('title', 'Profile (Sazal Ahamed) ')
@section('description', '')
@section('keyword', '')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/pages/app-invoice-list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/pages/app-user.css') }}">

<style media="screen">
.custom-control-input[disabled] ~ .custom-control-label, .custom-control-input:disabled ~ .custom-control-label {
  color: #3c3c3c;
}
.media-body.link{
  font-size: 0.9rem;
  color: #ea54d1;
}
.media-body.link:hover{
  color: #7367f0;
  font-weight: bold;
  transition: .5s;
}
</style>
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0"> Profile</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item active">
                                      <a href="{{ url('admin/client') }}">Profile</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">Profile
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">

              <section class="app-user-view">
                    <!-- User Card & Plan Starts -->
                    <div class="row">
                        <!-- User Card starts-->
                        <div class="col-xl-9 col-lg-8 col-md-7">
                          <div class="row">
                            <div class="col-md-12">


                            <div class="card user-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                            <div class="user-avatar-section">
                                                <div class="d-flex justify-content-start">
                                                    <img class="img-fluid rounded" src="{{ asset('theme/app-assets/images/avatars/9.png') }}" height="104" width="104" alt="User avatar" />
                                                    <div class="d-flex flex-column ml-1">
                                                        <div class="user-info mb-1">
                                                            <h4 class="mb-1 text-primary">
                                                              Ticor Title
                                                            </h4>
                                                            <span class="card-text" style="font-weight: 500">
                                                              Liz Barahona1
                                                              4304 El Dorado Street
                                                              <br>
                                                              Oakley, California 94561
                                                              <br>
                                                              Contra Costa
                                                            </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                            <div class="user-info-wrapper">

                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="star" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">SSN</span>
                                                    </div>
                                                    <p class="card-text mb-0">559-82-3494</p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="flag" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">State</span>
                                                    </div>
                                                    <p class="card-text mb-0">California</p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i class="fas fa-at mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Email</span>
                                                    </div>
                                                    <p class="card-text mb-0">lizbara04@yahoo.com</p>
                                                </div>
                                                <div class="d-flex flex-wrap">
                                                    <div class="user-info-title">
                                                        <i data-feather="phone" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Contact</span>
                                                    </div>
                                                    <p class="card-text mb-0">(123) 456-7890</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                              <div class="card">
                                  <!-- <div class="card-header">
                                      <h4 class="card-title">
                                        Administration Comments
                                      </h4>
                                  </div> -->
                                  <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <!-- <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Administration Comments	:</label>
                                                    </div> -->
                                                    <div class="col-sm-12">
                                                      <div class="row">
                                                        <div class="col-10">
                                                          <textarea type="text" name="adminComments" class="form-control" rows="3" placeholder="Write administration comments..." disabled></textarea>
                                                        </div>
                                                        <div class="col-2">
                                                          <button type="reset" class="btn btn-primary mr-1  text-center btn-block">Update</button>
                                                          <button type="reset" class="btn btn-outline-secondary  text-center btn-block">Save</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Update</button>
                                                <button type="reset" class="btn btn-outline-secondary">Save</button>
                                            </div> -->
                                        </div>
                                    </form>
                                  </div>
                              </div>
                            </div>


                            </div>

                        </div>
                        <!-- /User Card Ends-->

                        <!-- Plan Card starts-->
                        <div class="col-xl-3 col-lg-4 col-md-5">
                            <div class="card plan-card border-primary">
                                <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                                    <!-- <h5 class="mb-0">Current Plan</h5>
                                    <span class="badge badge-light-secondary" data-toggle="tooltip" data-placement="top" title="Expiry Date">July 22, <span class="nextYear"></span>
                                    </span> -->
                                </div>
                                <div class="card-body">


                                    <button class="btn btn-primary text-center btn-block">Send to QB</button>
                                    <button onclick="window.location.href='{{ route('client.pro.edit') }}'" class="btn btn-outline-success text-center btn-block"><i class="far fa-edit mr-25"></i> Edit Profile</button>

                                </div>
                            </div>
                        </div>
                        <!-- /Plan CardEnds -->
                    </div>
                    <!-- User Card & Plan Ends -->

                    <!-- User Timeline & Permissions Starts -->
                    <div class="row">
                        <!-- Client Profile starts -->
                        <div class="col-md-7">
                          <!-- Basic Horizontal form layout section start -->
                          <section id="basic-horizontal-layouts flatpickr">
                              <div class="row">
                                  <div class="col-md-12 col-12">
                                      <div class="card">
                                          <div class="card-header">
                                              <h4 class="card-title">
                                                <!-- Client Profile Information -->
                                              </h4>
                                          </div>
                                          <div class="card-body pro">
                                              <form class="form form-horizontal">
                                                  <div class="row">

                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Title Company :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>	Ticor Title</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>First Name :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Adam</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Last Name :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Babish</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Role :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-primary mr-1">
                                                                          <span>Escrow Officer</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Address</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Liz Barahona1 4304 El Dorado Street</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-9 offset-sm-3">
                                                          <div class="form-group">
                                                              <div class="demo-inline-spacing">
                                                                <p>Oakley, California 94561</p>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>City</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Oakley</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>

                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>State:</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>California</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>ZIP</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>94561</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Country:</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>United State</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Phone</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-primary mr-1">
                                                                          <i class="fas fa-phone-alt mr-25"></i>
                                                                          <span>925-625-5266</span>
                                                                      </div>
                                                                      <!-- <span style="color: red; font-size: 85%; margin-top: .3rem;">
                                                                        (Preferred)
                                                                      </span> -->
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>



                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Email</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-info mr-1">
                                                                          <i class="far fa-envelope mr-25"></i>
                                                                          <span> lizbara04@yahoo.com</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Would you like all invoices also copied to another person?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                      <label class="form-check-label"><div class="badge badge-pill badge-dark">YES</div></label>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">CC Person Email (Optional)</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-dark mr-1">
                                                                          <i class="far fa-envelope mr-25"></i>
                                                                          <span> lizbara04@yahoo.com</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Would you like all email contact from ProLink also copied to another person?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                      <label class="form-check-label"><div class="badge badge-pill badge-dark">YES</div></label>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">CC Email (Optional)</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-dark mr-1">
                                                                          <i class="far fa-envelope mr-25"></i>
                                                                          <span> lizbara04@yahoo.com</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="first-name">EIN</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 26</span>
                                                                      </div>
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 7654321</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="first-name">SSN</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 263</span>
                                                                      </div>
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 58</span>
                                                                      </div>
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 8697</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Preferred Carrier :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-primary mr-1">
                                                                          <i class="fas fa-shipping-fast mr-25"></i>
                                                                          <span> FedEx</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Shipping Method (priority, standard, 2 day) :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="text-primary mr-1">
                                                                          <i class="far fa-clock mr-25"></i>
                                                                          <span> Next Day Air Saver (3:00 P.M.)</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Shipping Account#</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-warning mr-1">
                                                                          <span> 15005550006</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Uploaded PDF Files :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                          <a href="#">
                                                                            <div class="badge badge-light-primary mr-1">
                                                                                <i class="fas fa-link mr-25"></i>
                                                                                <span> Guideline 01.pdf</span>
                                                                            </div>
                                                                          </a>
                                                                          <a href="#">
                                                                            <div class="badge badge-light-primary mr-1">
                                                                                <i class="fas fa-link mr-25"></i>
                                                                                <span> Guideline 02.pdf</span>
                                                                            </div>
                                                                          </a>
                                                                    </div>
                                                              </div>
                                                          </div>
                                                      </div>



                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </section>
                          <!-- Basic Horizontal form layout section end -->
                        </div>
                        <!-- Client profile End -->
                        <!-- information starts -->
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title mb-2">Resent Orders</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="timeline">
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h4>Closed</h4>
                                                    <span class="timeline-event-time" title="19-11-2020">19 Nov, 2020</span>
                                                </div>
                                                <p>Borrower: <span class="text-success">Gordon Ignacio</span></p>
                                                <div class="media align-items-center">
                                                    <a href="#"><div class="media-body link" style="">Order ID# 62092</div></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h5>Pending - Not Responded</h5>
                                                    <span class="timeline-event-time">2 Nov, 2020</span>
                                                </div>
                                                <p>This is a test sample...</p>
                                                <div class="media align-items-center">

                                                        <a href="#"><div class="media-body link">Order ID# 62085</div></a>

                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h4>On Hold</h4>
                                                    <span class="timeline-event-time" title="19-11-2020">1 Nov, 2020</span>
                                                </div>
                                                <p>Borrower: <span class="text-success">Jeffrey Taylor</span></p>
                                                <div class="media align-items-center">
                                                    <a href="#"><div class="media-body link" style="">Order ID# 61037</div></a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- information End -->


                    </div>
                    <!-- User Timeline & Permissions Ends -->


                </section>



            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });
    </script>
@endsection
