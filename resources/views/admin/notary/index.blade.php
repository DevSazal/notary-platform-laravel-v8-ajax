<!--  -->

@extends('layouts.app')

@section('title', 'Notaries')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Notaries</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="{{ url('admin/order') }}">Order</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">Notaries
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">



                <!-- Row grouping -->
                <section id="row-grouping-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-bottom">
                                    <h4 class="card-title">Notary List</h4>
                                </div>
                                <div class="card-datatable">
                                    <table id="dataTable_orders" class="table table-striped table-bordered dataTable yajra-datatable">
                                      <thead>
                                        <tr>

                                        <th>Notary ID</th>
                                        <th>Notary Name</th>
                                        <th>Strikes</th>
                                        <th>City</th>
                                        <th>Contact Number</th>
                                        <th>Availability</th>
                                        <th>Status</th>
                                        <th>Color</th>
                                        <th><i class="fa fa-cogs"></i></th>
                                        </tr>
                                      </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Row grouping -->


            </div>
        </div>
@endsection

@section('js')
<!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ asset('theme/app-assets/js/scripts/tables/table-datatables-basic.js') }}"></script>-->
    <!-- END: Page JS-->
    <script type="text/javascript">


    $(function () {

      var table = $('#dataTable_orders').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
            type : "post",
            url: "{{ route('admin.notary.index.list') }}",

          },
          columns: [
              {data: 'notaryID', name: 'Notary ID'},
              {data: 'notaryName', name: 'Notary Name'},
              {data: 'strikes', name: 'Strikes'},
              {data: 'city', name: 'City'},
              {data: 'contact', name: 'Contact Number'},
              {data: 'availability', name: 'Availability'},
              {data: 'status', name: 'Status'},
              {data: 'color', name: 'Color'},
              {data: 'actions', name: 'Actions'},
          ]
      });



    });
    </script>
@endsection
