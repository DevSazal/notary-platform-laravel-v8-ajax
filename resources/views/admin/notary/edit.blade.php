<!--  -->

@extends('layouts.app')

@section('title', 'Edit Notary (#'.$notary->notaryID.') ')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Edit Notary</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('admin/notary') }}">Notary</a>
                                    </li>
                                    <li class="breadcrumb-item active">#{{ $notary->notaryID }}
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts flatpickr">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Notary Information</h4>
                                </div>
                                <div class="card-body">


                                  <!--Closable Alerts start -->
                                  @if(\Session::has('job'))
                                    <div class="demo-spacing-1 mb-2">
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <div class="alert-body">
                                                <strong>Success!</strong> {{ session('job') }}.
                                            </div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                  @endif
                                  <!--Closable Alerts end -->
                                  <!--Closable Danger Alerts start -->
                                  @if(\Session::has('job_failed'))
                                    <div class="demo-spacing-1 mb-2">
                                        <div class="alert alert-danger mt-1 alert-validation-msg alert-dismissible fade show" role="alert">
                                            <div class="alert-body">
                                                <i data-feather="info" class="mr-50 align-middle"></i>
                                                <span><strong>Opps!</strong> {{ session('job_failed') }}</span>
                                            </div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                  @endif
                                  <!--Closable Danger Alerts end -->


                                    <form action="{{ route('admin.notary.update', $notary->notaryID ) }}" method="POST" class="form form-horizontal">
                                      @method('PUT')
                                      @csrf
                                      <input type="hidden" name="notaryID" value="{{ $notary->notaryID }}">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>First Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="first-name" class="form-control @error('firstName') is-invalid @enderror" name="firstName" value="{{ old('firstName') ? old('firstName') : $notary->firstName  }}" placeholder="First Name" />
                                                        @error('firstName')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Last Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="last-name" class="form-control @error('lastName') is-invalid @enderror" name="lastName" value="{{ old('lastName') ? old('lastName') : $notary->lastName  }}" placeholder="Last Name" />
                                                        @error('lastName')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Address</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="address" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') ? old('address') : $notary->address1  }}" placeholder="Address" />
                                                        @error('address')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control @error('address2') is-invalid @enderror" name="address2" value="{{ old('address2') ? old('address2') : $notary->address2  }}" placeholder="" />
                                                    @error('address2')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>City</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="city" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') ? old('city') : $notary->city  }}" placeholder="CITY NAME" />
                                                        @error('city')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>State:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select id="state" class="form-control @error('state') is-invalid @enderror" name="state" onchange="">
                                                          <option value=""> - Select State -</option>
                                                          @foreach($states as $state)
                                                            <option value="{{ $state->stateID }}"
                                                              @if (old('state') == $state->stateID ) {{ 'selected' }} @elseif($notary->state == $state->stateID) {{ 'selected' }} @endif>{{ $state->stateName }}
                                                            </option>
                                                          @endforeach
                                                      </select>
                                                      @error('state')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>ZIP</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="zipCode" class="form-control @error('zipCode') is-invalid @enderror" name="zipCode" value="{{ old('zipCode') ? old('zipCode') : $notary->zip1  }}" placeholder="ZIP CODE" />
                                                        @error('zipCode')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Home Phone</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <div class="demo-inline-spacing">
                                                          <!-- <div class="form-check form-check-inline responsive-form">
                                                              <div class="input-group">
                                                                  <div class="input-group-prepend" style="">
                                                                      <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-volume"></i></span>
                                                                  </div>
                                                                  <input type="number" class="form-control @error('homePhoneXX') is-invalid @enderror" name="homePhoneXX" value="{{ old('homePhoneXX') ? old('homePhoneXX') : $notary->homePhone1  }}" placeholder="Home Phone" />
                                                                  @error('homePhoneXX')
                                                                      <div class="invalid-feedback">
                                                                          {{ $message }}
                                                                      </div>
                                                                  @enderror
                                                              </div>
                                                          </div> -->
                                                          <div class="row">
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('homePhone') is-invalid @enderror" name="homePhone" value="{{ old('homePhone') ? old('homePhone') : $notary->homePhone1  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" />
                                                              @error('homePhone')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('homePhone2') is-invalid @enderror" name="homePhone2" value="{{ old('homePhone2') ? old('homePhone2') : $notary->homePhone2  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" />
                                                              @error('homePhone2')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('homePhone3') is-invalid @enderror" name="homePhone3" value="{{ old('homePhone3') ? old('homePhone3') : $notary->homePhone3  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" />
                                                              @error('homePhone3')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <input class="form-check-input" type="radio" name="phonePrefered" id="isActive1" value="1" {{ $notary->phonePrefered == '1' ? 'checked' : '' }} >
                                                                      <label class="form-check-label" for="isActive1"></label>
                                                                      @error('phonePrefered')
                                                                          <div class=" mt-0 ml-1 d-block invalid-feedback">
                                                                              {{ $message }}
                                                                          </div>
                                                                      @enderror
                                                                  </div>
                                                                </div>
                                                            </div>
                                                          </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Business Phone</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <div class="demo-inline-spacing">

                                                          <div class="row">
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('businessPhone') is-invalid @enderror" name="businessPhone" value="{{ old('businessPhone') ? old('businessPhone') : $notary->businessPhone1  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" />
                                                              @error('businessPhone')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('businessPhone2') is-invalid @enderror" name="businessPhone2" value="{{ old('businessPhone2') ? old('businessPhone2') : $notary->businessPhone2  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" />
                                                              @error('businessPhone2')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('businessPhone3') is-invalid @enderror" name="businessPhone3" value="{{ old('businessPhone3') ? old('businessPhone3') : $notary->businessPhone3  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" />
                                                              @error('businessPhone3')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <input class="form-check-input" type="radio" name="phonePrefered" id="isActive2" value="2" {{ $notary->phonePrefered == '2' ? 'checked' : '' }} >
                                                                      <label class="form-check-label" for="isActive2"></label>
                                                                      @error('phonePrefered')
                                                                          <div class=" mt-0 ml-1 d-block invalid-feedback">
                                                                              {{ $message }}
                                                                          </div>
                                                                      @enderror
                                                                  </div>
                                                                </div>
                                                            </div>
                                                          </div>



                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Cell Phone</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <div class="demo-inline-spacing">

                                                          <div class="row">
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('cellPhone') is-invalid @enderror" name="cellPhone" value="{{ old('cellPhone') ? old('cellPhone') : $notary->cellPhone1  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" />
                                                              @error('cellPhone')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('cellPhone2') is-invalid @enderror" name="cellPhone2" value="{{ old('cellPhone2') ? old('cellPhone2') : $notary->cellPhone2  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" />
                                                              @error('cellPhone2')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                              <input type="number" class="form-control @error('cellPhone3') is-invalid @enderror" name="cellPhone3" value="{{ old('cellPhone3') ? old('cellPhone3') : $notary->cellPhone3  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" />
                                                              @error('cellPhone3')
                                                                  <div class="invalid-feedback">
                                                                      {{ $message }}
                                                                  </div>
                                                              @enderror
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <input class="form-check-input" type="radio" name="phonePrefered" id="isActive3" value="3" {{ $notary->phonePrefered == '3' ? 'checked' : '' }} >
                                                                      <label class="form-check-label" for="isActive3"></label>
                                                                      @error('phonePrefered')
                                                                          <div class=" mt-0 ml-1 d-block invalid-feedback">
                                                                              {{ $message }}
                                                                          </div>
                                                                      @enderror
                                                                  </div>
                                                                </div>
                                                            </div>
                                                          </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Email</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ? old('email') : $notary->email  }}" placeholder="Email" />
                                                        @error('email')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Alternate Email (Optional)</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="email" class="form-control @error('alternateEmail') is-invalid @enderror" name="alternateEmail" value="{{ old('alternateEmail') ? old('alternateEmail') : $notary->alternateEmail  }}" placeholder="Alternate Email" />
                                                        @error('alternateEmail')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">EIN</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="row">
                                                        <div class="col-3">
                                                          <input type="number" class="form-control @error('ein1') is-invalid @enderror" name="ein1" value="{{ old('ein1') ? old('ein1') : $notary->ein1  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" />
                                                          @error('ein1')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                        </div>
                                                        <div class="col-9">
                                                          <input type="number" class="form-control @error('ein2') is-invalid @enderror" name="ein2" value="{{ old('ein2') ? old('ein2') : $notary->ein2  }}" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==7) return false;" />
                                                          @error('ein2')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">SSN</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="row">
                                                        <div class="col-3">
                                                          <input type="number" class="form-control @error('ssn1') is-invalid @enderror" name="ssn1" value="{{ old('ssn1') ? old('ssn1') : $notary->ssn1  }}" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" placeholder="" />
                                                          @error('ssn1')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                        </div>
                                                        <div class="col-2">
                                                          <input type="number" class="form-control @error('ssn2') is-invalid @enderror" name="ssn2" value="{{ old('ssn2') ? old('ssn2') : $notary->ssn2  }}" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" placeholder="" />
                                                          @error('ssn2')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                        </div>
                                                        <div class="col-7">
                                                          <input type="number" class="form-control @error('ssn3') is-invalid @enderror" name="ssn3" value="{{ old('ssn3') ? old('ssn3') : $notary->ssn3  }}" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" placeholder="" />
                                                          @error('ssn3')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Is Overnight Delivery Address Different than Above?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="overNightDelivery" id="overNightDelivery1" value="y" onclick="$('#isDeliveryDiv').show()" {{ old('overNightDelivery') =='y' ? 'checked' : ($notary->overNightDelivery == 'y' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="overNightDelivery1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="overNightDelivery" id="overNightDelivery2" value="n" onclick="$('#isDeliveryDiv').hide()" {{ old('overNightDelivery') =='n' ? 'checked' : ($notary->overNightDelivery == 'n' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="overNightDelivery2">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="isDeliveryDiv" style=" @if(old('overNightDelivery') =='y') '' @elseif($notary->overNightDelivery == 'y') '' @else display: none @endif">

                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Overnight Delivery Address for Documents</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" id="overNightDelAddress" class="form-control @error('overNightDelAddress1') is-invalid @enderror" name="overNightDelAddress1" value="{{ old('overNightDelAddress1') ? old('overNightDelAddress1') : $notary->overNightDelAddress1  }}" placeholder="Address" />
                                                          @error('overNightDelAddress1')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-sm-9 offset-sm-3">
                                                  <div class="form-group" style="margin-left: -1rem;margin-right: -1rem;">
                                                      <input type="text" class="form-control @error('overNightDelAddress2') is-invalid @enderror" name="overNightDelAddress2" value="{{ old('overNightDelAddress2') ? old('overNightDelAddress2') : $notary->overNightDelAddress2  }}" placeholder="" />
                                                      @error('overNightDelAddress2')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                  </div>
                                              </div>
                                              <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>City</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" id="overNightDelCity" class="form-control @error('overNightDelCity') is-invalid @enderror" name="overNightDelCity" value="{{ old('overNightDelCity') ? old('overNightDelCity') : $notary->overNightDelCity  }}" placeholder="CITY NAME" />
                                                          @error('overNightDelCity')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>

                                              <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>State:</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                        <select id="overNightDelState" class="form-control @error('overNightDelState') is-invalid @enderror" id="" name="overNightDelState" onchange="">
                                                            <option value=""> - Select State -</option>
                                                            @foreach($states as $state)
                                                              <option value="{{ $state->stateID }}" @if (old('overNightDelState') == $state->stateID ) {{ 'selected' }} @elseif($notary->overNightDelState == $state->stateID) {{ 'selected' }} @endif>{{ $state->stateName }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('overNightDelState')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>ZIP</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" id="overNightDelZip" class="form-control @error('overNightDelZip1') is-invalid @enderror" name="overNightDelZip1" value="{{ old('overNightDelZip1') ? old('overNightDelZip1') : $notary->overNightDelZip1  }}" placeholder="ZIP CODE" />
                                                          @error('overNightDelZip1')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>

                                            </div>


                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Can you receive documents via email?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="receiveDocEmail1" name="receiveDocEmail" value="1" {{ old('receiveDocEmail') =='1' ? 'checked' : ($notary->receiveDocEmail == '1' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="receiveDocEmail1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="receiveDocEmail2" name="receiveDocEmail" value="0" {{ old('receiveDocEmail') =='0' ? 'checked' : ($notary->receiveDocEmail == '0' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="receiveDocEmail2">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Do you have a laser printer?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="laserPrinter1" name="laserPrinter" value="1" {{ old('laserPrinter') =='1' ? 'checked' : ($notary->laserPrinter == '1' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="laserPrinter1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="laserPrinter2" name="laserPrinter" value="0" {{ old('laserPrinter') =='0' ? 'checked' : ($notary->laserPrinter == '0' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="laserPrinter2">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Can you print legal size pages?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="legalSizePage1" name="legalSizePage" value="1" {{ old('legalSizePage') =='1' ? 'checked' : ($notary->legalSizePage == '1' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="legalSizePage1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="legalSizePage2" name="legalSizePage" value="0" {{ old('legalSizePage') =='0' ? 'checked' : ($notary->legalSizePage == '0' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="legalSizePage2">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Availability</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control @error('availability') is-invalid @enderror" name="availability" onchange="">
                                                          <option value="">Select Availability</option>
                                                          <option value="Anytime" @if (old('availability') == "Anytime") {{ 'selected' }} @elseif($notary->availability == "Anytime") {{ 'selected' }}  @endif>Anytime</option>
                                                          <option value="Daytime Only" @if (old('availability') == "Daytime Only") {{ 'selected' }} @elseif($notary->availability == "Anytime") {{ 'selected' }}  @endif>Daytime Only</option>
                                                          <option value="Evening Only" @if (old('availability') == "Evening Only") {{ 'selected' }} @elseif($notary->availability == "Evening Only") {{ 'selected' }}  @endif>Evening Only</option>
                                                          <option value="Weekend Only" @if (old('availability') == "Weekend Only") {{ 'selected' }} @elseif($notary->availability == "Weekend Only") {{ 'selected' }}  @endif>Weekend Only</option>
                                                          <option value="Evening & Weekends" @if (old('availability') == "Evening & Weekends") {{ 'selected' }} @elseif($notary->availability == "Evening & Weekends") {{ 'selected' }}  @endif>Evening & Weekends</option>
                                                          <option value="Daytime & Weekends" @if (old('availability') == "Daytime & Weekends") {{ 'selected' }} @elseif($notary->availability == "Daytime & Weekends") {{ 'selected' }}  @endif>Daytime & Weekends</option>
                                                      </select>
                                                      @error('availability')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Do you speak any foreign languages?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="foreignLanguage1" name="foreignLanguage" value="y"  onclick="$('#haveForeignDiv').show()" {{ old('foreignLanguage') =='y' ? 'checked' : ($notary->foreignLanguage == 'y' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="foreignLanguage1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="foreignLanguage2" name="foreignLanguage" value="n"  onclick="$('#haveForeignDiv').hide()" {{ old('foreignLanguage') =='n' ? 'checked' : ($notary->foreignLanguage == 'n' ? 'checked' : '') }} >
                                                            <label class="form-check-label" for="foreignLanguage2">NO</label>
                                                        </div>
                                                      </div>
                                                      @error('foreignLanguage')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="haveForeignDiv" style=" @if(old('foreignLanguage') =='y') '' @elseif($notary->foreignLanguage == 'y') '' @else display: none @endif">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Languages :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">

                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="Spanish" id="Lang1" @if($notary->spanish == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang1">Spanish</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="French" id="Lang2" @if($notary->french == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang2">French</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="German" id="Lang3" @if($notary->german == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang3">German</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="Chinese" id="Lang4" @if($notary->chinese == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang4">Chinese</label>
                                                        </div>

                                                      </div>
                                                      <div class="demo-inline-spacing">

                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="Japanese" id="Lang5" @if($notary->japanese == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang5">Japanese</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="Italian" id="Lang6" @if($notary->italian == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang6">Italian</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="Russian" id="Lang7" @if($notary->russian == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang7">Russian</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="checkLanguage[]" class="custom-control-input" value="Hindi" id="Lang8" @if($notary->hindi == 1) checked @endif />
                                                            <label class="custom-control-label" for="Lang8">Hindi</label>
                                                        </div>

                                                      </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="contact-info">How long have you been a notary?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control @error('howLongNotary') is-invalid @enderror" name="howLongNotary" value="{{ old('howLongNotary') ? old('howLongNotary') : $notary->howLongNotary  }}" placeholder="" />
                                                        @error('howLongNotary')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Do you have experience in signing loan documents?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="haveExperience1" name="haveExperience" value="y" {{ old('haveExperience') =='y' ? 'checked' : ($notary->haveExperience == 'y' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="haveExperience1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="haveExperience2" name="haveExperience" value="n" {{ old('haveExperience') =='n' ? 'checked' : ($notary->haveExperience == 'n' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="haveExperience2">NO</label>
                                                        </div>
                                                      </div>
                                                      @error('haveExperience')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>May we contact your references?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="refer1" name="refer" value="1" {{ old('refer') =='1' ? 'checked' : ($notary->refer == '1' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="refer1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="refer2" name="refer" value="0" {{ old('refer') =='0' ? 'checked' : ($notary->refer == '0' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="refer2">NO</label>
                                                        </div>
                                                      </div>
                                                      @error('refer')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Approximately how many loan documents have you signed?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="howmanyLoanDocuments1" name="howmanyLoanDocuments" value="0-50" {{ old('howmanyLoanDocuments') =='0-50' ? 'checked' : ($notary->howmanyLoanDocuments == '0-50' ? 'checked' : '') }}>
                                                            <label class="form-check-label" id="howmanyLoanDocuments1">0-50</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="howmanyLoanDocuments2" name="howmanyLoanDocuments" value="51-100" {{ old('howmanyLoanDocuments') =='51-100' ? 'checked' : ($notary->howmanyLoanDocuments == '51-100' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="howmanyLoanDocuments2">51-100</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="howmanyLoanDocuments3" name="howmanyLoanDocuments" value="100+" {{ old('howmanyLoanDocuments') =='100+' ? 'checked' : ($notary->howmanyLoanDocuments == '100+' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="howmanyLoanDocuments3">100+</label>
                                                        </div>
                                                      </div>
                                                      @error('howmanyLoanDocuments')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Notary Commission :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control @error('notaryCommission') is-invalid @enderror" name="notaryCommission" value="{{ old('notaryCommission') ? old('notaryCommission') : $notary->notaryCommission  }}" placeholder="" />
                                                        @error('notaryCommission')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Notary Commission Expiration :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control @error('notaryCommissionExpiration') is-invalid @enderror" name="notaryCommissionExpiration" value="{{ old('notaryCommissionExpiration') ? old('notaryCommissionExpiration') : $notary->notaryCommissionExpiration  }}" placeholder="" />
                                                        @error('notaryCommissionExpiration')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Are you bonded?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="areYouBonded1" name="areYouBonded" value="y" onclick="$('#isBondDiv').show()" {{ old('areYouBonded') =='y' ? 'checked' : ($notary->areYouBonded == 'y' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="areYouBonded1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="areYouBonded2" name="areYouBonded" value="n" onclick="$('#isBondDiv').hide()" {{ old('areYouBonded') =='n' ? 'checked' : ($notary->areYouBonded == 'n' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="areYouBonded2">NO</label>
                                                        </div>
                                                      </div>
                                                      @error('areYouBonded')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isBondDiv" style=" @if(old('areYouBonded') =='y') '' @elseIf($notary->areYouBonded == 'y') '' @else display: none @endif">

                                              <div class="col-12"  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Bond Certificate Number :</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" class="form-control @error('bondCertificateNumber') is-invalid @enderror" name="bondCertificateNumber" value="{{ old('bondCertificateNumber') ? old('bondCertificateNumber') : $notary->bondCertificateNumber  }}" placeholder="" />
                                                          @error('bondCertificateNumber')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12"  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Bond Certificate Amount :</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" class="form-control @error('bondCertificateAmount') is-invalid @enderror" name="bondCertificateAmount" value="{{ old('bondCertificateAmount') ? old('bondCertificateAmount') : $notary->bondCertificateAmount  }}" placeholder="" />
                                                          @error('bondCertificateAmount')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12"  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Bond Certificate Expiration :</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" class="form-control @error('bondCertificateExpiration') is-invalid @enderror" name="bondCertificateExpiration" value="{{ old('bondCertificateExpiration') ? old('bondCertificateExpiration') : $notary->bondCertificateExpiration  }}" placeholder="" />
                                                          @error('bondCertificateExpiration')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>

                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Do you have E&O Insurance?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="eo1" name="EO" value="y" onclick="$('#isEODiv').show()" {{ old('EO') =='y' ? 'checked' : ($notary->EO == 'y' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="eo1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" id="eo2" name="EO" value="n" onclick="$('#isEODiv').hide()" {{ old('EO') =='n' ? 'checked' : ($notary->EO == 'n' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="eo2">NO</label>
                                                        </div>
                                                      </div>
                                                      @error('EO')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isEODiv" style=" @if(old('EO') =='y') '' @elseIf($notary->EO == 'y') '' @else display: none @endif">

                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>E&O Insurance Number :</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" class="form-control @error('eoInsuranceNo') is-invalid @enderror" name="eoInsuranceNo" value="{{ old('eoInsuranceNo') ? old('eoInsuranceNo') : $notary->eoInsuranceNo  }}" placeholder="" />
                                                          @error('eoInsuranceNo')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12"  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>E&O Insurance Amount :</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" class="form-control @error('eoInsuranceAmount') is-invalid @enderror" name="eoInsuranceAmount" value="{{ old('eoInsuranceAmount') ? old('eoInsuranceAmount') : $notary->eoInsuranceAmount  }}" placeholder="" />
                                                          @error('eoInsuranceAmount')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12"  style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>E&O Insurance Expiration :</label>
                                                      </div>
                                                      <div class="col-sm-9">
                                                          <input type="text" class="form-control @error('eoInsuranceExpiration') is-invalid @enderror" name="eoInsuranceExpiration" value="{{ old('eoInsuranceExpiration') ? old('eoInsuranceExpiration') : $notary->eoInsuranceExpiration  }}" placeholder="" />
                                                          @error('eoInsuranceExpiration')
                                                              <div class="invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror
                                                      </div>
                                                  </div>
                                              </div>

                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Additional Information	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <textarea type="text" class="form-control @error('additionalInformation') is-invalid @enderror" name="additionalInformation" rows="2" placeholder="Write...">{{ old('additionalInformation') ? old('additionalInformation') : $notary->additionalInformation  }}</textarea>
                                                      @error('additionalInformation')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Administration Comments	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <textarea type="text" class="form-control @error('adminComments') is-invalid @enderror" name="adminComments" rows="2" placeholder="Write...">{{ old('adminComments') ? old('adminComments') : $notary->adminComments  }}</textarea>
                                                      @error('adminComments')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1">Update</button>
                                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->



            </div>
        </div>
        <!-- <span class="street-address">205 Glen Haven Ct</span>, <span class="locality">Swedesboro</span>, <span class="region">NJ</span> <span class="postal-code">08085-3051</span>, <span class="country-name">USA</span> -->
        <div id="mapString" class="d-none"></div>
        <div id="mapString2" class="d-none"></div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyB8zsk0CYFsdI8-4ItciBolf7-2vc1s6r4"></script>
    <script>

        var searchInput = 'address';

        $(document).ready(function () {
          var autocomplete;
          autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
              types: ['geocode'],
              componentRestrictions: {
                  country: "us"
              }
          });

          google.maps.event.addListener(autocomplete, 'place_changed', function () {
              var near_place = autocomplete.getPlace();
              console.log(near_place);
              // console.log(near_place.address_components[1].long_name);
              // save data with div id
              document.getElementById("mapString").innerHTML = near_place.adr_address;

              var city = document.getElementsByClassName("locality");
              document.getElementById('city').value = city[0].innerHTML;

              var zip = document.getElementsByClassName("postal-code");
              var data = zip[0].innerHTML;
              var res = data.split("-");
              document.getElementById('zipCode').value = res[0];


              document.getElementById('address').value = near_place.name;

              // var state = document.getElementsByClassName("region");
              // document.getElementById('state').value = state[0].innerHTML;




          });
        });
    </script>
    <script>

        var searchInput2 = 'overNightDelAddress';

        $(document).ready(function () {
          var autocomplete2;
          autocomplete2 = new google.maps.places.Autocomplete((document.getElementById(searchInput2)), {
              types: ['geocode'],
              componentRestrictions: {
                  country: "us"
              }
          });

          google.maps.event.addListener(autocomplete2, 'place_changed', function () {
              var near_place2 = autocomplete2.getPlace();
              console.log(near_place2);
              // console.log(near_place2.address_components[1].long_name);
              // save data with div id
              document.getElementById("mapString2").innerHTML = near_place2.adr_address;
              document.getElementById("mapString").innerHTML = '';

              var city = document.getElementsByClassName("locality");
              document.getElementById('overNightDelCity').value = city[0].innerHTML;

              var zip = document.getElementsByClassName("postal-code");
              var data = zip[0].innerHTML;
              var res = data.split("-");
              document.getElementById('overNightDelZip').value = res[0];


              document.getElementById('overNightDelAddress').value = near_place2.name;

              // var state = document.getElementsByClassName("region");
              // document.getElementById('state').value = state[0].innerHTML;

          });
        });
    </script>
@endsection
