<!--  -->

@extends('layouts.app')

@section('title', 'Notary (#1001) ')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/pages/app-invoice-list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/pages/app-user.css') }}">

<style media="screen">
.custom-control-input[disabled] ~ .custom-control-label, .custom-control-input:disabled ~ .custom-control-label {
  color: #3c3c3c;
}
</style>
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Notary Profile</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('admin/notary') }}">Notary</a>
                                    </li>
                                    <li class="breadcrumb-item active">#1001
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">

              <section class="app-user-view">
                    <!-- User Card & Plan Starts -->
                    <div class="row">
                        <!-- User Card starts-->
                        <div class="col-xl-9 col-lg-8 col-md-7">
                          <div class="row">
                            <div class="col-md-12">


                            <div class="card user-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                            <div class="user-avatar-section">
                                                <div class="d-flex justify-content-start">
                                                    <img class="img-fluid rounded" src="{{ asset('theme/app-assets/images/map.png') }}" height="104" width="104" alt="User avatar" />
                                                    <div class="d-flex flex-column ml-1">
                                                        <div class="user-info mb-1">
                                                            <h4 class="mb-0">

                                                            </h4>
                                                            <span class="card-text" style="font-weight: 900">
                                                              Liz Barahona1
                                                              4304 El Dorado Street
                                                              <br>
                                                              Oakley, California 94561
                                                              <br>
                                                              Contra Costa
                                                            </span>
                                                        </div>
                                                        <!-- <div class="d-flex flex-wrap">
                                                            <a href="./app-user-edit.html" class="btn btn-primary">Edit</a>
                                                            <button class="btn btn-outline-danger ml-1">Delete</button>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="d-flex align-items-center user-total-numbers">
                                                <div class="d-flex align-items-center mr-2">
                                                    <div class="color-box bg-light-primary">
                                                        <i data-feather="dollar-sign" class="text-primary"></i>
                                                    </div>
                                                    <div class="ml-1">
                                                        <h5 class="mb-0">23.3k</h5>
                                                        <small>Monthly Sales</small>
                                                    </div>
                                                </div>
                                                <div class="d-flex align-items-center">
                                                    <div class="color-box bg-light-success">
                                                        <i data-feather="trending-up" class="text-success"></i>
                                                    </div>
                                                    <div class="ml-1">
                                                        <h5 class="mb-0">$99.87K</h5>
                                                        <small>Annual Profit</small>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                        <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                            <div class="user-info-wrapper">
                                                <!-- <div class="d-flex flex-wrap">
                                                    <div class="user-info-title">
                                                        <i data-feather="user" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Username</span>
                                                    </div>
                                                    <p class="card-text mb-0">eleanor.aguilar</p>
                                                </div> -->
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="check" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Status</span>
                                                    </div>
                                                    <p class="card-text mb-0">
                                                      <div class="badge badge-light-primary mr-1">Active</div>
                                                    </p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="star" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">SSN</span>
                                                    </div>
                                                    <p class="card-text mb-0">559-82-3494</p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="flag" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">State</span>
                                                    </div>
                                                    <p class="card-text mb-0">California</p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i class="fas fa-at mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Email</span>
                                                    </div>
                                                    <p class="card-text mb-0">lizbara04@yahoo.com</p>
                                                </div>
                                                <div class="d-flex flex-wrap">
                                                    <div class="user-info-title">
                                                        <i data-feather="phone" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Business Phone</span>
                                                    </div>
                                                    <p class="card-text mb-0">(123) 456-7890</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                              <div class="card">
                                  <!-- <div class="card-header">
                                      <h4 class="card-title">
                                        Administration Comments
                                      </h4>
                                  </div> -->
                                  <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <!-- <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Administration Comments	:</label>
                                                    </div> -->
                                                    <div class="col-sm-12">
                                                      <div class="row">
                                                        <div class="col-10">
                                                          <textarea type="text" name="adminComments" class="form-control" rows="3" placeholder="Write administration comments..."></textarea>
                                                        </div>
                                                        <div class="col-2">
                                                          <button type="reset" class="btn btn-primary mr-1  text-center btn-block">Update</button>
                                                          <button type="reset" class="btn btn-outline-secondary  text-center btn-block">Save</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Update</button>
                                                <button type="reset" class="btn btn-outline-secondary">Save</button>
                                            </div> -->
                                        </div>
                                    </form>
                                  </div>
                              </div>
                            </div>


                            </div>

                        </div>
                        <!-- /User Card Ends-->

                        <!-- Plan Card starts-->
                        <div class="col-xl-3 col-lg-4 col-md-5">
                            <div class="card plan-card border-primary">
                                <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                                    <!-- <h5 class="mb-0">Current Plan</h5>
                                    <span class="badge badge-light-secondary" data-toggle="tooltip" data-placement="top" title="Expiry Date">July 22, <span class="nextYear"></span>
                                    </span> -->
                                </div>
                                <div class="card-body">
                                    <!-- <div class="badge badge-light-primary">Basic</div>
                                    <ul class="list-unstyled my-1">
                                        <li>
                                            <span class="align-middle">5 Users</span>
                                        </li>
                                        <li class="my-25">
                                            <span class="align-middle">10 GB storage</span>
                                        </li>
                                        <li>
                                            <span class="align-middle">Basic Support</span>
                                        </li>
                                    </ul> -->
                                    <button class="btn btn-outline-primary text-center btn-block">Notary Compilance</button>
                                    <button class="btn btn-outline-secondary text-center btn-block">Quickbook Add Green Light</button>
                                    <button class="btn btn-primary text-center btn-block">Send to QB</button>
                                    <button class="btn btn-outline-dark text-center btn-block"><i class="fas fa-tasks mr-25"></i> Call Logs</button>
                                    <button class="btn btn-outline-warning text-center btn-block"><i class="fas fa-bug mr-25"></i> Notary Error Log</button>
                                    <button class="btn btn-outline-primary text-center btn-block"><i class="fas fa-user-lock mr-25"></i> Login As This User</button>
                                    <button class="btn btn-outline-success text-center btn-block"><i class="far fa-edit mr-25"></i> Edit Profile</button>
                                </div>
                            </div>
                        </div>
                        <!-- /Plan CardEnds -->
                    </div>
                    <!-- User Card & Plan Ends -->

                    <!-- User Timeline & Permissions Starts -->
                    <div class="row">
                        <!-- Notary Profile starts -->
                        <div class="col-md-7">
                          <!-- Basic Horizontal form layout section start -->
                          <section id="basic-horizontal-layouts flatpickr">
                              <div class="row">
                                  <div class="col-md-12 col-12">
                                      <div class="card">
                                          <div class="card-header">
                                              <h4 class="card-title">
                                                <!-- Notary Profile Information -->
                                              </h4>
                                          </div>
                                          <div class="card-body pro">
                                              <form class="form form-horizontal">
                                                  <div class="row">
                                                      <div class="col-sm-9 offset-sm-3">
                                                          <div class="form-group">
                                                              <div class="custom-control custom-checkbox">
                                                                  <input type="checkbox" name="isScheduled" class="custom-control-input" id="avDate" id="isScheduled" onclick="$('#AvailableDateDiv').toggle()" />
                                                                  <label class="custom-control-label" for="avDate">
                                                                     Out of town
                                                                  </label>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="AvailableDateDiv" style="display: none">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="first-name">Next Available Date : </label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <!-- <input type="text" id="fp-default" name="RequestedAppointmentDate" value="" data-date-format="d-m-Y" class="form-control flatpickr-basic" placeholder="DD-MM-YYYY" /> -->

                                                                  <div class="input-group">
                                                                      <input type="text" id="fp-default" name="RequestedAppointmentDate" value="" data-date-format="d-m-Y" class="form-control flatpickr-basic" placeholder="DD-MM-YYYY" aria-describedby="button-addon2"/>
                                                                      <div class="input-group-append" id="button-addon2">
                                                                          <button class="btn btn-outline-primary" type="button"><i class="far fa-calendar-check mr-25"></i> Save Date</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Contact Name</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Adam Babish</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Address</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Liz Barahona1 4304 El Dorado Street</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-9 offset-sm-3">
                                                          <div class="form-group">
                                                              <div class="demo-inline-spacing">
                                                                <p>Oakley, California 94561</p>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>City</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Oakley</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>

                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>State:</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>California</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>ZIP</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>94561</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Country:</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>United State</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Home Phone</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-primary mr-1">
                                                                          <i class="fas fa-phone mr-25"></i>
                                                                          <span>925-625-5266</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-9 offset-sm-3">
                                                          <div class="form-group">
                                                              <div class="demo-inline-spacing">
                                                                  <div class="btn-group" role="group" aria-label="Basic example">
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Adam</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Allen</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Christina</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Keliani</button>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Business Phone</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-primary mr-1">
                                                                          <i class="fas fa-phone-alt mr-25"></i>
                                                                          <span>925-625-5266</span>
                                                                      </div>
                                                                      <span style="color: red; font-size: 85%; margin-top: .3rem;">
                                                                        (Preferred)
                                                                      </span>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-9 offset-sm-3">
                                                          <div class="form-group">
                                                              <div class="demo-inline-spacing">
                                                                  <div class="btn-group" role="group" aria-label="Basic example">
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Adam</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Allen</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Christina</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Keliani</button>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Cell Phone</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-primary mr-1">
                                                                          <i class="fas fa-phone-alt mr-25"></i>
                                                                          <span>925-625-5266</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-sm-9 offset-sm-3">
                                                          <div class="form-group">
                                                              <div class="demo-inline-spacing">
                                                                  <div class="btn-group" role="group" aria-label="Basic example">
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Adam</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Allen</button>
                                                                      <button type="button" class="btn btn-outline-primary"><i class="fas fa-headset mr-25"></i> Call Keliani</button>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <!-- <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="contact-info">Business Phone</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="number" id="contact-info" class="form-control" name="contact" placeholder="Business Phone" />
                                                              </div>
                                                          </div>
                                                      </div> -->
                                                      <!-- <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="contact-info">Fax</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <input type="number" id="" class="form-control" name="contact" placeholder="" />
                                                              </div>
                                                          </div>
                                                      </div> -->

                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Email</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-success mr-1">
                                                                          <i class="far fa-envelope mr-25"></i>
                                                                          <span> lizbara04@yahoo.com</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="email-id">Alternate Email (Optional)</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-light-info mr-1">
                                                                          <i class="far fa-envelope mr-25"></i>
                                                                          <span> lizbara04@yahoo.com</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="first-name">EIN</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 26</span>
                                                                      </div>
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 7654321</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="first-name">SSN</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 263</span>
                                                                      </div>
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 58</span>
                                                                      </div>
                                                                      <div class="badge badge-secondary mr-1">
                                                                          <span> 8697</span>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Is Overnight Delivery Address Different than Above?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <label class="form-check-label">NO</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Notary Fee :</label>
                                                              </div>
                                                              <div class="col-sm-9">

                                                                    <div class="input-group">
                                                                        <input type="number" class="form-control" placeholder="0.00" aria-describedby="button-addon2" />
                                                                        <div class="input-group-append" id="button-addon2">
                                                                            <button class="btn btn-outline-primary" type="button"><i class="far fa-edit mr-25"></i> Save</button>
                                                                            <button class="btn btn-outline-danger" type="button"><i class="far fa-trash-alt mr-25"></i> Clear</button>
                                                                        </div>
                                                                    </div>


                                                              </div>
                                                          </div>
                                                      </div>

                                                      <div class="col-12" id="isDeliveryDiv" style="display: none">

                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>Overnight Delivery Address for Documents</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <input type="text" id="" class="form-control" name="address" placeholder="Address" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-9 offset-sm-3">
                                                            <div class="form-group" style="margin-left: -1rem;margin-right: -1rem;">
                                                                <input type="text" id="" class="form-control" name="address2" placeholder="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>City</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <input type="text" id="" class="form-control" name="address" placeholder="CITY NAME" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>State:</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                  <select class="form-control" id="" name="state" onchange="">
                                                                      <option value="">Select State</option>
                                                                      <option value="1">CA</option>
                                                                      <option value="2">AZ</option>
                                                                      <option value="3">NY</option>
                                                                      <option value="4">Durai</option>
                                                                      <option value="5">Lucifer</option>
                                                                  </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>ZIP</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <input type="text" id="" class="form-control" name="address" placeholder="ZIP CODE" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                      </div>


                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Can you receive documents via email?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <label class="form-check-label">YES</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Do you have a laser printer?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <label class="form-check-label">YES</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Can you print legal size pages?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <label class="form-check-label">YES</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Availability</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>Weekend Only</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Do you speak any foreign languages?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                  <div class="form-check form-check-inline">
                                                                      <label class="form-check-label">YES</label>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="haveForeignDiv" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Languages :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">

                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang1" checked disabled/>
                                                                      <label class="custom-control-label" for="Lang1">Spanish</label>
                                                                  </div>
                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang2"  disabled />
                                                                      <label class="custom-control-label" for="Lang2">French</label>
                                                                  </div>
                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang3" checked disabled />
                                                                      <label class="custom-control-label" for="Lang3">German</label>
                                                                  </div>
                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang4"  disabled />
                                                                      <label class="custom-control-label" for="Lang4">Chinese</label>
                                                                  </div>

                                                                </div>
                                                                <div class="demo-inline-spacing">

                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang5"  disabled />
                                                                      <label class="custom-control-label" for="Lang5">Japanese</label>
                                                                  </div>
                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang6"  checked disabled />
                                                                      <label class="custom-control-label" for="Lang6">Italian</label>
                                                                  </div>
                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang7"  disabled />
                                                                      <label class="custom-control-label" for="Lang7">Russian</label>
                                                                  </div>
                                                                  <div class="custom-control custom-checkbox">
                                                                      <input type="checkbox" name="checkLanguage[]" class="custom-control-input" id="Lang8"  disabled />
                                                                      <label class="custom-control-label" for="Lang8">Hindi</label>
                                                                  </div>

                                                                </div>
                                                              </div>
                                                          </div>

                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="contact-info">How long have you been a notary?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>2005</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Do you have experience in signing loan documents?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                      <label class="form-check-label">YES</label>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>May we contact your references?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                      <label class="form-check-label">YES</label>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="" style="">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Approximately how many loan documents have you signed?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                      <label class="form-check-label">51-100</label>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Notary Commission :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p>1754182</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Notary Commission Expiration :</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                  <div class="demo-inline-spacing">
                                                                    <p style="color: #6610F2"><i class="far fa-calendar-alt mr-25"></i> 15-05-2021</p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Are you bonded?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                      <label class="form-check-label">YES</label>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="isBondDiv" style="">

                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>Bond Certificate Number :</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                      <p>25255252</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>Bond Certificate Amount :</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                      <div class="badge badge-primary mr-1">
                                                                          <i class="fas fa-comment-dollar mr-25"></i>
                                                                          <span> 200 USD</span>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>Bond Certificate Expiration :</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                      <p style="color: #6610F2"><i class="far fa-calendar-alt mr-25"></i> 15-05-2021</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                      </div>
                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label>Do you have E&O Insurance?</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <div class="demo-inline-spacing">
                                                                      <label class="form-check-label">YES</label>
                                                                </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-12" id="isEODiv" style="">

                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>E&O Insurance Number :</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                      <p>2525554</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>E&O Insurance Amount :</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                      <div class="badge badge-primary mr-1">
                                                                          <i class="fas fa-comment-dollar mr-25"></i>
                                                                          <span> 200555 USD</span>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12" id=""  style="margin-left: -1rem;margin-right: -1rem;">
                                                            <div class="form-group row">
                                                                <div class="col-sm-3 col-form-label">
                                                                    <label>E&O Insurance Expiration :</label>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <div class="demo-inline-spacing">
                                                                      <p style="color: #6610F2"><i class="far fa-calendar-alt mr-25"></i> 15-05-2021</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                      </div>

                                                      <div class="col-12">
                                                          <div class="form-group row">
                                                              <div class="col-sm-3 col-form-label">
                                                                  <label for="first-name">Additional Information	</label>
                                                              </div>
                                                              <div class="col-sm-9">
                                                                <textarea type="text" name="Additional" class="form-control" rows="2" placeholder="Write..." ></textarea>

                                                              </div>
                                                          </div>
                                                      </div>


                                                      <!-- <div class="col-sm-9 offset-sm-3">
                                                          <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                                          <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                                      </div> -->
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </section>
                          <!-- Basic Horizontal form layout section end -->
                        </div>
                        <!-- Notary profile Ends -->
                        <!-- information starts -->
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title mb-2">Call Logs</h4>
                                </div>
                                <div class="card-body">
                                    <ul class="timeline">
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h6>Left Message</h6>
                                                    <span class="timeline-event-time" title="19-11-2020">19 Nov, 2020</span>
                                                </div>
                                                <p>This is a test sample.</p>
                                                <div class="media align-items-center">
                                                    <div class="media-body" style="font-size: 0.9rem; color: #ea54d1;">Order ID# 62092</div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h6>SMS- Not Responded</h6>
                                                    <span class="timeline-event-time">2 Nov, 2020</span>
                                                </div>
                                                <p>This is a test sample...</p>
                                                <div class="media align-items-center">

                                                        <div class="media-body" style="font-size: 0.9rem; color: #ea54d1;">Order ID# 62092</div>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- information Ends -->


                    </div>
                    <!-- User Timeline & Permissions Ends -->


                </section>



            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });
    </script>
@endsection
