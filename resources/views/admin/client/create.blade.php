<!--  -->

@extends('layouts.app')

@section('title', 'Add New Client')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Add Client</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('admin/client') }}">Client</a>
                                    </li>
                                    <li class="breadcrumb-item active">New
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts flatpickr">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Place Client Information</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Title Company</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="cName" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>First Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="first-name" class="form-control" name="FirstName" placeholder="First Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Last Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="last-name" class="form-control" name="LastName" placeholder="Last Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Role :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="" name="state" onchange="">
                                                          <option value="">Select Role</option>
                                                          <option value="1">Escrow Officer</option>
                                                          <option value="2">Escrow Assistant</option>
                                                          <option value="3">Assistant</option>
                                                          <option value="4">Loan Officer</option>
                                                          <option value="5">Other</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Address</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="address" placeholder="Address" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <input type="text" id="" class="form-control" name="address2" placeholder="" />
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>City</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="address" placeholder="CITY NAME" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>State:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="" name="state" onchange="">
                                                          <option value="">Select State</option>
                                                          <option value="1">CA</option>
                                                          <option value="2">AZ</option>
                                                          <option value="3">NY</option>
                                                          <option value="4">Durai</option>
                                                          <option value="5">Lucifer</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>ZIP</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="address" placeholder="ZIP CODE" />
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Phone</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                      <div class="input-group">
                                                          <div class="input-group-prepend" style="">
                                                              <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-alt"></i></span>
                                                          </div>
                                                          <input type="number" id="contact-info-icon" class="form-control" name="contact-icon" placeholder="Phone" />
                                                      </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="contact-info">Business Phone</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="number" id="contact-info" class="form-control" name="contact" placeholder="Business Phone" />
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="contact-info">Fax</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="number" id="" class="form-control" name="contact" placeholder="" />
                                                    </div>
                                                </div>
                                            </div> -->

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Email</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="email" id="email-id" class="form-control" name="email" placeholder="Email" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Would you like all invoices also copied to another person?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_invoice_email_cc" id="" value="Yes" onclick="$('#isInvoiceEmailcc').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_invoice_email_cc" id="" value="No" onclick="$('#isInvoiceEmailcc').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isInvoiceEmailcc" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Enter Email Address</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="email2" placeholder="CC Person Email " />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Would you like all email contact from ProLink also copied to another person?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_email_contact_cc" id="" value="Yes" onclick="$('#isContactEmailcc').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_email_contact_cc" id="" value="No" onclick="$('#isContactEmailcc').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isContactEmailcc" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Enter Email Address</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="email2" placeholder="CC Email " />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Preferred Carrier :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="" name="carrier" onchange="changeCourier(value)">
                                                          <option value=""> - Select Carrier -</option>
                                                          <option value="1">UPS</option>
                                                          <option value="2">FedEx</option>
                                                          <option value="3">DHL / Airborne</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="shippingRoot" style="display:none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Shipping Method (priority, standard, 2 day)</label>
                                                    </div>
                                                    <div class="col-sm-9" id="shippingNull" style="">
                                                      <select class="form-control" id="" name="shippingNull" onchange="">

                                                      </select>
                                                    </div>
                                                    <div class="col-sm-9" id="shippingUPS" style="display: none">
                                                      <select class="form-control" id="" name="shippingUPS" onchange="">

                                                          <option value="1">Next Day Air (10:30 A.M.)</option>
                                                          <option value="2">Next Day Air Saver (3:00 P.M.)</option>
                                                          <option value="3">2nd Day Air A.M. (12:00 Noon)</option>
                                                      </select>
                                                    </div>
                                                    <div class="col-sm-9" id="shippingFedex" style="display: none">
                                                      <select class="form-control" id="" name="shippingFedex" onchange="">

                                                          <option value="1">Priority Overnight (10:30 A.M.)</option>
                                                          <option value="2">Standard Overnight (3:00 P.M.)</option>
                                                          <option value="3">2-Day (4:30 P.M.)</option>
                                                      </select>
                                                    </div>
                                                    <div class="col-sm-9" id="shippingDHL" style="display: none">
                                                      <select class="form-control" id="" name="shippingDHL" onchange="">

                                                          <option value="1">DHL Overnight (12 noon)</option>
                                                          <option value="2">DHL 2nd Day (5:00 P.M.)</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Shipping Account#</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control" name="ac" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Administration Comments	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <textarea type="text" name="adminComments" class="form-control" rows="2" placeholder="Write..."></textarea>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Upload PDF File :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="address" placeholder="" />
                                                      <!-- <textarea type="text" name="Additional" class="form-control" rows="2" placeholder="Write..."></textarea> -->

                                                    </div>
                                                </div>
                                            </div>


                                            <!-- <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">EIN</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="row">
                                                        <div class="col-3">
                                                          <input type="number" id="" class="form-control" name="" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" />
                                                        </div>
                                                        <div class="col-9">
                                                          <input type="number" id="" class="form-control" name="" placeholder="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==7) return false;" />
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">SSN</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="row">
                                                        <div class="col-3">
                                                          <input type="number" id="" class="form-control" name="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" placeholder="" />
                                                        </div>
                                                        <div class="col-2">
                                                          <input type="number" id="" class="form-control" name="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" placeholder="" />
                                                        </div>
                                                        <div class="col-7">
                                                          <input type="number" id="" class="form-control" name="" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" placeholder="" />
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div> -->











                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->



            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });

      function changeCourier($i) {
        // alert($i);
        $('#shippingRoot').show();

        if ($i == 1) {
          $('#shippingUPS').show();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 2) {
          $('#shippingUPS').hide();
          $('#shippingFedex').show();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 3) {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').show();
          $('#shippingNull').hide();
        } else {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').show();

          $('#shippingRoot').hide();
        }
       }
    </script>
@endsection
