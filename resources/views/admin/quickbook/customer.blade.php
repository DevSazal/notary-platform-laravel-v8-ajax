<!--  -->

@extends('layouts.app')

@section('title', 'QuickBooks Customer')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">QB Customer</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="{{ url('admin/order') }}">Order</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">Customer
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">



                <!-- Row grouping -->
                <section id="row-grouping-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-bottom">

                                  <div class="head-label">
                                    <h4 class="card-title">QB Customer List</h4>
                                  </div>

                                  <!-- <div class="dt-action-buttons text-right">
                                    <div class="dt-buttons d-inline-flex">
                                      <a href="{{ route('admin.admin.create') }}" class="dt-button buttons-collection btn btn-outline-primary mr-2 waves-effect waves-float waves-light">
                                        Create New Admin
                                      </a>
                                    </div>
                                  </div> -->

                                </div>
                                <div class="card-datatable" style="padding: 0rem 1rem 1rem">
                                    <table id="dataTable_orders" class="table">
                                        <thead>
                                            <tr>
                                                <th>ID#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>1</td>
                                            <td>Adam</td>
                                            <td>adam@theprolink.com</td>
                                            <td>
                                              <a href="#" class="btn btn-sm btn-primary waves-effect waves-float waves-light">Send to QB</a>
                                              <a href="#" class="btn btn-sm btn-warning waves-effect waves-float waves-light">Delete</a>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>2</td>
                                            <td>John</td>
                                            <td>john@gmail.com</td>
                                            <td>
                                              <a href="#" class="btn btn-sm btn-primary waves-effect waves-float waves-light">Send to QB</a>
                                              <a href="#" class="btn btn-sm btn-warning waves-effect waves-float waves-light">Delete</a>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>3</td>
                                            <td>Sazal Ahamed</td>
                                            <td>test@mail.com</td>
                                            <td>
                                              <a href="#" class="btn btn-sm btn-primary waves-effect waves-float waves-light">Send to QB</a>
                                              <a href="#" class="btn btn-sm btn-warning waves-effect waves-float waves-light">Delete</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Row grouping -->



            </div>
        </div>
@endsection

@section('js')
<!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ asset('theme/app-assets/js/scripts/tables/table-datatables-basic.js') }}"></script>-->
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        $('#dataTable_orders').DataTable();
      /*  var table = $('#dataTable_orders').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.orders.index') }}",
            // columns: [
            //     {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            //     {data: 'name', name: 'name'},
            //     {data: 'email', name: 'email'},
            //     {data: 'action', name: 'action', orderable: false, searchable: false},
            // ]
        });*/
      });
    </script>
@endsection
