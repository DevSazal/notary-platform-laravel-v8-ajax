<!--  -->

@extends('layouts.app')

@section('title', 'QuickBooks Invoice')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<style media="screen">
  .btn-sm{
    margin-bottom: 0.3rem;
  }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">QB Invoice</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="{{ url('admin/order') }}">Order</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">Invoice
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">



                <!-- Row grouping -->
                <section id="row-grouping-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-bottom">

                                  <div class="head-label">
                                    <h4 class="card-title">QB Invoice List</h4>
                                  </div>

                                  <!-- <div class="dt-action-buttons text-right">
                                    <div class="dt-buttons d-inline-flex">
                                      <a href="{{ route('admin.admin.create') }}" class="dt-button buttons-collection btn btn-outline-primary mr-2 waves-effect waves-float waves-light">
                                        Create New Admin
                                      </a>
                                    </div>
                                  </div> -->

                                </div>
                                <div class="card-datatable" style="padding: 0rem 1rem 1rem">
                                    <table id="dataTable_orders" class="table">
                                        <thead>
                                            <tr>
                                                <th>Order #</th>
                                                <th>Escrow #</th>
                                                <th>Borrower Name</th>
                                                <th>Company Name</th>
                                                <th>Client Name</th>
                                                <th>Signing Date</th>
                                                <th>Cost</th>
                                                <th>Status</th>
                                                <th>Note</th>
                                                <th>Duplicate</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td><a href="#">102</a></td>
                                            <td><a href="#">25362</a></td>
                                            <td>Adam</td>
                                            <td>Chartwell Escrow</td>
                                            <td>GABRIELA DIAZ</td>
                                            <td><span class="text-success mr-25">January 9, 2021</span></td>
                                            <td><span class="text-primary mr-25">200</span></td>
                                            <td><div class="badge badge-light-success mr-1">Closed</div></td>
                                            <td>
                                              <a href="#" target="popup" onclick="window.open('','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
                                                <i class="far fa-comment-alt"></i>
                                              </a>
                                            </td>
                                            <td><div class="badge badge-pill badge-light-primary mr-1">Yes</div></td>
                                            <td>
                                              <div class="btn-group">
                                                  <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      Action
                                                  </button>
                                                  <div class="dropdown-menu">
                                                      <a class="dropdown-item" href="javascript:void(0);">Send to QB</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">Amend Invoice</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">Edit Order</a>
                                                      <a class="dropdown-item" href="javascript:void(0);" onClick='deleteMsg()'>Delete</a>

                                                      <div class="dropdown-divider"></div>
                                                      <a class="dropdown-item" href="javascript:void(0);">Mark As Sent to QB</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">View</a>
                                                  </div>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td><a href="#">102</a></td>
                                            <td><a href="#">25362</a></td>
                                            <td>Adam</td>
                                            <td>Adam</td>
                                            <td>Adam</td>
                                            <td><span class="text-success mr-25">January 5, 2021</span></td>
                                            <td><span class="text-primary mr-25">200</span></td>
                                            <td><div class="badge badge-light-danger mr-1">Cancelled</div></td>
                                            <td>
                                              <a href="#" target="popup" onclick="window.open('','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
                                                <i class="far fa-comment-alt"></i>
                                              </a>
                                            </td>
                                            <td><div class="badge badge-pill badge-light-primary mr-1">Yes</div></td>
                                            <td>
                                              <div class="btn-group">
                                                  <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      Action
                                                  </button>
                                                  <div class="dropdown-menu">
                                                      <a class="dropdown-item" href="javascript:void(0);">Send to QB</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">Amend Invoice</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">Edit Order</a>
                                                      <a class="dropdown-item" href="javascript:void(0);" onClick='deleteMsg()'>Delete</a>

                                                      <div class="dropdown-divider"></div>
                                                      <a class="dropdown-item" href="javascript:void(0);">Mark As Sent to QB</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">View</a>
                                                  </div>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td><a href="#">102</a></td>
                                            <td><a href="#">25362</a></td>
                                            <td>Sazal Ahamed</td>
                                            <td>Morris Manning Martin</td>
                                            <td>Jessica Adkins</td>
                                            <td><span class="text-success mr-25">January 11, 2021</span></td>
                                            <td><span class="text-primary mr-25">200</span></td>
                                            <td><div class="badge badge-light-success mr-1">Closed</div></td>
                                            <td>
                                              <a href="#" target="popup" onclick="window.open('','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
                                                <i class="far fa-comment-alt"></i>
                                              </a>
                                            </td>
                                            <td><div class="badge badge-pill badge-light-primary mr-1">Yes</div></td>
                                            <td>
                                              <div class="btn-group">
                                                  <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      Action
                                                  </button>
                                                  <div class="dropdown-menu">
                                                      <a class="dropdown-item" href="javascript:void(0);">Send to QB</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">Amend Invoice</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">Edit Order</a>
                                                      <a class="dropdown-item" href="javascript:void(0);" onClick='deleteMsg()'>Delete</a>

                                                      <div class="dropdown-divider"></div>
                                                      <a class="dropdown-item" href="javascript:void(0);">Mark As Sent to QB</a>
                                                      <a class="dropdown-item" href="javascript:void(0);">View</a>
                                                  </div>
                                              </div>
                                            </td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Row grouping -->



            </div>
        </div>
@endsection

@section('js')
<!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ asset('theme/app-assets/js/scripts/tables/table-datatables-basic.js') }}"></script>-->
    <script src="{{ asset('theme/app-assets/js/scripts/components/components-dropdowns.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        $('#dataTable_orders').DataTable();
      /*  var table = $('#dataTable_orders').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.orders.index') }}",
            // columns: [
            //     {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            //     {data: 'name', name: 'name'},
            //     {data: 'email', name: 'email'},
            //     {data: 'action', name: 'action', orderable: false, searchable: false},
            // ]
        });*/
      });

      // function deleteMsg(){
      //   swal("Are you sure?", {
      //     dangerMode: true,
      //     buttons: true,
      //     });
      // }
      function deleteMsg(){
          swal({
            title: "Are you sure?",
            text: "You want to delete!",
            icon: "warning",
            buttons: [true, "Confirm"],
            dangerMode: true,
            // timer: 9000,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("The QB invoice has been deleted!", {
                icon: "success",
              });
            }
          });
      }
    </script>
@endsection
