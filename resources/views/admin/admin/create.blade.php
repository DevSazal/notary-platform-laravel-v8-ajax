<!--  -->

@extends('layouts.app')

@section('title', 'Add New Admin')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Create New Admin</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ route('admin.admin.index') }}">Admin</a>
                                    </li>
                                    <li class="breadcrumb-item active">New
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts flatpickr">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">PROVIDE AN ADMIN INFORMATION</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <!-- <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="isScheduled" class="custom-control-input" id="customCheck1" id="isScheduled" onclick="$('#RequestedAppointmentDateDiv').toggle(); $('#isScheduledTDiv').toggle(); $('#isScheduledDiv').toggle();" />
                                                        <label class="custom-control-label" for="customCheck1">
                                                           If you would like us to schedule the appointment time with Customer, please click here
                                                        </label>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Admin Level?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="isMeet" value="Yes" onclick="$('#tripDiv').hide()">
                                                            <label class="form-check-label">Root</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="isMeet" value="No" onclick="$('#tripDiv').toggle()">
                                                            <label class="form-check-label">Limited</label>
                                                        </div>
                                                      </div>



                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="a">Username :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="a" class="form-control" name="a_username" placeholder="Username" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">First Name :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="first-name" class="form-control" name="FirstName" placeholder="First Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Last Name :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="last-name" class="form-control" name="LastName" placeholder="Last Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Password :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="password" id="" class="form-control" name="password" placeholder="Password" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Confirm Password :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="password" id="" class="form-control" name="confirm-password" placeholder="Password Again" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Email :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="email" id="" class="form-control" name="email" placeholder="Email" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="contact-info">Phone (Optional)</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="number" id="contact-info" class="form-control" name="contact" placeholder="Phone" />
                                                    </div>
                                                </div>
                                            </div>





                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->



            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });
    </script>
@endsection
