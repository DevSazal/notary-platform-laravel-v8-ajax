<!--  -->

@extends('layouts.app')

@section('title', 'Shipments')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<style media="screen">
  .btn-sm{
    margin-bottom: 0.3rem;
  }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Shipments Log</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item"><a href="{{ url('admin/order') }}">Order</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">Shipments
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">



                <!-- Row grouping -->
                <section id="row-grouping-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header border-bottom">

                                  <div class="head-label">
                                    <h4 class="card-title">Shipments List</h4>
                                  </div>

                                  <!-- <div class="dt-action-buttons text-right">
                                    <div class="dt-buttons d-inline-flex">
                                      <a href="{{ route('admin.admin.create') }}" class="dt-button buttons-collection btn btn-outline-primary mr-2 waves-effect waves-float waves-light">
                                        Create New Admin
                                      </a>
                                    </div>
                                  </div> -->

                                </div>
                                <div class="card-datatable" style="padding: 0rem 1rem 1rem">
                                    <table id="dataTable_orders" class="table">
                                        <thead>
                                            <tr>
                                                <th>Order#</th>
                                                <th>Escrow#</th>

                                                <th>Company Name</th>
                                                <th>Borrower Name</th>
                                                <th>Closer Name</th>
                                                <th>Appt Date & Time</th>

                                                <th>Status</th>
                                                <th>Tracking#</th>
                                                <th>Delivery Status</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td><a href="#">102</a></td>
                                            <td><a href="#">25362</a></td>

                                            <td>Chartwell Escrow</td>
                                            <td>Adam</td>
                                            <td>GABRIELA DIAZ</td>
                                            <td><span class="text-success mr-25">January 9, 2021</span></td>

                                            <td><div class="badge badge-light-primary mr-1">Closed</div></td>
                                            <td><i class="fas fa-shipping-fast"></i> 796041717700</td>
                                            <td><div class="badge badge-pill badge-light-primary mr-1">Delivered</div></td>

                                          </tr>
                                          <tr>
                                            <td><a href="#">102</a></td>
                                            <td><a href="#">25362</a></td>
                                            <td>Adam</td>
                                            <td>Adam</td>
                                            <td>Adam</td>
                                            <td><span class="text-success mr-25">January 5, 2021</span></td>

                                            <td><div class="badge badge-light-warning mr-1">Pending</div></td>
                                            <td><i class="fas fa-shipping-fast"></i> 782672678432</td>
                                            <td><div class="badge badge-pill badge-light-dark mr-1">N/A</div></td>

                                          </tr>
                                          <tr>
                                            <td><a href="#">102</a></td>
                                            <td><a href="#">25362</a></td>

                                            <td>Morris Manning Martin</td>
                                            <td>Sazal Ahamed</td>
                                            <td>Jessica Adkins</td>
                                            <td><span class="text-success mr-25">January 11, 2021</span></td>

                                            <td><div class="badge badge-light-primary mr-1">Closed</div></td>
                                            <td><i class="fas fa-shipping-fast"></i> 78260000000</td>
                                            <td><div class="badge badge-pill badge-light-primary mr-1">Delivered</div></td>

                                          </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Row grouping -->



            </div>
        </div>
@endsection

@section('js')
<!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ asset('theme/app-assets/js/scripts/tables/table-datatables-basic.js') }}"></script>-->
    <script src="{{ asset('theme/app-assets/js/scripts/components/components-dropdowns.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        $('#dataTable_orders').DataTable();
      /*  var table = $('#dataTable_orders').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.orders.index') }}",
            // columns: [
            //     {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            //     {data: 'name', name: 'name'},
            //     {data: 'email', name: 'email'},
            //     {data: 'action', name: 'action', orderable: false, searchable: false},
            // ]
        });*/
      });

      // function deleteMsg(){
      //   swal("Are you sure?", {
      //     dangerMode: true,
      //     buttons: true,
      //     });
      // }
      function deleteMsg(){
          swal({
            title: "Are you sure?",
            text: "You want to delete!",
            icon: "warning",
            buttons: [true, "Confirm"],
            dangerMode: true,
            // timer: 9000,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("The QB invoice has been deleted!", {
                icon: "success",
              });
            }
          });
      }
    </script>
@endsection
