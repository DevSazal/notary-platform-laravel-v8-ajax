<!--  -->

@extends('layouts.app')

@section('title', 'News')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">


    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/editors/quill/katex.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/editors/quill/monokai-sublime.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/editors/quill/quill.snow.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/extensions/dragula.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-quill-editor.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/extensions/ext-component-toastr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/pages/app-todo.css') }}">

@endsection

@section('app-class', 'todo-application')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-10 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">News</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                            </li>
                            <!-- <li class="breadcrumb-item active">
                              <a href="{{ url('admin/client') }}">Client</a>
                            </li> -->
                            <li class="breadcrumb-item active">Manage
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-right col-md-2 col-12 d-md-block ">
            <div class="form-group breadcrumb-right">
              <div class="add-task">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#new-task-modal">
                      <i class="fas fa-bullhorn mr-25"></i> Add News
                  </button>
              </div>

            </div>
        </div>
    </div>

    <div class="content-body">
    </div>
</div>



<div class="content-area-wrapper">

    <div class="content-right" style="width: calc(100% - 0px)">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="body-content-overlay"></div>
                <div class="todo-app-list">
                    <!-- Todo search starts -->
                    <div class="app-fixed-search d-flex align-items-center">
                        <div class="sidebar-toggle d-block d-lg-none ml-1">
                            <i data-feather="menu" class="font-medium-5"></i>
                        </div>
                        <div class="d-flex align-content-center justify-content-between w-100">
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span>
                                </div>
                                <input type="text" class="form-control" id="todo-search" placeholder="Search News" aria-label="Search..." aria-describedby="todo-search" />
                            </div>
                        </div>
                        <div class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle hide-arrow mr-1" id="todoActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i data-feather="more-vertical" class="font-medium-2 text-body"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="todoActions">
                                <a class="dropdown-item sort-asc" href="javascript:void(0)">Sort A - Z</a>
                                <a class="dropdown-item sort-desc" href="javascript:void(0)">Sort Z - A</a>
                                <a class="dropdown-item" href="javascript:void(0)">Sort Date</a>
                                <a class="dropdown-item" href="javascript:void(0)">Sort Today</a>
                                <a class="dropdown-item" href="javascript:void(0)">Sort 1 Week</a>
                                <a class="dropdown-item" href="javascript:void(0)">Sort 1 Month</a>
                            </div>
                        </div>
                    </div>
                    <!-- Todo search ends -->

                    <!-- Todo List starts -->
                    <div class="todo-task-list-wrapper list-group">
                        <ul class="todo-task-list media-list" id="todo-task-list">
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Fix Responsiveness for new structure</span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 08</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Plan a party for development team </span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 30</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Hire 5 new Fresher or Experienced, frontend and backend developers </span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 28</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Skype Tommy for project status & report</span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 18</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Send PPT with real-time reports</span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 22</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Submit quotation for Abid's ecommerce website and admin project </span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 24</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Reminder to mail clients for holidays</span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 27</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Refactor Code and fix the bugs and test it on server </span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 27</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">List out all the SEO resources and send it to new SEO team. </span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">
                                        <small class="text-nowrap text-muted mr-1">Sept 15</small>

                                    </div>
                                </div>
                            </li>
                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Finish documentation and make it live</span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 28</small>

                                    </div>
                                </div>
                            </li>

                            <li class="todo-item">
                                <div class="todo-title-wrapper">
                                    <div class="todo-title-area">
                                        <i data-feather="more-vertical" class="drag-icon"></i>
                                        <div class="title-wrapper">

                                            <span class="todo-title">Meet Jane and ask for coffee </span>
                                        </div>
                                    </div>
                                    <div class="todo-item-action">

                                        <small class="text-nowrap text-muted mr-1">Aug 10</small>

                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="no-results">
                            <h5>No Items Found</h5>
                        </div>
                    </div>
                    <!-- Todo List ends -->
                </div>

                <!-- Right Sidebar starts -->
                <div class="modal modal-slide-in sidebar-todo-modal fade" id="new-task-modal">
                    <div class="modal-dialog sidebar-lg">
                        <div class="modal-content p-0">
                            <form id="form-modal-todo" class="todo-modal needs-validation" novalidate onsubmit="return false">
                                <div class="modal-header align-items-center mb-1">
                                    <h5 class="modal-title">Add News</h5>
                                    <div class="todo-item-action d-flex align-items-center justify-content-between ml-auto">
                                        <span class="todo-item-favorite cursor-pointer mr-75"><i data-feather="star" class="font-medium-2"></i></span>
                                        <button type="button" class="close font-large-1 font-weight-normal py-0" data-dismiss="modal" aria-label="Close">
                                            ×
                                        </button>
                                    </div>
                                </div>
                                <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                                    <div class="action-tags">
                                        <div class="form-group">
                                            <label for="todoTitleAdd" class="form-label">Title</label>
                                            <input type="text" id="todoTitleAdd" name="todoTitleAdd" class="new-todo-item-title form-control" placeholder="Title" />
                                        </div>
                                        <!-- <div class="form-group position-relative">
                                            <label for="task-assigned" class="form-label d-block">Assignee</label>
                                            <select class="select2 form-control" id="task-assigned" name="task-assigned">
                                                <option data-img="../../../app-assets/images/portrait/small/avatar-s-3.jpg" value="Phill Buffer" selected>
                                                    Phill Buffer
                                                </option>
                                                <option data-img="../../../app-assets/images/portrait/small/avatar-s-1.jpg" value="Chandler Bing">
                                                    Chandler Bing
                                                </option>
                                                <option data-img="../../../app-assets/images/portrait/small/avatar-s-4.jpg" value="Ross Geller">
                                                    Ross Geller
                                                </option>
                                                <option data-img="../../../app-assets/images/portrait/small/avatar-s-6.jpg" value="Monica Geller">
                                                    Monica Geller
                                                </option>
                                                <option data-img="../../../app-assets/images/portrait/small/avatar-s-2.jpg" value="Joey Tribbiani">
                                                    Joey Tribbiani
                                                </option>
                                                <option data-img="../../../app-assets/images/portrait/small/avatar-s-11.jpg" value="Rachel Green">
                                                    Rachel Green
                                                </option>
                                            </select>
                                        </div> -->
                                        <div class="form-group">
                                            <label for="task-due-date" class="form-label">Date</label>
                                            <input type="text" class="form-control task-due-date" id="task-due-date" name="task-due-date" data-date-format="d M, Y" />
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="task-tag" class="form-label d-block">Tag</label>
                                            <select class="form-control task-tag" id="task-tag" name="task-tag" multiple="multiple">
                                                <option value="Team">Team</option>
                                                <option value="Low">Low</option>
                                                <option value="Medium">Medium</option>
                                                <option value="High">High</option>
                                                <option value="Update">Update</option>
                                            </select>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="form-label">Description</label>
                                            <div id="task-desc" class="border-bottom-0" data-placeholder="Write Your Description"></div>
                                            <div class="d-flex justify-content-end desc-toolbar border-top-0">
                                                <span class="ql-formats mr-0">
                                                    <button class="ql-bold"></button>
                                                    <button class="ql-italic"></button>
                                                    <button class="ql-underline"></button>
                                                    <button class="ql-align"></button>
                                                    <button class="ql-link"></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group my-1">
                                        <button type="submit" class="btn btn-primary add-todo-item mr-1">Add</button>
                                        <button type="button" class="btn btn-outline-secondary add-todo-item " data-dismiss="modal">
                                            Cancel
                                        </button>
                                        <button type="button" class="btn btn-primary d-none update-btn update-todo-item mr-1">Update</button>
                                        <button type="button" class="btn btn-outline-danger update-btn d-none" data-dismiss="modal">Delete</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Right Sidebar ends -->

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/katex.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/highlight.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dragula.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>

    <!-- <script src="{{ asset('theme/app-assets/js/scripts/pages/app-todo.js') }}"></script> -->
    <script src="{{ asset('theme/src/js/scripts/pages/app-todo.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });

      function changeCourier($i) {
        // alert($i);
        $('#shippingRoot').show();

        if ($i == 1) {
          $('#shippingUPS').show();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 2) {
          $('#shippingUPS').hide();
          $('#shippingFedex').show();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 3) {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').show();
          $('#shippingNull').hide();
        } else {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').show();

          $('#shippingRoot').hide();
        }
       }
      function eventSetLender($i) {
        // alert($i);
        document.getElementById('lenderT').value = $i;
      }
      function eventSetCannedRes($i) {
        document.getElementById('cannedResT').value = $i;
      }
    </script>
@endsection
