<!--  -->

@extends('layouts.app')

@section('title', 'Add New Order')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Add Order</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                      <a href="{{ url('admin/order') }}">Order</a>
                                    </li>
                                    <li class="breadcrumb-item active">new
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts flatpickr">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">PLACE AN ORDER</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('admin.order.register') }}" method="POST" class="form form-horizontal">
                                        @csrf

                                        <div class="row">
                                            <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="isScheduled" class="custom-control-input" id="customCheck1" id="isScheduled" onclick="$('#RequestedAppointmentDateDiv').toggle(); $('#isScheduledTDiv').toggle(); $('#isScheduledDiv').toggle();" />
                                                        <label class="custom-control-label" for="customCheck1">
                                                           If you would like us to schedule the appointment time with Customer, please click here
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="RequestedAppointmentDateDiv">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Requested Appointment Date</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="fp-default" name="RequestedAppointmentDate" value="2015-12-12" class="form-control flatpickr-basic" placeholder="YYYY-MM-DD" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isScheduledDiv" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Choose Scheduling Option</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                          <option value="">Select Option</option>
                                                          <option value="Today">Today</option>
                                                          <option value="Today Or Tomorrow">Today Or Tomorrow</option>
                                                          <option value="Tomorrow">Tomorrow</option>
                                                          <option value="Open">Open</option>
                                                          <option value="Other">Other</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isScheduledTDiv">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Requested Appointment Time</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="row">
                                                        <div class="col-4">
                                                          <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                              <option value="">Select Hour</option>
                                                              <option value="01">01</option>
                                                              <option value="01">02</option>
                                                              <option value="01">03</option>
                                                              <option value="01">01</option>
                                                              <option value="01">01</option>
                                                          </select>
                                                        </div>
                                                        <div class="col-4">
                                                          <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                              <option value="">Select Minute</option>
                                                              <option value="1">1</option>
                                                              <option value="1">2</option>
                                                              <option value="1">3</option>
                                                              <option value="1">4</option>
                                                          </select>
                                                        </div>
                                                        <div class="col-4">
                                                          <select class="form-control" id="scheduleOption" name="scheduleOption" onchange="scheduleOptionFunction();">
                                                              <option value="">Select</option>
                                                              <option value="AM">AM</option>
                                                              <option value="PM">PM</option>
                                                          </select>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Type of Request:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control @error('Request_type') is-invalid @enderror" id="RequestType" name="Request_type" onchange="">
                                                          <option value=""> - Select Option -</option>
                                                          <option value="New Signing" @if (old('Request_type') == "New Signing") {{ 'selected' }} @endif>New Signing</option>
                                                          <option value="Redraw" @if (old('Request_type') == "Redraw") {{ 'selected' }} @endif>Redraw</option>
                                                          <option value="Single Document" @if (old('Request_type') == "Single Document") {{ 'selected' }} @endif>Single Document</option>
                                                          <option value="Multiple Document" @if (old('Request_type') == "Multiple Document") {{ 'selected' }} @endif>Multiple Document</option>
                                                      </select>
                                                      @error('Request_type')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Is this the 1st trip to meet with this Customer?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_first_trip_to_meet" id="inlineRadio1" value="Yes" onclick="$('#tripDiv').hide()"  {{ old('is_first_trip_to_meet') =='Yes' ? 'checked' : '' }}   @if( !Config::has(old('is_first_trip_to_meet')) ) {{ 'checked' }} @endif>
                                                            <label class="form-check-label" for="inlineRadio1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_first_trip_to_meet" id="inlineRadio2" value="No" onclick="$('#tripDiv').toggle()"  {{ old('is_first_trip_to_meet') =='No' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="inlineRadio2">NO</label>
                                                        </div>
                                                      </div>

                                                      @error('is_first_trip_to_meet')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="tripDiv" style=" @if(old('is_first_trip_to_meet') =='No') '' @else display: none @endif">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="no_of_trips">Trip#:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control @error('no_of_trips') is-invalid @enderror" id="no_of_trips" name="no_of_trips" onchange="">
                                                          <option value="1" @if (old('no_of_trips') == "1") {{ 'selected' }} @endif>1</option>
                                                          <option value="2" @if (old('no_of_trips') == "2") {{ 'selected' }} @endif>2</option>
                                                          <option value="3" @if (old('no_of_trips') == "3") {{ 'selected' }} @endif>3</option>
                                                          <option value="4" @if (old('no_of_trips') == "4") {{ 'selected' }} @endif>4</option>
                                                          <option value="5" @if (old('no_of_trips') == "5") {{ 'selected' }} @endif>5</option>
                                                          <option value="More than 5" @if (old('no_of_trips') == "More than 5") {{ 'selected' }} @endif>More than 5</option>
                                                      </select>
                                                      @error('no_of_trips')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="invoiceDBA">Invoice client:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control @error('invoiceDba') is-invalid @enderror" id="invoiceDBA" name="invoiceDba" onchange="">
                                                          <option value=""> - Select Client -</option>
                                                          <option value="11">Andrew Dains</option>
                                                          <option value="12">John</option>
                                                          <option value="13">Alex</option>
                                                          <option value="14">Durai</option>
                                                          <option value="15">Lucifer</option>
                                                      </select>
                                                      @error('invoiceDba')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Method of Delivery for Documents</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc1" value="Email" onclick="$('#estimatedTimeDiv').show()" {{ old('methodDeliveryDoc') =='Email' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="methodDeliveryDoc1">Email</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc2" value="Overnight" onclick="$('#methodDeliveryDocTypeDiv').show();$('#estimatedTimeDiv').hide()" {{ old('methodDeliveryDoc') =='Overnight' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="methodDeliveryDoc2">Overnight</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc3" value="Courier" onclick="$('#methodDeliveryDocTypeDiv').show();$('#estimatedTimeDiv').hide()" {{ old('methodDeliveryDoc') =='Courier' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="methodDeliveryDoc3">Courier</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDoc" id="methodDeliveryDoc4" value="Customer Has Docs" onclick="$('#methodDeliveryDocTypeDiv').show();$('#estimatedTimeDiv').hide()" {{ old('methodDeliveryDoc') =='Customer Has Docs' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="methodDeliveryDoc4">Customer Has Docs</label>
                                                        </div>
                                                      </div>
                                                      @error('methodDeliveryDoc')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="estimatedTimeDiv" style="display: none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Estimated Time</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control @error('estimatedTime') is-invalid @enderror" name="estimatedTime" value="{{ old('estimatedTime') }}" placeholder="" />
                                                        @error('estimatedTime')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="methodDeliveryDocTypeDiv" style=" @if(old('methodDeliveryDoc') =='Overnight') '' @else display: none @endif">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Delivery Document Type:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDocType" id="methodDeliveryDocType1" value="Docs To Notary" {{ old('methodDeliveryDocType') =='Docs To Notary' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="methodDeliveryDocType1">Docs To Notary</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="methodDeliveryDocType" id="methodDeliveryDocType2" value="Docs To Customer" {{ old('methodDeliveryDocType') =='Docs To Customer' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="methodDeliveryDocType2">Docs To Customer</label>
                                                        </div>
                                                      </div>
                                                      @error('methodDeliveryDocType')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Loan Package Type:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control  @error('loanType1') is-invalid @enderror" id="" name="loanType1" onchange="">
                                                          <option value=""> - Select Loan Package -</option>
                                                          <option value="Refi 1st" @if (old('loanType1') == "Refi 1st") {{ 'selected' }} @endif>Refi 1st</option>
                                                          <option value="Refi 1st 2nd" @if (old('loanType1') == "Refi 1st 2nd") {{ 'selected' }} @endif>Refi 1st 2nd</option>
                                                          <option value="Purchase 1st" @if (old('loanType1') == "Purchase 1st") {{ 'selected' }} @endif>Purchase 1st</option>
                                                          <option value="Purchase 1st 2nd" @if (old('loanType1') == "Purchase 1st 2nd") {{ 'selected' }} @endif>Purchase 1st 2nd</option>
                                                          <option value="Seller Signing" @if (old('loanType1') == "Seller Signing") {{ 'selected' }} @endif>Seller Signing</option>
                                                          <option value="Redraw" @if (old('loanType1') == "Redraw") {{ 'selected' }} @endif>Redraw</option>
                                                          <option value="HELOC" @if (old('loanType1') == "HELOC") {{ 'selected' }} @endif>HELOC</option>
                                                          <option value="Reverse Mortgage" @if (old('loanType1') == "Reverse Mortgage") {{ 'selected' }} @endif>Reverse Mortgage</option>
                                                          <option value="2nd trip" @if (old('loanType1') == "2nd trip") {{ 'selected' }} @endif>2nd trip</option>
                                                          <option value="Single Document @if (old('loanType1') == "Single Document") {{ 'selected' }} @endif">Single Document</option>
                                                          <option value="Signing Fee" @if (old('loanType1') == "Signing Fee") {{ 'selected' }} @endif>Signing Fee</option>
                                                          <option value="Loan Signing" @if (old('loanType1') == "Loan Signing") {{ 'selected' }} @endif>Loan Signing</option>
                                                          <option value="SBA Loan" @if (old('loanType1') == "SBA Loan") {{ 'selected' }} @endif>SBA Loan</option>
                                                          <option value="Commercial Transaction" @if (old('loanType1') == "Commercial Transaction") {{ 'selected' }} @endif>Commercial Transaction</option>
                                                          <option value="Other" @if (old('loanType1') == "Other") {{ 'selected' }} @endif>Other</option>
                                                      </select>
                                                      @error('loanType1')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Lender Name:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select class="form-control  @error('lender') is-invalid @enderror" id="" name="lender" onchange="">
                                                          <option value=""> - Select Lender -</option>
                                                          @foreach($lenders as $lender)
                                                            <option value="{{ $lender->id }}"
                                                              @if (old('lender') == $lender->id ) {{ 'selected' }} @endif>{{ $lender->name }}
                                                            </option>
                                                          @endforeach
                                                      </select>
                                                      @error('lender')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Escrow #</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control @error('Escrow') is-invalid @enderror" name="Escrow" value="{{ old('Escrow') }}" placeholder="Escrow Number" />
                                                        @error('Escrow')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Customer Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="first-name" class="form-control @error('customerName') is-invalid @enderror" value="{{ old('customerName') }}" name="customerName" placeholder="Name" />
                                                        @error('customerName')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="signer2" class="custom-control-input" value="y" id="signer2" onclick="$('#customerName2Div').toggle()" {{ old('signer2') =='y' ? 'checked' : '' }} />
                                                        <label class="custom-control-label" for="signer2">
                                                           Add Another Signer
                                                        </label>
                                                    </div>
                                                </div>
                                                @error('signer2')
                                                    <div class="d-block invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="col-12" id="customerName2Div" style=" @if(old('signer2') =='y') '' @else display: none @endif">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>2nd Customer Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control @error('CocustomerName') is-invalid @enderror" name="CocustomerName" value="{{ old('CocustomerName') }}" placeholder="Co Customer Name" />
                                                        @error('CocustomerName')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Marital Status</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="maritalStatus" id="maritalStatus1" value="1" {{ old('maritalStatus') =='1' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="maritalStatus1">Married</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="maritalStatus" id="maritalStatus2" value="0" {{ old('maritalStatus') =='0' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="maritalStatus2">Unmarried</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="maritalStatus" id="maritalStatus2" value="2" {{ old('maritalStatus') =='2' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="maritalStatus2">TBD</label>
                                                        </div>
                                                      </div>
                                                      @error('maritalStatus')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Signing Location :</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="signingLocation" id="signingLocation1" value="0" onclick="$('#businessNameDiv').hide()" {{ old('signingLocation') =='0' ? 'checked' : '' }}   @if( !Config::has(old('signingLocation')) ) {{ 'checked' }} @endif>
                                                            <label class="form-check-label" for="signingLocation1">Residence</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="signingLocation" id="signingLocation2" value="1" onclick="$('#businessNameDiv').show()" {{ old('signingLocation') =='1' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="maritalStatus2">Business</label>
                                                        </div>
                                                      </div>
                                                      @error('signingLocation')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="businessNameDiv" style=" @if(old('signingLocation') =='1') '' @else display: none @endif">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Business Name</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="" class="form-control @error('businessName') is-invalid @enderror" name="businessName" value="{{ old('businessName') }}" placeholder="Business Name" />
                                                        @error('businessName')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Signing Address</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="address" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" placeholder="Address" />
                                                        @error('address')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control @error('address2') is-invalid @enderror" name="address2" value="{{ old('address2') }}" placeholder="" />
                                                    @error('address2')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>City</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="city" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" placeholder="CITY NAME" />
                                                        @error('city')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>State:</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <select id="state" class="form-control @error('state') is-invalid @enderror" name="state" onchange="">
                                                          <option value="">Select State</option>
                                                          @foreach($states as $state)
                                                            <option value="{{ $state->stateID }}"
                                                              @if (old('state') == $state->stateID ) {{ 'selected' }} @endif>{{ $state->stateName }}
                                                            </option>
                                                          @endforeach
                                                      </select>
                                                      @error('state')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>ZIP</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="zipCode" class="form-control @error('zipCode') is-invalid @enderror" name="zipCode" value="{{ old('zipCode') }}" placeholder="ZIP CODE" />
                                                        @error('zipCode')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="contact-info">Phone</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="number" id="contact-info" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Phone" />
                                                        @error('phone')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Customer Email (Optional)</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="email" id="email-id" class="form-control @error('customerEmail') is-invalid @enderror" name="customerEmail" value="{{ old('customerEmail') }}" placeholder="Email" />
                                                        @error('customerEmail')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Comments/Special Instructions	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <textarea type="text" name="comments" class="form-control @error('comments') is-invalcoid @enderror" rows="3" cols="80" placeholder="Write...">{{ old('comments') }}</textarea>
                                                      @error('comments')
                                                          <div class="invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <Label>Funds Due Escrow?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_fund_due_escrow" id="is_fund_due_escrow1" value="Yes" onclick="$('#isFundDueEscrowDiv').show()" {{ old('is_fund_due_escrow') =='Yes' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="is_fund_due_escrow1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_fund_due_escrow" id="is_fund_due_escrow2" value="No" onclick="$('#isFundDueEscrowDiv').hide()" {{ old('is_fund_due_escrow') =='No' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="is_fund_due_escrow2">NO</label>
                                                        </div>
                                                      </div>
                                                      @error('is_fund_due_escrow')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12" id="isFundDueEscrowDiv" style=" @if(old('is_fund_due_escrow') =='Yes') '' @else display: none @endif">
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Fund Due Amount	</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="is_collect_due_HUD" id="is_collect_due_HUD1" value="0" onclick="$('#isFundDueEscrowDiv').show()" {{ old('is_collect_due_HUD') =='0' ? 'checked' : '' }} >
                                                                <label class="form-check-label" for="is_collect_due_HUD1">
                                                                  <div class="input-group pl-1">
                                                                      <div class="input-group-prepend" style="height: 2rem">
                                                                          <span class="input-group-text" id="basic-addon1">$</span>
                                                                      </div>
                                                                      <input type="number" name="fund_due_amount" value="{{ old('fund_due_amount') }}" class="form-control" placeholder="0.00" aria-label="" aria-describedby="basic-addon1" id="fundValue" style="height: 2rem; width: 10rem">
                                                                  </div>
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="is_collect_due_HUD" id="is_collect_due_HUD2" value="1" {{ old('is_collect_due_HUD') =='1' ? 'checked' : '' }}  onclick="$( "#fundValue" ).prop( "disabled", true );" >
                                                                <label class="form-check-label" for="is_collect_due_HUD2"> Collect Amount Due On HUD</label>
                                                            </div>
                                                          </div>
                                                          @error('is_collect_due_HUD')
                                                              <div class="d-block invalid-feedback">
                                                                  {{ $message }}
                                                              </div>
                                                          @enderror

                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Method (check all that apply):	</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                        <div class="form-group">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkMethod[]" class="custom-control-input" id="checkMethod1" value="Personal Check" />
                                                                <label class="custom-control-label" for="checkMethod1">Personal Check</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkMethod[]" class="custom-control-input" id="checkMethod2" value="Cashiers Check" />
                                                                <label class="custom-control-label" for="checkMethod2">Cashiers Check</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkMethod[]" class="custom-control-input" id="checkMethod3" value="Customer To Wire" />
                                                                <label class="custom-control-label" for="checkMethod3">Customer To Wire</label>
                                                            </div>
                                                        </div>
                                                        @error('checkMethod')
                                                            <div class="d-block invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                      </div>
                                                  </div>
                                              </div>
                                            </div>



                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Customer Signing in Trust?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_borrower_signing_trust" id="isTrust1" value="Yes" onclick="$('#isTrustDiv').show(); $('#isTrust2Div').show()" {{ old('is_borrower_signing_trust') =='Yes' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="isTrust1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_borrower_signing_trust" id="isTrust2" value="No" onclick="$('#isTrustDiv').hide(); ; $('#isTrust2Div').hide()" {{ old('is_borrower_signing_trust') =='No' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="isTrust2">NO</label>
                                                        </div>
                                                      </div>
                                                      @error('is_borrower_signing_trust')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isTrustDiv" style=" @if(old('is_borrower_signing_trust') =='Yes') '' @else display: none @endif">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Do you want the customer to sign with trustee verbiage?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_trustee_verbiage" id="is_trustee_verbiage1" value="Yes" onclick="" {{ old('is_trustee_verbiage') =='Yes' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="is_trustee_verbiage1">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_trustee_verbiage" id="is_trustee_verbiage2" value="No" onclick="" {{ old('is_trustee_verbiage') =='No' ? 'checked' : '' }} >
                                                            <label class="form-check-label" for="is_trustee_verbiage2">Customer signs their name only</label>
                                                        </div>
                                                      </div>
                                                      @error('is_trustee_verbiage')
                                                          <div class="d-block invalid-feedback">
                                                              {{ $message }}
                                                          </div>
                                                      @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="isTrust2Div" style="display:none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Notes About Trustee	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <textarea type="text" name="trustee_notes" class="form-control" rows="2" cols="" placeholder="Write Note..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Customer Signing with POA?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_borrower_signing_POA"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_borrower_signing_POA"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="" style="">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Esigning?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_amtrust_signing"  value="Yes" onclick="$('#isAmTrust').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_amtrust_signing"  value="No" onclick="$('#isAmTrust').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3" id="isAmTrust" style="display:none">
                                              <div class="form-group row pl-1">
                                                <div class="demo-inline-spacing">
                                                  <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="is_amtrust"  value="Yes" onclick="">
                                                      <label class="form-check-label">NYCB</label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                      <input class="form-check-input" type="radio" name="is_amtrust"  value="No" onclick="">
                                                      <label class="form-check-label">Flagstar</label>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Provident Funding Loan?	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_provident_funding_loan"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_provident_funding_loan"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Please Obtain Copy of Customer ID?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="cid"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="cid"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do You Want To Provide Loan Officer Contact Info?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_loan_officer"  value="Yes" onclick="$('#loanOfficerDiv').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_loan_officer"  value="No" onclick="$('#loanOfficerDiv').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="loanOfficerDiv" style="display:none">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="first-name">Phone Number	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="text" id="" class="form-control" name="" placeholder="Loan Officer Number" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you want to CC anyone on this one order? (Your Assistant, Co-worker, LO or Broker etc.)</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_clientEmailCC"  value="Yes" onclick="$('#clientEmailCCDiv').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_clientEmailCC"  value="No" onclick="$('#clientEmailCCDiv').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" id="clientEmailCCDiv" style="display: none">
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label for="first-name">Additional party email to CC</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                        <input type="text" id="" class="form-control" name="" placeholder="email" />
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Initial Order Request Email</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x1" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x1" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Notary Assigned Order Conformation and ProLink Invoice Email</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x2" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x2" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Signing Completed And Prolink Invoice Email</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x3" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x3" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you want to change the default return method?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_update_instructions"  value="Yes" onclick="$('#is_update_instructions').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_update_instructions"  value="No" onclick="$('#is_update_instructions').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="is_update_instructions" style="display: none">
                                              <div class="col-sm-9 offset-sm-3" style="padding-bottom: 2rem">
                                                  <p class="card-text" style="font-weight: 800">
                                                      Default Return Method:
                                                      Return Docs To:
                                                      ProLink
                                                      Attn: Adam Babish
                                                      TBD
                                                      San Jose California 95136

                                                      Method of shipping: FedEx
                                                      Choose Option: Priority Overnight (10:30 A.M.)
                                                      Account# : See Label
                                                    </p>
                                              </div>
                                              <div class="col-12" style="margin-left: -1rem;margin-right: -1rem;">
                                                  <div class="form-group row">
                                                      <div class="col-sm-3 col-form-label">
                                                          <label>Would you like to have the notary hold docs for courier pick up?</label>
                                                      </div>
                                                      <div class="col-sm-9 pl-2">
                                                          <div class="demo-inline-spacing">
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x5" value="Yes" onclick="">
                                                                <label class="form-check-label">YES</label>
                                                            </div>
                                                            <div class="form-check form-check-inline" style="margin-top: 0rem">
                                                                <input class="form-check-input" type="radio" name="x5" value="HUD" onclick="">
                                                                <label class="form-check-label">NO</label>
                                                            </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Mandatory For The notary to make the same day pick up?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="sameDayPickup"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="sameDayPickup"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Scanbacks Required?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_required"  value="Yes" onclick="">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_required"  value="No" onclick="">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you Want to Upload Docs Now?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_requiredc"  value="Yes" onclick="$('#allDocUploaded').show(); $('#Uploader').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="scanbacks_requiredc"  value="No" onclick="$('#allDocUploaded').hide(); $('#Uploader').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">How will Invoice Be Paid?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="form-check">
                                                          <div class="form-group">
                                                              <input class="form-check-input radio-sazal" type="radio" name="invoicePaidMethod"  value="Yes" onclick="$('#invoicePaidMethod').show()">
                                                              <label class="form-check-label radio-sazal-label">Inside Of Escrow (I will be responsible to pay the fee)</label>
                                                          </div>
                                                          <div class="form-group">
                                                              <input class="form-check-input radio-sazal" type="radio" name="invoicePaidMethod"  value="Yes" onclick="$('#invoicePaidMethod').show()">
                                                              <label class="form-check-label radio-sazal-label">Outside Of Escrow (Someone else will be responsible to pay the fee)</label>
                                                          </div>
                                                          <div class="form-group">
                                                              <input class="form-check-input radio-sazal" type="radio" name="invoicePaidMethod"  value="Yes" onclick="$('#invoicePaidMethod').show()">
                                                              <label class="form-check-label radio-sazal-label">Other</label>
                                                          </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3" id="allDocUploaded" style="display:none">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="allDocUploaded" class="custom-control-input" />
                                                        <label class="custom-control-label">
                                                           All Docs Uploaded
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-9 offset-sm-3" id="Uploader" style="display:none">
                                                <div class="form-group">
                                                    <form action="#" class="dropzone dropzone-area" id="dpz-multiple-files">
                                                        <div class="dz-message">Drop files here or click to upload.</div>
                                                    </form>
                                                </div>
                                            </div> -->
                                            <!-- multi file upload starts -->
                                            <!-- <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <p class="card-text" style="">
                                                              By default, dropzone is a multiple file uploader. User can either click on the dropzone area and select
                                                              multiple files or just drop all selected files in the dropzone area. This example is the most basic setup
                                                              for dropzone.
                                                            </p>
                                                            <form action="#" class="dropzone dropzone-area" id="dpz-multiple-files">
                                                                <div class="dz-message">Drop files here or click to upload.</div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- multi file upload ends -->

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="">Do you want to add Internall notes on this order?</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <div class="demo-inline-spacing">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_internal_notes"  value="Yes" onclick="$('#textarea').show()">
                                                            <label class="form-check-label">YES</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="is_internal_notes"  value="No" onclick="$('#textarea').hide()">
                                                            <label class="form-check-label">NO</label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3" id="textarea" style="display:none; padding-bottom: 3rem">
                                                <textarea name="nameh" class="form-control" rows="3" cols="" placeholder="Add some note..."></textarea>
                                            </div>



                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                                <button type="reset" class="btn btn-info mr-1">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->



            </div>
        </div>
        <!-- <span class="street-address">205 Glen Haven Ct</span>, <span class="locality">Swedesboro</span>, <span class="region">NJ</span> <span class="postal-code">08085-3051</span>, <span class="country-name">USA</span> -->
        <div id="mapString" class="d-none"></div>
        <div id="mapString2" class="d-none"></div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyB8zsk0CYFsdI8-4ItciBolf7-2vc1s6r4"></script>
    <script>
        /// Google Places API
        var searchInput = 'address';

        $(document).ready(function () {
          var autocomplete;
          autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
              types: ['geocode'],
              componentRestrictions: {
                  country: "us"
              }
          });

          google.maps.event.addListener(autocomplete, 'place_changed', function () {
              var near_place = autocomplete.getPlace();
              console.log(near_place);
              // console.log(near_place.address_components[1].long_name);
              // save data with div id
              document.getElementById("mapString").innerHTML = near_place.adr_address;

              var city = document.getElementsByClassName("locality");
              document.getElementById('city').value = city[0].innerHTML;

              var zip = document.getElementsByClassName("postal-code");
              var data = zip[0].innerHTML;
              var res = data.split("-");
              document.getElementById('zipCode').value = res[0];


              document.getElementById('address').value = near_place.name;

              // var state = document.getElementsByClassName("region");
              // document.getElementById('state').value = state[0].innerHTML;

          });
        });
    </script>
@endsection
