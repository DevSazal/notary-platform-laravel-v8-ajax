<!--  -->

@extends('layouts.app')

@section('title', 'Guides')
@section('description', 'Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.')
@section('keyword', 'admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app')

@section('css')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/plugins/forms/form-file-uploader.css') }}">

@endsection

@section('content')
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Guides</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Home</a>
                                    </li>
                                    <!-- <li class="breadcrumb-item active">
                                      <a href="{{ url('admin/client') }}">Client</a>
                                    </li> -->
                                    <li class="breadcrumb-item active">Guide
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts flatpickr">
                    <div class="row">
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Provident Funding Loan</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>Provident Loan Instructions.pdf</span>
                                                        </button>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Borrower Signing in Trust</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <!-- <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>Provident Loan Instructions.pdf</span>
                                                        </button> -->

                                                        <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Borrower Signing with POA</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>Power Of Attorney Signing Instructions.pdf</span>
                                                        </button>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Seller Signing</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <!-- <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>Power Of Attorney Signing Instructions.pdf</span>
                                                        </button> -->

                                                        <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Notary survey</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>ProLink Survey.pdf</span>
                                                        </button>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Electronic Marketing Material</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <!-- <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>ProLink Survey.pdf</span>
                                                        </button> -->

                                                        <div class="demo-inline-spacing">
                                                          <a href="#">
                                                            <div class="badge badge-light-primary mr-1">
                                                                <i class="fas fa-link mr-25"></i>
                                                                <span>ProLink Flyer.pdf</span>
                                                            </div>
                                                          </a>
                                                        </div>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Lender</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Title :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="lenderT" placeholder="Type A Lender Name" aria-describedby="button-addon2" />
                                                            <div class="input-group-append" id="button-addon2">
                                                                <button class="btn btn-outline-primary" type="button"><i class="fas fa-plus mr-25"></i> Add</button>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Lenders :	</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <div class="input-group">
                                                            <select class="form-control" id="eventLenderDiv" name="eventLenderInput" onchange="eventSetLender(value)" aria-describedby="button-addon2">
                                                                <option value="">-- Select A Lender --</option>
                                                                <option value="Bank Of America">Bank Of America</option>
                                                                <option value="NYCB Bank">NYCB Bank</option>
                                                                <option value="Provident Funding">Provident Funding</option>
                                                                <option value="Union Bank">Union Bank</option>
                                                                <option value="US Bank">US Bank</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                            <!-- <input type="text" class="form-control" placeholder="Button on right" aria-describedby="button-addon2" /> -->
                                                            <div class="input-group-append" id="button-addon2">
                                                                <button class="btn btn-outline-primary" type="button"><i class="far fa-edit mr-25"></i> Edit</button>
                                                                <button class="btn btn-outline-danger" type="button"><i class="far fa-trash-alt mr-25"></i> Delete</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Canned Responses</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Title :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="cannedResT" placeholder="type " aria-describedby="button-addon2" />
                                                            <div class="input-group-append" id="button-addon2">
                                                                <button class="btn btn-outline-primary" type="button"><i class="fas fa-plus mr-25"></i> Add</button>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Responses :	</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <div class="input-group">
                                                            <select class="form-control" id="eventCannedResDiv" name="eventCannedResInput" onchange="eventSetCannedRes(value)" aria-describedby="button-addon2">
                                                                <option value="">-- Select A Response --</option>
                                                                <option value="Checking in on the status of this signing.">Checking in on the status of this signing. </option>
                                                                <option value="Testing 2...">Testing 2...</option>
                                                                <option value="Testing 3...">Testing 3...</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                            <!-- <input type="text" class="form-control" placeholder="Button on right" aria-describedby="button-addon2" /> -->
                                                            <div class="input-group-append" id="button-addon2">
                                                                <button class="btn btn-outline-primary" type="button"><i class="far fa-edit mr-25"></i> Edit</button>
                                                                <button class="btn btn-outline-danger" type="button"><i class="far fa-trash-alt mr-25"></i> Delete</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Notes for new Notary</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <!-- <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>ProLink Survey.pdf</span>
                                                        </button> -->

                                                        <div class="demo-inline-spacing">
                                                          <a href="#">
                                                            <div class="badge badge-light-primary mr-1">
                                                                <i class="fas fa-link mr-25"></i>
                                                                <span>Notary Guidelines.pdf</span>
                                                            </div>
                                                          </a>
                                                        </div>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Quicken Lender Type Attachment</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Current Document :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <!-- <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>ProLink Survey.pdf</span>
                                                        </button> -->

                                                        <div class="demo-inline-spacing">
                                                          <a href="#">
                                                            <div class="badge badge-light-primary mr-1">
                                                                <i class="fas fa-link mr-25"></i>
                                                                <span>ATTN NOTARY-QUICKEN.pdf</span>
                                                            </div>
                                                          </a>
                                                        </div>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Upload document :	</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                      <input type="file" id="" class="form-control" name="ufile" placeholder="" />

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="reset" class="btn btn-primary mr-1">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->
                        <!-- H form col start -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">File Transfer Settings</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Manage Uploaded Files :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <!-- <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>ProLink Survey.pdf</span>
                                                        </button> -->

                                                        <div class="demo-inline-spacing">
                                                          <a href="#">
                                                            <div class="badge badge-primary mr-1">
                                                                <i class="far fa-eye mr-25"></i>
                                                                <span>Let's check the available files</span>
                                                            </div>
                                                          </a>
                                                        </div>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label>Report of Usage :</label>
                                                    </div>
                                                    <div class="col-sm-9">

                                                        <!-- <button type="button" class="btn btn-flat-primary round" onclick="window.location='{{ url("/admin") }}'">
                                                            <i data-feather="link" class="mr-25"></i>
                                                            <span>ProLink Survey.pdf</span>
                                                        </button> -->

                                                        <div class="demo-inline-spacing">
                                                          <a href="#">
                                                            <div class="badge badge-primary mr-1">
                                                                <i class="fas fa-chart-line mr-25"></i>
                                                                <span>Let's open the usages report</span>
                                                            </div>
                                                          </a>
                                                        </div>

                                                        <!-- <div class="demo-inline-spacing">
                                                          <div class="badge badge-light-warning mr-1">Not Found</div>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- H form col End -->




                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->



            </div>
        </div>
@endsection

@section('js')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('theme/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('theme/app-assets/js/scripts/forms/form-file-uploader.js') }}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">

      $(function () {
        // $('#dataTable_orders').DataTable();

      });

      function changeCourier($i) {
        // alert($i);
        $('#shippingRoot').show();

        if ($i == 1) {
          $('#shippingUPS').show();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 2) {
          $('#shippingUPS').hide();
          $('#shippingFedex').show();
          $('#shippingDHL').hide();
          $('#shippingNull').hide();
        } else if ($i == 3) {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').show();
          $('#shippingNull').hide();
        } else {
          $('#shippingUPS').hide();
          $('#shippingFedex').hide();
          $('#shippingDHL').hide();
          $('#shippingNull').show();

          $('#shippingRoot').hide();
        }
       }
      function eventSetLender($i) {
        // alert($i);
        document.getElementById('lenderT').value = $i;
      }
      function eventSetCannedRes($i) {
        document.getElementById('cannedResT').value = $i;
      }
    </script>
@endsection
