<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keyword')">

    <meta name="author" content="PIXINVENT">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - TheProlink</title>
    <link rel="apple-touch-icon" href="{{ asset('theme/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('theme/app-assets/images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
          rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/charts/apexcharts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/vendors/css/extensions/toastr.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/app-assets/css/themes/bordered-layout.css') }}">

    @yield('css')

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/assets/css/style.css') }}">
    <!-- END: Custom CSS-->
</head>



<body class="vertical-layout vertical-menu-modern light" data-menu=" vertical-menu-modern" data-layout="" style="" data-framework="laravel" data-asset-path="{{ asset('/') }}">

  <!-- BEGIN: Content-->
  <div class="app-content content " style="margin-left: unset">
    <div class="content-wrapper container p-0">
      <div class="content-body">

        @yield('content')

      </div>
    </div>
  </div>
  <!-- End: Content-->



  <!-- BEGIN: Footer-->
  <!-- <cemter>
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-md-center d-block d-md-inline-block mt-25">COPYRIGHT &copy; {{ date('Y') }}<a class="ml-25" href="{{ url('/') }}" target="_blank">TheProLink Signing Services</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span>
        </p>
    </footer>
  </cemter> -->

  <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
  <!-- END: Footer-->


  <!-- BEGIN: Vendor JS-->
  <script src="{{ asset('theme/app-assets/vendors/js/vendors.min.js') }}"></script>
  <!-- BEGIN Vendor JS-->

  <!-- BEGIN: Page Vendor JS-->
  <script src="{{ asset('theme/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
  <script src="{{ asset('theme/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
  <!-- END: Page Vendor JS-->

  <!-- BEGIN: Theme JS-->
  <script src="{{ asset('theme/app-assets/js/core/app-menu.js') }}"></script>
  <script src="{{ asset('theme/app-assets/js/core/app.js') }}"></script>
  <!-- END: Theme JS-->

  <!-- BEGIN: Page JS-->
  <!-- <script src="{{asset('theme/app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script> -->
  <!-- END: Page JS-->

  <script>
      $(window).on('load', function() {
          if (feather) {
              feather.replace({
                  width: 14,
                  height: 14
              });
          }
      })
  </script>

</body>

</html>
