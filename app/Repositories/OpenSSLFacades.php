<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Facade; // ... facade called serialize 02

class OpenSSLFacades extends Facade
{
    protected static function getFacadeAccessor() // ... facade called serialize 05
    {
        return 'openssl';
    }
}
