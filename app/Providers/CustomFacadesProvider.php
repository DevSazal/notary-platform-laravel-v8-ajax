<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CustomFacadesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() // ... facade called serialize 03
    {
        //
        app()->bind('openssl', function(){  //Keep in mind this "openssl" must be return from facades accessor
            return new \App\Repositories\OpenSSL;
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
