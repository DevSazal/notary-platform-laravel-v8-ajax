<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\State;

class NotaryRegis extends Model
{
    use HasFactory;
    protected $table = 'notaryregis';
    public $timestamps = false;
    protected $primaryKey = 'notaryID';

    public function state(){
        return $this->belongsTo(State::class, 'state', 'stateID');
    }

}
