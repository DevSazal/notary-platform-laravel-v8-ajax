<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\ClientRegis;

class NotaryOrder extends Model
{
    use HasFactory;

    protected $table = 'notaryorder';
    public $timestamps = false;
    protected $primaryKey = 'notaryOrderID';

    public function client(){
        return $this->belongsTo(ClientRegis::class, 'clientID', 'clientID');
    }

}
