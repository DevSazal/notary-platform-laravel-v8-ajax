<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientRegis extends Model
{
    use HasFactory;

    protected $table = 'clientregis'; //0
    public $timestamps = false;
    protected $primaryKey = 'clientID';

}
