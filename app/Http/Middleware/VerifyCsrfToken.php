<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //  here you can exlude theurls ok
        '/admin/notary/data-list',
        'admin/notary/{id}/active',
        'admin/notary/{id}/inactive',
        '/admin/order/unassigned',
        '/admin/order/hold',
        '/admin/order/',

    ];
}
