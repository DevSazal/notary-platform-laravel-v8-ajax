<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

      // Authenticatation Gate
      if (Auth::guard('admin')->check()){
          // Admin Role Gate
          return $next($request);
      }
      return redirect('/admin/login');

    }
}
