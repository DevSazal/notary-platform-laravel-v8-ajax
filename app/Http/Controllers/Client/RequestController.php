<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RequestController extends Controller
{
  public function mousepad(){
    return view('client.request.mousepad');
  }
}
