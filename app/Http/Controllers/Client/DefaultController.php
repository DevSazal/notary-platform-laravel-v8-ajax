<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DefaultController extends Controller
{
  public function __construct(){
    // $this->middleware('client');
  }

  public function index(){
    return view('client.index');
  }
  public function suggestion(){
    return view('client.suggestion.index');
  }
  public function coWorker(){
    return view('client.co-worker.index');
  }
}
