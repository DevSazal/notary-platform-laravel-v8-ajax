<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckController extends Controller
{
  public function paid(){
    return view('admin.check.paid');
  }
  public function unpaid(){
    return view('admin.check.unpaid');
  }
}
