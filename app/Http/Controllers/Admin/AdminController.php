<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{

  public function __construct(){
    // $this->middleware('auth:admin');
    $this->middleware('admin');
  }

  public function index(){
    return view('admin.index');
  }
  public function admin(){
    return view('admin.admin.index');
  }
  public function create(){
    return view('admin.admin.create');
  }

}
