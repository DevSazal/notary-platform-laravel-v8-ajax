<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuickBookController extends Controller
{
  public function customers(){
    return view('admin.quickbook.customer');
  }
  public function vendors(){
    return view('admin.quickbook.vendor');
  }
  public function invoices(){
    return view('admin.quickbook.invoice');
  }
  public function bills(){
    return view('admin.quickbook.bill');
  }
}
