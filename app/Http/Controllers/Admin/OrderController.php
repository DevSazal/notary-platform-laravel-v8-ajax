<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;

use Validator;
use Illuminate\Validation\Rule;

use App\Models\NotaryOrder;
use App\Models\State;
use App\Models\Lender;

class OrderController extends Controller
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.order.index');
    }

    public function create()
    {
        $array['states'] = State::orderBy('stateName', 'asc')->get();
        $array['lenders'] = Lender::orderBy('name', 'asc')->get();
        return view('admin.order.create')->with($array);
    }

    public function registerOrder(Request $request){

      // ...  New Order Data Validation For Notary
      $validator = Validator::make($request->all(), [
        'Request_type' => 'required|string',
        'is_first_trip_to_meet' => 'required',
        'no_of_trips' => 'nullable|string',
        'invoiceDba' => 'required|string',
        'methodDeliveryDoc' => 'required|string',
        'estimatedTime' => 'nullable|string',
        'methodDeliveryDocType' => 'nullable|string',
        'loanType1' => 'required|string',
        'lender' => 'required',
        'Escrow' => 'required|string',
        'customerName' => 'required|string|min:4',
        'signer2' => 'nullable',
        'CocustomerName' => 'nullable|string',
        'maritalStatus' => 'required',
        'signingLocation' => 'nullable|integer',
        'businessName' => 'nullable|string',
        'address' => 'required|string|min:3',
        'address2' => 'string|nullable',
        'city' => 'required|string',
        'state' => 'required|integer',
        'zipCode' => 'required|string',
        'phone' => 'string|nullable',
        // 'businessPhone' => Rule::requiredIf(!$request->homePhone && !$request->cellPhone),
        'customerEmail' => 'nullable|email',
        'comments' => 'nullable',
        'is_fund_due_escrow' => 'required',
        'is_collect_due_HUD' => Rule::requiredIf($request->is_fund_due_escrow == 'Yes'),
        'fund_due_amount' => 'nullable',
        'checkMethod' => Rule::requiredIf($request->is_fund_due_escrow == 'Yes'),
        'is_borrower_signing_trust' => 'required',
        'is_trustee_verbiage' => Rule::requiredIf($request->is_borrower_signing_trust == 'Yes'),


      ]);


      if ($validator->fails()) {
          return back()->withErrors($validator)
                        ->withInput();
          // return dd($validator->errors());
      }else {

        // ... save order data

      }
    }

    public function indexAjax(Request $request)
    {
      $start = $request->start;
      $length  = $request->length;
      $totalCount = NotaryOrder::count();

      $search = $request->search['value'];

      // incase search box is not empty
      if(!empty(trim($search))){
        // apply search here for now leaving empty
         // replace with the number fo records filtered from total
      }else{
        $filteredCount = $totalCount;
      }

      if(trim($search) != ''){
          $data = NotaryOrder::select('*')
              // ->where('status1', 1)
              ->where(function($result) use($search){
                // inside condition
                $result->where('orderDate', 'LIKE','%'.$search.'%')
                        ->orwhere('notaryOrderID', 'LIKE','%'.$search.'%')
                        ->orwhere('Escrow', 'LIKE','%'.$search.'%')
                        ->orwhere('customerName', 'LIKE','%'.$search.'%')
                        ->orwhere('customerLastname', 'LIKE','%'.$search.'%')
                        ->orwhereHas('client', function($query) use($search){
                              // ... Join table Search
                              $query->where('titleCompany', 'LIKE','%'.$search.'%');
                          });
              })
              ->take($length)->skip($start)->get();
          $filteredCount = count($data);
      }else{
          $data = NotaryOrder::select('*')->take($length)->skip($start)->get();
      }



      $listing  = array();
      $colorClasses['red'] = 'bg-danger';
      $colorClasses['blue'] = 'bg-info';
      $colorClasses['yellow'] = 'bg-warning';
      $colorClasses['green']= 'bg-success';

      foreach ($data AS $val){
        // dd($val->client->titleCompany);
        $listItem = array();
          $listItem['notaryOrderID'] = $val->notaryOrderID; // notaryOrderID == OrderID
          $listItem['Escrow'] = $val->Escrow;
          $listItem['borrowerName'] = $val->customerName.' '.$val->customerLastname;
          $listItem['yourCompanyName'] = $val->client->titleCompany;
          $listItem['closerName'] = $val->client->firstName.' '.$val->client->lastName;
          $listItem['requestedAppTime'] = $val->requestedAppTime;
          // TODO: need to get exact count of strikes (Shehroz to provide query logic for that)


          $deleteHtml = '';
          $deleteHtml .= '<div>';
          $deleteHtml .= '<input type="checkbox" name="delete_hold_'.$val->notaryOrderID.'" value="true" onClick="deleteMsg()">';
          $deleteHtml .= '</div>';

          $listItem['delete'] = $deleteHtml;

          // For Note HTML
          $noteHtml = '';
          $noteHtml .= '<a href="#" target="popup" onclick="window.open(\'\',\'popup\',\'width=600,height=600,scrollbars=no,resizable=no\'); return false;">';
          $noteHtml .= '<i class="far fa-comment-alt"></i>';
          $noteHtml .= '</a>';

          $listItem['note'] = $noteHtml;

          // Docs Reserve
          $rcvHtml = '';
          if ($val->methodDeliveryDoc == "Overnight") {
              $rcvHtml .= '<div class="badge badge-light-dark mr-1">O/N</div>';
          } elseif ($val->methodDeliveryDoc == "Courier") {
              $rcvHtml .= '<div class="badge badge-light-dark mr-1">C/O</div>';
          }else {
              if ($val->is_docs_received == 1 || $val->allDocUploaded == 1 || $val->status1 == 1 || $val->methodDeliveryDoc == "Customer Has Docs") {
                $rcvHtml .= '<div class="badge badge-light-primary mr-1">Yes</div>';
              }else {
                $rcvHtml .= '<div class="badge badge-light-danger mr-1">No</div>';
              }
          }

          $listItem['received'] = $rcvHtml;

          // Notary Accress
          $notaryAccessHtml = '';
          if ($val->notaryViewDateTime == "0000-00-00 00:00:00") {
              $notaryAccessHtml .= '<div class="badge badge-light-warning mr-1">NO</div>';
          } else {
              $notaryAccessHtml .= '<div class="badge badge-success mr-1">Yes</div>';
          }

          $listItem['notaryAccess'] = $notaryAccessHtml;

          // For NotaryOrder status
          $statusHtml = '';
          if ($val->status1 == 0) {
              $statusHtml .= '<div class="badge badge-pill badge-light-warning mr-1">Pending</div>';
          }elseif ($val->status1 == 1) {
              $statusHtml .= '<div class="badge badge-pill badge-light-success mr-1">Closed</div>';
          }elseif ($val->status1 == 2) {
              $statusHtml .= '<div class="badge badge-pill badge-light-secondary mr-1">Cancelled</div>';
          }elseif ($val->status1 == 3) {
              $statusHtml .= '<div class="badge badge-pill badge-light-danger mr-1">On-hold</div>';
          }else {
              $statusHtml .= '<div class="badge badge-pill badge-light-primary mr-1">Triped</div>';
          }
          $statusHtml .= '';
          $listItem['status'] = $statusHtml;

          $listing[] = $listItem;
      }

      $orders['data'] = $listing;
      $orders['draw'] = $request->draw;
      $orders['recordsTotal'] = $totalCount;
      $orders['recordsFiltered'] = $filteredCount;

      // echo json_encode($orders);
      return response($orders, 200)->header('Content-Type', 'application/json');
    }

    public function unassignedOrderDataTable(Request $request)
    {

      $start = $request->start;
      $length  = $request->length;
      $totalCount = NotaryOrder::whereNull('notaryID')->count();

      $search = $request->search['value'];

      // incase search box is not empty
      if(!empty(trim($search))){
        // apply search here for now leaving empty
         // replace with the number fo records filtered from total
      }else{
        $filteredCount = $totalCount;
      }

      if(trim($search) != ''){
          $data = NotaryOrder::select('*')
              ->whereNull('notaryID')
              ->where(function($result) use($search){
                // inside condition
                $result->where('orderDate', 'LIKE','%'.$search.'%')
                        ->orwhere('Escrow', 'LIKE','%'.$search.'%')
                        ->orwhere('customerName', 'LIKE','%'.$search.'%')
                        ->orwhere('customerLastname', 'LIKE','%'.$search.'%')
                        ->orwhere('yourCompanyName', 'LIKE','%'.$search.'%');
              })
              ->take($length)->skip($start)->get();
          $filteredCount = count($data);
      }else{
          $data = NotaryOrder::select('*')->whereNull('notaryID')->take($length)->skip($start)->get();
      }



      $listing  = array();
      $colorClasses['red'] = 'bg-danger';
      $colorClasses['blue'] = 'bg-info';
      $colorClasses['yellow'] = 'bg-warning';
      $colorClasses['green']= 'bg-success';

      foreach ($data AS $val){
        // dd($val->client->titleCompany);
        $listItem = array();
          $listItem['orderDate'] = $val->orderDate;
          $listItem['Escrow'] = $val->Escrow;
          $listItem['borrowerName'] = $val->customerName.' '.$val->customerLastname;
          $listItem['yourCompanyName'] = $val->client->titleCompany;
          $listItem['requestedAppTime'] = $val->requestedAppTime;
          // TODO: need to get exact count of strikes (Shehroz to provide query logic for that)

              $odatetime = date('Y-m-d h:i:s', strtotime($val->orderDate));

              $day1 = $odatetime;
              $day1 = strtotime($day1);
              $day2 = date('Y-m-d h:i:s');
              $day2 = strtotime($day2);

              $diffHours = (($day2 - $day1) / 3600);

              $diffHours = number_format($diffHours,2);
              $diffHours = abs($diffHours);
              $notaryOrderID = $val->notaryOrderID;
              //echo 'diff hours'.$diffHours;

              $countHtml = '';
              if(date('Y-m-d h:i:s', strtotime($val->orderDate)) > date('Y-m-d h:i:s', strtotime('-48 hours')))
              {
                if($diffHours >= 1){
                  $countHtml .= '<h3 id="f'.$notaryOrderID.'c" style="color:red;font-size:13px">';
                  $countHtml .= '</h3>';
                }else {
                  $countHtml .= '<h3 id="f'.$notaryOrderID.'c" style="font-size: 13px">';
                  $countHtml .= '</h3>';
                }

                if($diffHours >= 1){
                  $countHtml .= '<span style="color: red; font-weight:bold; font-size:8px">';
                  $countHtml .= 'Tell Client Status!';
                  $countHtml .= '</span>';
                }

                $countHtml .= '<script>';
                $countHtml .= '$(\'#f'.$notaryOrderID.'c\').countid({';
                $countHtml .= 'clock: true,';
                $countHtml .= 'dateTime: \''.$day1.'\',';
                $countHtml .= 'dateTplElapsed: "%H : %M : %S",';
                $countHtml .= 'clock: true,';
                $countHtml .= '})';
                $countHtml .= '</script>';

              }else {
                $countHtml .= '<h3 id="f'.$notaryOrderID.'c" style="font-size: 13px;color:red;font-size:15px">';
                $countHtml .= 'Other';
                $countHtml .= '</h3>';
              }

          $listItem['counterTime'] = $countHtml;


          $workHtml = '';
          if ($val->working_on == 1) {
            $workHtml .= '<div>';
            $workHtml .= '<input type="radio" name="working_on'.$val->notaryOrderID.'" value="1" checked>';
            $workHtml .= ' Adam';
            $workHtml .= '<br>';
            $workHtml .= '<input type="radio" name="working_on'.$val->notaryOrderID.'" value="2">';
            $workHtml .= ' Meri';
            $workHtml .= '</div>';
          }

          $listItem['working_on'] = $workHtml;



          $editLink = url("/admin/order/".$val->notaryOrderID."/edit");


          $buttonsHtml = '<div class="btn-group">';
              $buttonsHtml .= '<button type="button" class="btn btn-outline-primary dropdown-toggle waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
              $buttonsHtml .= 'Action';
              $buttonsHtml .= '</button>';
              $buttonsHtml .= '<div class="dropdown-menu" style="">';
                  $buttonsHtml .= '<a class="dropdown-item" href="'.$editLink.'">Edit</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);" onclick="deleteMsg()">Delete</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="http://theprolinkupgrade/admin/client/1">View</a>';
              $buttonsHtml .= '<div class="dropdown-divider"></div>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);">Instructions</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);" onclick="blockNotary()">Block Notary</a>';
              $buttonsHtml .= '</div>';
          $buttonsHtml .= '</div>';
          $listItem['actions'] = $buttonsHtml;


          $listing[] = $listItem;
      }

      $orders['data'] = $listing;
      $orders['draw'] = $request->draw;
      $orders['recordsTotal'] = $totalCount;
      $orders['recordsFiltered'] = $filteredCount;

      // echo json_encode($orders);
      return response($orders, 200)->header('Content-Type', 'application/json');
    }

    public function savedOrder()
    {
        return view('admin.order.saved');
    }
    public function unassignedOrder()
    {
        return view('admin.order.unassigned');
    }
    public function holdOrder()
    {
        return view('admin.order.hold');
    }

    public function holdOrderDataTable(Request $request)
    {

      $start = $request->start;
      $length  = $request->length;
      $totalCount = NotaryOrder::count();

      $search = $request->search['value'];

      // incase search box is not empty
      if(!empty(trim($search))){
        // apply search here for now leaving empty
         // replace with the number fo records filtered from total
      }else{
        $filteredCount = $totalCount;
      }

      if(trim($search) != ''){
          $data = NotaryOrder::select('*')
              // ->where('status1', 1)
              ->where(function($result) use($search){
                // inside condition
                $result->where('orderDate', 'LIKE','%'.$search.'%')
                        ->orwhere('Escrow', 'LIKE','%'.$search.'%')
                        ->orwhere('customerName', 'LIKE','%'.$search.'%')
                        ->orwhere('customerLastname', 'LIKE','%'.$search.'%')
                        ->orwhereHas('client', function($query) use($search){
                              // ... Join table Search
                              $query->where('titleCompany', 'LIKE','%'.$search.'%');
                          });
              })
              ->take($length)->skip($start)->get();
          $filteredCount = count($data);
      }else{
          $data = NotaryOrder::select('*')->take($length)->skip($start)->get();
      }



      $listing  = array();
      $colorClasses['red'] = 'bg-danger';
      $colorClasses['blue'] = 'bg-info';
      $colorClasses['yellow'] = 'bg-warning';
      $colorClasses['green']= 'bg-success';

      foreach ($data AS $val){
        // dd($val->client->titleCompany);
        $listItem = array();
          $listItem['orderDate'] = $val->orderDate;
          $listItem['Escrow'] = $val->Escrow;
          $listItem['borrowerName'] = $val->customerName.' '.$val->customerLastname;
          $listItem['yourCompanyName'] = $val->client->titleCompany;
          $listItem['requestedAppTime'] = $val->requestedAppTime;
          // TODO: need to get exact count of strikes (Shehroz to provide query logic for that)


          $deleteHtml = '';
          $deleteHtml .= '<div>';
          $deleteHtml .= '<input type="checkbox" name="delete_hold_'.$val->notaryOrderID.'" value="true" onClick="deleteMsg()">';
          $deleteHtml .= '</div>';

          $listItem['delete'] = $deleteHtml;



          $editLink = url("/admin/order/".$val->notaryOrderID."/edit");


          $buttonsHtml = '<div class="btn-group">';
              $buttonsHtml .= '<button type="button" class="btn btn-outline-primary dropdown-toggle waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
              $buttonsHtml .= 'Action';
              $buttonsHtml .= '</button>';
              $buttonsHtml .= '<div class="dropdown-menu" style="">';
                  $buttonsHtml .= '<a class="dropdown-item" href="'.$editLink.'">Edit</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);" onclick="deleteMsg()">Delete</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="http://theprolinkupgrade/admin/client/1">View</a>';
              $buttonsHtml .= '<div class="dropdown-divider"></div>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);">Upload</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);" onclick="blockNotary()">Move To Unassigned</a>';
              $buttonsHtml .= '</div>';
          $buttonsHtml .= '</div>';
          $listItem['actions'] = $buttonsHtml;


          $listing[] = $listItem;
      }

      $orders['data'] = $listing;
      $orders['draw'] = $request->draw;
      $orders['recordsTotal'] = $totalCount;
      $orders['recordsFiltered'] = $filteredCount;

      // echo json_encode($orders);
      return response($orders, 200)->header('Content-Type', 'application/json');
    }

}
