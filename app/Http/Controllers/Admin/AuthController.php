<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
// import model
use App\Models\Admin;

class AuthController extends Controller
{
    /*
    * Authentication
    */

    public function index(){
      return view('admin.auth.login2');
    }

    public function login(Request $request){
      $request->validate([
                    'login_id' => 'required|string|min:3',
                    'login_password' => 'required|string|min:4',
                ]);

      /*
      * Default Login Code
      */

      $credentials = [
          'adminID' => $request->login_id,
          'password' => $request->login_password,
      ];

      if (Auth::guard('admin')->attempt($credentials)) {
          return redirect()->intended('/admin');
      }

      /*
      * MD5 Login Code
      */

      // $admin = \App\Models\Admin::where([
      //             'adminID' => $request->login_id,
      //             'password' => md5($request->login_password)
      //         ])->first();
      //
      // if ($admin) {
      //       Auth::guard('admin')->login($admin);
      //       return redirect()->intended('/admin');
      //   }

      return redirect('/admin/login')->with('error', 'You have entered invalid credentials');

    }

    public function logout() {
      Auth::guard('admin')->logout();
      return redirect('/admin/login');
    }

}
