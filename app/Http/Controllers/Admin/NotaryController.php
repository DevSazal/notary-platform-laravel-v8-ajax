<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use Illuminate\Validation\Rule;

use DataTables;

use App\Models\NotaryRegis;
use App\Models\State;

class NotaryController extends Controller
{
    public function __construct(){
      // $this->middleware('auth:admin');
      $this->middleware('admin');
    }

    public function index(){
      $array['notaries'] = NotaryRegis::orderBy('notaryID', 'desc')
                          ->paginate(10);
                          // ->get();
      return view('admin.notary.index')->with($array);
    }
    // public function notaryDataTable(Request $request)
    // {
    //   if ($request->ajax()) {
    //         $data = NotaryRegis::latest()->get();
    //         return Datatables::of($data)
    //                 ->addIndexColumn()
    //                 ->addColumn('action', function($row){
    //                     $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
    //                     return $btn;
    //                 })
    //                 ->rawColumns(['action'])
    //                 ->make(true);
    //     }
    //
    // }
    public function notaryDataTable(Request $request)
    {

      $start = $request->start;
      $length  = $request->length;
      $totalCount = NotaryRegis::count();

      $search = $request->search['value'];

      // incase search box is not empty
      if(!empty(trim($search))){
        // apply search here for now leaving empty
         // replace with the number fo records filtered from total
      }else{
        $filteredCount = $totalCount;
      }

      if(trim($search) != ''){
          $data = NotaryRegis::select('*')
              ->where('firstName', 'LIKE','%'.$search.'%')
              ->orwhere('notaryID', 'LIKE','%'.$search.'%')
              ->orwhere('lastName', 'LIKE','%'.$search.'%')
              ->orwhere('email', 'LIKE','%'.$search.'%')
              ->take($length)->skip($start)->get();
          $filteredCount = count($data);
      }else{
          $data = NotaryRegis::select('*')->take($length)->skip($start)->get();
      }




      $listing  = array();
      $colorClasses['red'] = 'bg-danger';
      $colorClasses['blue'] = 'bg-info';
      $colorClasses['yellow'] = 'bg-warning';
      $colorClasses['green']= 'bg-success';

      foreach ($data AS $row=>$val){
        $listItem = array();
          $listItem['notaryID'] = $val['notaryID'];
          $listItem['notaryName'] = $val['firstName'].' '.$val['lastName'];
          // TODO: need to get exact count of strikes (Shehroz to provide query logic for that)
          $listItem['strikes'] = '0';
          $listItem['city'] = $val['city'];
          if($val['phonePrefered'] == 1){
              $listItem['contact'] = $val['homePhone1'].'-'.$val['homePhone2'].'-'.$val['homePhone3'];
          }elseif($val['phonePrefered'] == 1){
              $listItem['contact'] = $val['businessPhone1'].'-'.$val['businessPhone2'].'-'.$val['businessPhone3'];
          }else{
              $listItem['contact'] = $val['cellPhone1'].'-'.$val['cellPhone2'].'-'.$val['cellPhone3'];
          }

          $listItem['availability'] = $val['availability'];
          if($val['status'] == 1){
              $listItem['status'] = 'Active';
          }else{
              $listItem['status'] = 'In-Active';
          }



        //  $listItem['color'] = $val['color'];
          if(!empty($val['color'])){
              $listItem['color'] = '<span class="btn '.$colorClasses[$val['color']].'">';
              $listItem['color'] .= '<span>';
          }else{
              $listItem['color'] = '';
          }


          $editLink = url("/admin/notary/".$val['n The notary has been updated']."/edit");


          $buttonsHtml = '<div class="btn-group">';
              $buttonsHtml .= '<button type="button" class="btn btn-outline-primary dropdown-toggle waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
              $buttonsHtml .= 'Action';
              $buttonsHtml .= '</button>';
              $buttonsHtml .= '<div class="dropdown-menu" style="">';
                  $buttonsHtml .= '<a class="dropdown-item" href="'.$editLink.'">Edit</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);" onclick="deleteMsg()">Delete</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="http://theprolinkupgrade/admin/client/1">View</a>';
              $buttonsHtml .= '<div class="dropdown-divider"></div>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);">Instructions</a>';
                  $buttonsHtml .= '<a class="dropdown-item" href="javascript:void(0);" onclick="blockNotary()">Block Notary</a>';
              $buttonsHtml .= '</div>';
          $buttonsHtml .= '</div>';
          $listItem['actions'] = $buttonsHtml;


          $listing[] = $listItem;
      }

      $notaries['data'] = $listing;
      $notaries['draw'] = $request->draw;
      $notaries['recordsTotal'] = $totalCount;
      $notaries['recordsFiltered'] = $filteredCount;

      // echo json_encode($notaries);
      return response($notaries, 200)->header('Content-Type', 'application/json');
    }

    public function create(){
      $array['states'] = State::orderBy('stateName', 'asc')->get();
      return view('admin.notary.create')->with($array);
    }

    public function registerNotary(Request $request){

      // ...  Notary Data Validation
      $validator = Validator::make($request->all(), [
          'firstName' => 'required|string|min:3|max:50',
          'lastName' => 'required|string|min:3|max:50',
          'address' => 'required|string|min:3',
          'address2' => 'string|nullable',
          'city' => 'required|string',
          'state' => 'required|integer',
          'zipCode' => 'required|string',
          'homePhone' => 'string|nullable',
          'businessPhone' => Rule::requiredIf(!$request->homePhone && !$request->cellPhone),
          'cellPhone' => Rule::requiredIf(!$request->homePhone && !$request->businessPhone),
          'phonePrefered' => 'required',
          'email' => 'required|email|max:255',
          'alternateEmail' => 'email|max:255|nullable',

          'ein1' => 'nullable|integer|min:2',
          'ein2' => 'nullable|integer|min:7',

          'ssn1' => 'required_without:ein1|required_without:ein2|nullable|integer|min:3',
          'ssn2' => 'required_without:ein1|required_without:ein2|nullable|integer|min:2',
          'ssn3' => 'required_without:ein1|required_without:ein2|nullable|integer|min:4',

          'overNightDelivery' => 'nullable',
          'overNightDelAddress1' => 'nullable|string',
          'overNightDelAddress2' => 'nullable|string',
          'overNightDelCity' => 'nullable|string',
          'overNightDelState' => 'nullable|integer',
          'overNightDelZip1' => 'nullable|string',

          'receiveDocEmail' => 'nullable',
          'laserPrinter' => 'nullable',
          'legalSizePage' => 'nullable',
          'availability' => 'nullable',

          'foreignLanguage' => 'required',
          'checkLanguage' => 'nullable',

          'howLongNotary' => 'nullable|string',
          'haveExperience' => 'nullable',
          'refer' => 'nullable',
          'howmanyLoanDocuments' => 'nullable|string',
          'notaryCommission' => 'nullable|string',
          'notaryCommissionExpiration' => 'nullable|string',

          'areYouBonded' => 'required',
          'bondCertificateNumber' => 'nullable|string',
          'bondCertificateAmount' => 'nullable|string',
          'bondCertificateExpiration' => 'nullable|string',

          'EO' => 'required',
          'eoInsuranceNo' => 'nullable|string',
          'eoInsuranceAmount' => 'nullable|string',
          'eoInsuranceExpiration' => 'nullable|string',

          'additionalInformation' => 'nullable|string',
          'adminComments' => 'nullable|string',

      ]);


      if ($validator->fails()) {
          return back()->withErrors($validator)
                        ->withInput();
          // return dd($validator->errors());
      }else {

        // ... save notary data
        $notary = new NotaryRegis;
        $notary->firstName = $request->firstName;
        $notary->lastName = $request->lastName;
        $notary->address1 = $request->address;
        $notary->address2 = $request->address2;

        $notary->city = $request->city;
        $notary->state = $request->state;
        $notary->zip1 = $request->zipCode;
        $notary->homePhone1 = $request->homePhone;
        $notary->businessPhone1 = $request->businessPhone;
        $notary->cellPhone1 = $request->cellPhone;
        $notary->phonePrefered = $request->phonePrefered;
        $notary->email = $request->email;
        $notary->alternateEmail = $request->alternateEmail;

        $notary->ein1 = $request->ein1;
        $notary->ein2 = $request->ein2;

        $notary->ssn1 = $request->ssn1;
        $notary->ssn2 = $request->ssn2;
        $notary->ssn3 = $request->ssn3;

        $notary->overNightDelivery = $request->overNightDelivery;
        $notary->overNightDelAddress1 = $request->overNightDelAddress1;
        $notary->overNightDelAddress2 = $request->overNightDelAddress2;
        $notary->overNightDelCity = $request->overNightDelCity;
        $notary->overNightDelState = $request->overNightDelState;
        $notary->overNightDelZip1 = $request->overNightDelZip1;

        $notary->receiveDocEmail = $request->receiveDocEmail;
        $notary->laserPrinter = $request->laserPrinter;
        $notary->legalSizePage = $request->legalSizePage;
        $notary->availability = $request->availability;

        $notary->foreignLanguage = $request->foreignLanguage;
        $notary->spanish = language_checker_if_found_by_array(
                                    'Spanish',
                                    $request->checkLanguage);
        $notary->french = language_checker_if_found_by_array(
                                    'French',
                                    $request->checkLanguage);
        $notary->german = language_checker_if_found_by_array(
                                    'German',
                                    $request->checkLanguage);
        $notary->chinese = language_checker_if_found_by_array(
                                    'Chinese',
                                    $request->checkLanguage);
        $notary->japanese = language_checker_if_found_by_array(
                                    'Japanese',
                                    $request->checkLanguage);
        $notary->italian = language_checker_if_found_by_array(
                                    'Italian',
                                    $request->checkLanguage);
        $notary->russian = language_checker_if_found_by_array(
                                    'Russian',
                                    $request->checkLanguage);
        $notary->hindi = language_checker_if_found_by_array(
                                    'Hindi',
                                    $request->checkLanguage);

        $notary->howLongNotary = $request->howLongNotary;
        $notary->haveExperience = $request->haveExperience;
        $notary->refer = $request->refer;
        $notary->howmanyLoanDocuments = $request->howmanyLoanDocuments;
        $notary->notaryCommission = $request->notaryCommission;
        $notary->notaryCommissionExpiration = $request->notaryCommissionExpiration;

        $notary->areYouBonded = $request->areYouBonded;
        $notary->bondCertificateNumber = $request->bondCertificateNumber;
        $notary->bondCertificateAmount = $request->bondCertificateAmount;
        $notary->bondCertificateExpiration = $request->bondCertificateExpiration;

        $notary->EO = $request->EO;
        $notary->eoInsuranceNo = $request->eoInsuranceNo;
        $notary->eoInsuranceAmount = $request->eoInsuranceAmount;
        $notary->eoInsuranceExpiration = $request->eoInsuranceExpiration;

        $notary->additionalInformation = $request->additionalInformation;
        $notary->adminComments = $request->adminComments;

        // default entry ...
        $notary->color = '';
        $notary->fee_loan_overnight_edocs = 0.00;
        $notary->fee_loan_edocs = 0.00;
        $notary->onlist_date = date('Y-m-d H:i:s');
        $notary->per = 0;
        $notary->bio = '';
        $notary->einLater = '';
        $notary->ssnLater = '';
        $notary->notaryFee = 0.00;
        $notary->approvedOvernight = 0.00;
        $notary->approvedEdocs = 0.00;
        $notary->updated = 0;
        $notary->omitted_zip = '';

        // $notary->password = Hash::make($request->password);

        $result = $notary->save();

        if ($result){
          $request->session()->flash('job', "The notary has been saved for notaryID=".$request->notaryID);
          // return ["result" => "Data has been saved."];
          return redirect('/admin/notary/create');
        } else{
          $request->session()->flash('job_failed', "The Operation Failed!");
          // return ["result" => "Operation Failed!"];
          return redirect('/admin/notary/create');
        }

        // dd($request);

        // dd($request->checkLanguage);
        // if (in_array('German', $request->checkLanguage)) {
        //     echo $request->checkLanguage[0];
        // }
        // if ($key = array_search('German', $request->checkLanguage)) {
        //     echo $request->checkLanguage[$key];
        // }
        // echo language_printer_if_found_by_array('German', $request->checkLanguage);
      }
    }

    public function show($id){
      return view('admin.notary.show');
    }

    public function edit($id){
      $array['notary'] = NotaryRegis::where('notaryID', $id)->firstOrFail();
      $array['states'] = State::orderBy('stateName', 'asc')
                          ->get();
      return view('admin.notary.edit')->with($array);
    }

    public function update($id, Request $request){

      // ...  Notary Data Validation
      $validator = Validator::make($request->all(), [
          'notaryID' => 'required',
          'firstName' => 'required|string|min:3|max:50',
          'lastName' => 'required|string|min:3|max:50',
          'address' => 'required|string|min:3',
          'address2' => 'string|nullable',
          'city' => 'required|string',
          'state' => 'required|integer',
          'zipCode' => 'required|string',
          'homePhone' => 'string|nullable',
          'homePhone2' => 'string|nullable',
          'homePhone3' => 'string|nullable',
          'businessPhone' => Rule::requiredIf(!$request->homePhone && !$request->cellPhone),
          'businessPhone2' => 'string|nullable',
          'businessPhone2' => 'string|nullable',
          'cellPhone' => Rule::requiredIf(!$request->homePhone && !$request->businessPhone),
          'cellPhone2' => 'string|nullable',
          'cellPhone2' => 'string|nullable',
          'phonePrefered' => 'required',
          'email' => 'required|email|max:255',
          'alternateEmail' => 'email|max:255|nullable',

          'ein1' => 'nullable|integer|min:2',
          'ein2' => 'nullable|integer|min:7',

          'ssn1' => 'required_without:ein1|required_without:ein2|nullable|integer|min:3',
          'ssn2' => 'required_without:ein1|required_without:ein2|nullable|integer|min:2',
          'ssn3' => 'required_without:ein1|required_without:ein2|nullable|integer|min:4',

          'overNightDelivery' => 'nullable',
          'overNightDelAddress1' => 'nullable|string',
          'overNightDelAddress2' => 'nullable|string',
          'overNightDelCity' => 'nullable|string',
          'overNightDelState' => 'nullable|integer',
          'overNightDelZip1' => 'nullable|string',

          'receiveDocEmail' => 'nullable',
          'laserPrinter' => 'nullable',
          'legalSizePage' => 'nullable',
          'availability' => 'nullable',

          'foreignLanguage' => 'required',
          'checkLanguage' => 'nullable',

          'howLongNotary' => 'nullable|string',
          'haveExperience' => 'nullable',
          'refer' => 'nullable',
          'howmanyLoanDocuments' => 'nullable|string',
          'notaryCommission' => 'nullable|string',
          'notaryCommissionExpiration' => 'nullable|string',

          'areYouBonded' => 'required',
          'bondCertificateNumber' => 'nullable|string',
          'bondCertificateAmount' => 'nullable|string',
          'bondCertificateExpiration' => 'nullable|string',

          'EO' => 'required',
          'eoInsuranceNo' => 'nullable|string',
          'eoInsuranceAmount' => 'nullable|string',
          'eoInsuranceExpiration' => 'nullable|string',

          'additionalInformation' => 'nullable|string',
          'adminComments' => 'nullable|string',

      ]);


      if ($validator->fails()) {
          return back()->withErrors($validator)
                        ->withInput();
          // return dd($validator->errors());
      }else {

        // ... update notary data
        $notary = NotaryRegis::findOrFail($id);
        $notary->firstName = $request->firstName;
        $notary->lastName = $request->lastName;
        $notary->address1 = $request->address;
        $notary->address2 = $request->address2;

        $notary->city = $request->city;
        $notary->state = $request->state;
        $notary->zip1 = $request->zipCode;
        $notary->homePhone1 = $request->homePhone;
        $notary->homePhone2 = $request->homePhone2;
        $notary->homePhone3 = $request->homePhone3;
        $notary->businessPhone1 = $request->businessPhone;
        $notary->businessPhone2 = $request->businessPhone2;
        $notary->businessPhone3 = $request->businessPhone3;
        $notary->cellPhone1 = $request->cellPhone;
        $notary->cellPhone2 = $request->cellPhone2;
        $notary->cellPhone3 = $request->cellPhone3;
        $notary->phonePrefered = $request->phonePrefered;
        $notary->email = $request->email;
        $notary->alternateEmail = $request->alternateEmail;

        $notary->ein1 = $request->ein1;
        $notary->ein2 = $request->ein2;

        $notary->ssn1 = $request->ssn1;
        $notary->ssn2 = $request->ssn2;
        $notary->ssn3 = $request->ssn3;

        $notary->overNightDelivery = $request->overNightDelivery;
        $notary->overNightDelAddress1 = $request->overNightDelAddress1;
        $notary->overNightDelAddress2 = $request->overNightDelAddress2;
        $notary->overNightDelCity = $request->overNightDelCity;
        $notary->overNightDelState = $request->overNightDelState;
        $notary->overNightDelZip1 = $request->overNightDelZip1;

        $notary->receiveDocEmail = $request->receiveDocEmail;
        $notary->laserPrinter = $request->laserPrinter;
        $notary->legalSizePage = $request->legalSizePage;
        $notary->availability = $request->availability;

        $notary->foreignLanguage = $request->foreignLanguage;
        $notary->spanish = language_checker_if_found_by_array(
                                    'Spanish',
                                    $request->checkLanguage);
        $notary->french = language_checker_if_found_by_array(
                                    'French',
                                    $request->checkLanguage);
        $notary->german = language_checker_if_found_by_array(
                                    'German',
                                    $request->checkLanguage);
        $notary->chinese = language_checker_if_found_by_array(
                                    'Chinese',
                                    $request->checkLanguage);
        $notary->japanese = language_checker_if_found_by_array(
                                    'Japanese',
                                    $request->checkLanguage);
        $notary->italian = language_checker_if_found_by_array(
                                    'Italian',
                                    $request->checkLanguage);
        $notary->russian = language_checker_if_found_by_array(
                                    'Russian',
                                    $request->checkLanguage);
        $notary->hindi = language_checker_if_found_by_array(
                                    'Hindi',
                                    $request->checkLanguage);

        $notary->howLongNotary = $request->howLongNotary;
        $notary->haveExperience = $request->haveExperience;
        $notary->refer = $request->refer;
        $notary->howmanyLoanDocuments = $request->howmanyLoanDocuments;
        $notary->notaryCommission = $request->notaryCommission;
        $notary->notaryCommissionExpiration = $request->notaryCommissionExpiration;

        $notary->areYouBonded = $request->areYouBonded;
        $notary->bondCertificateNumber = $request->bondCertificateNumber;
        $notary->bondCertificateAmount = $request->bondCertificateAmount;
        $notary->bondCertificateExpiration = $request->bondCertificateExpiration;

        $notary->EO = $request->EO;
        $notary->eoInsuranceNo = $request->eoInsuranceNo;
        $notary->eoInsuranceAmount = $request->eoInsuranceAmount;
        $notary->eoInsuranceExpiration = $request->eoInsuranceExpiration;

        $notary->additionalInformation = $request->additionalInformation;
        $notary->adminComments = $request->adminComments;

        // default entry ...
        // $notary->color = '';
        // $notary->fee_loan_overnight_edocs = 0.00;
        // $notary->fee_loan_edocs = 0.00;
        // $notary->onlist_date = date('Y-m-d H:i:s');
        // $notary->per = 0;
        // $notary->bio = '';
        // $notary->einLater = '';
        // $notary->ssnLater = '';
        // $notary->notaryFee = 0.00;
        // $notary->approvedOvernight = 0.00;
        // $notary->approvedEdocs = 0.00;
        // $notary->updated = 0;
        // $notary->omitted_zip = '';

        // $notary->password = Hash::make($request->password);

        $result = $notary->save();

        if ($result){
          $request->session()->flash('job', "Notary Profile Update");
          // return ["result" => "Data has been saved."];
          return redirect()->route('admin.notary.edit', $id);
        } else{
          $request->session()->flash('job_failed', "The Update Operation Failed!");
          // return ["result" => "Operation Failed!"];
          return redirect()->route('admin.notary.edit', $id);
        }
      }
    }

    public function active($id, Request $request){

        // ... update notary status
        $notary = NotaryRegis::find($id);
        $notary->status = 1;
        $result = $notary->save();

        if ($result){
          $request->session()->flash('job', "The notary has been updated for notaryID=".$id);
          return response()->json(
                  [
                    'message' => ['The notary has been actived for notaryID='.$id]
                  ], 202);

        } else{
          $request->session()->flash('job_failed', "The Update Operation Failed!");
          return response()->json(
                  [
                    'error' => ['The Operation is Failed!']
                  ], 406);
        }
    }

    public function inactive($id, Request $request){

        // ... update notary status
        $notary = NotaryRegis::find($id);
        $notary->status = 0;
        $result = $notary->save();

        if ($result){
          $request->session()->flash('job', "The notary has been updated for notaryID=".$id);
          return response()->json(
                  [
                    'message' => ['The notary has been inactived for notaryID='.$id]
                  ], 202);

        } else{
          $request->session()->flash('job_failed', "The Update Operation Failed!");
          return response()->json(
                  [
                    'error' => ['The Operation is Failed!']
                  ], 406);
        }
    }

    public function delete($id)
    {
      // code... Delete
        $notary = NotaryRegis::find($id);

        if($notary){
          $result = $notary->delete();
        }else {
          $result = NULL;
        }

        if($result){
          return response()->json(
                  [
                    'message' => ['The notary has been deleted.']
                  ], 202);
        }else{
          return response()->json(
                  [
                    'error' => ['The Delete Operation is Failed!']
                  ], 406);
        }
    }



}
