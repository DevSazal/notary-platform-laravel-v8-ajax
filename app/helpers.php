<?php

use Illuminate\Support\Facades\Auth;

/*
* Helper Function
*/

if (!function_exists('language_printer_if_found_by_array')) {
  function language_printer_if_found_by_array($value, $languageArray)
  {
    // echo "test...";
    if (empty($languageArray)) {
      return '';
    }

    if ($key = array_search($value, $languageArray) ) {
      return $languageArray[$key];
    }else {
      return '';
    }
  }

}

if (!function_exists('language_checker_if_found_by_array')) {
  function language_checker_if_found_by_array($value, $languageArray)
  {
    // echo "test...";
    if (empty($languageArray)) {
      return '0';
    }

    if (in_array($value, $languageArray) ) {
      return '1';
    }else {
      return '0';
    }
  }

}

if (!function_exists('encryptHash')) {
    function encryptHash($password) {
      if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
          $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
          return crypt($password, $salt);
      }
    }
}

if (!function_exists('encrypt_verify')) {
    function encrypt_verify($password, $hashedPassword) {
      return crypt($password, $hashedPassword) == $hashedPassword;
    }
}

if (!function_exists('encryptOpenSSL')) {
    /*
    * OpenSSl Encryption
    * Author: DevSazal
    */
    function encryptOpenSSL($password) {

      // Store a string into the variable which
      // need to be Encrypted
      $simple_string = $password;

      // Store the cipher method
      $ciphering = "AES-128-CTR";

      // Use OpenSSl Encryption method
      $iv_length = openssl_cipher_iv_length($ciphering);
      // echo $iv_length;
      // echo "<br>";
      $options = 0;

      // Non-NULL Initialization Vector for encryption
      $encryption_iv = '1234567891011121';

      // Store the encryption key
      $encryption_key = "AdamEncryptedByte";

      // Use openssl_encrypt() function to encrypt the data
      $encryption = openssl_encrypt($simple_string, $ciphering,
      $encryption_key, $options, $encryption_iv);

      return $encryption;
    }
}
if (!function_exists('decryptOpenSSL')) {
    /*
    * OpenSSl Decryption
    * Author: Sazal Ahamed
    */
    function decryptOpenSSL($encryptedPassword) {

      // Store a string into the variable which
      // need to be Encrypted


      // Store the cipher method
      $ciphering = "AES-128-CTR";

      // Use OpenSSl Encryption method
      $iv_length = openssl_cipher_iv_length($ciphering);
      $options = 0;

      // Non-NULL Initialization Vector for decryption
      $decryption_iv = '1234567891011121';

      // Store the decryption key
      $decryption_key = "AdamEncryptedByte";

      // Use openssl_decrypt() function to decrypt the data
      $decryption=openssl_decrypt ($encryptedPassword, $ciphering,
      $decryption_key, $options, $decryption_iv);

      // Display the decrypted string
      return $decryption;
    }
}
